#ifndef _TT_KMOST_
#define _TT_KMOST_

// ======================================================================= //
// Cristinel Ababei 10/17/2003
// ======================================================================= //

#include <list>

#include "ipr_types.h"

using namespace std;

// ======================================================================= //


// ======================================================================= //
// Used for the K-most critical paths stuff:
typedef list<int> ll;
typedef list<double> ll_double;
// ======================================================================= //
// This class is used for the k-most critical paths storage and 
// manipulation. Implements the algo in 
// Ghanta(DAC89) & Saleh(DAC91).  This is reused 
// code, and for now some members may not be in use.
class KmostPaths 
{
  // paths:
  // delays:
  // no_of_enumerated_paths:
  // fragments_no_j:  Number of fragments for path j
  // k_j:  Number of times a paths has been cut so far
  // k_j_edge:  Will reflect only edges (not hedges) cut along paths
  // no_of_verts_in_frag_i_in_path_j
  // verts_in_frag_i_in_path_j
  // edgeweights_in_frag_i_in_path_j

  ll * paths; 
  double * delays;
  int no_of_enumerated_paths;
  //int * fragments_no_j;
  int * k_j;
  int * k_j_edge;
  ll * no_of_verts_in_frag_i_in_path_j;
  ll * verts_in_frag_i_in_path_j;
  ll_double * edgeweights_in_frag_i_in_path_j;
  
 public:
  KmostPaths(){}

  //gets:
  ll * Paths() { return paths; }
  double * Delays() { return delays; }
  int NoOfEnumPaths() { return no_of_enumerated_paths; }
  int * K_j() { return k_j;}
  int * K_j_edge() { return k_j_edge;}
  //int * K_j_hm() { return k_j_hm;}
  ll * No_of_verts_in_frag_i_in_path_j() 
    { return no_of_verts_in_frag_i_in_path_j; }
  ll * Verts_in_frag_i_in_path_j() 
    { return verts_in_frag_i_in_path_j; }
  ll_double * Edgeweights_in_frag_i_in_path_j() 
    { return edgeweights_in_frag_i_in_path_j; }
  
  // sets:
  void setPaths(ll * f_paths) {paths =f_paths;};
  void setDelays(double * f_delays) {delays =f_delays;};
  void setNoOfEnumPaths(int f_value) { no_of_enumerated_paths =f_value; }
  void setK_j(int * f_k_j) {k_j =f_k_j;};
  void setK_j_edge(int * f_k_j_edge) {k_j_edge =f_k_j_edge;};
  //void setK_j_hm(int * f_k_j_hm) {k_j_hm =f_k_j_hm;};
  void setNo_of_verts_in_frag_i_in_path_j(ll * f_value) 
    { no_of_verts_in_frag_i_in_path_j =f_value; }
  void setVerts_in_frag_i_in_path_j(ll *  f_value) 
    { verts_in_frag_i_in_path_j =f_value; }
  void setEdgeweights_in_frag_i_in_path_j(ll_double * f_value) 
    { edgeweights_in_frag_i_in_path_j =f_value; }
  
};
// ======================================================================= //

#endif




