// ======================================================================= //
// Cristinel Ababei 10/11/2003
// ======================================================================= //
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <algorithm>

#include "fpga3d.h"
#include "tt_hgraph.h"

using namespace std;

extern s_fpga3d * fg3;

// ======================================================================= //
//                      IdWeightPair
// ======================================================================= //
// ======================================================================= //
//                      Element
// ======================================================================= //
// ======================================================================= //
//                      Vertex
// ======================================================================= //
Vertex::Vertex() 
{
  netID_fpga3d=-1;
  is_on_kmost=0;
  tokens=0;
  vmiu=0;
  Crit=0;
  setX(0);setY(0);setZ(0);
  arr=0,req=100000000,slack=0;
  max_delay_to_sink=0;
  FF=0;
}
// ======================================================================= //
void Vertex::addElementInFanin(int VertexId)
{
  Element t_element; // allocate
  t_element.ID=VertexId;
  t_element.miu=0;
  
  fINverts[VertexId] = t_element; // fINverts is a map
}
// ======================================================================= //
void Vertex::addElementInFanout(int VertexId)
{
  Element t_element; // allocate
  t_element.ID=VertexId;
  t_element.miu=0;
  
  fOUTverts[VertexId] = t_element; // fOUTverts is a map
}
// ======================================================================= //
// ======================================================================= //
//                       Net
// ======================================================================= //
Net::Net() 
{ 
  netID_fpga3d=-1;
  weight_was_biased=0;
  delay_type=VAR_DELAY;
  weight=0;
  delay=0;
}
// ======================================================================= //
// ======================================================================= //
//                       tt_ghraph
// ======================================================================= //
tt_hgraph::tt_hgraph()
{
  NumVerts=0;NumNets=0;K=10;//default
  flag =0;	
}
// ======================================================================= //
tt_hgraph::tt_hgraph( int f_NumNets )
{
  NumVerts=0;
  NumNets=f_NumNets;K=10;//default
  nets.resize(NumNets);
  flag =0;
}
// ======================================================================= //
void tt_hgraph::addVertex( Vertex t_vertex )
{
  // NumVerts is used as index and to count inserted vertices.
  // Vertex was allocated already and we get here its pointer

  t_vertex.vmiu=0;
  t_vertex.tokens=0;
  t_vertex.netID=-1;
  t_vertex.setDelay(0);
  t_vertex.delay_type = VAR_DELAY;//will potentially be set as FIXED

  t_vertex.ID = NumVerts;
  verts[ NumVerts ] = t_vertex;
  NumVerts = NumVerts + 1; //Prepare it for next Vertex insertion.
}//eof

void tt_hgraph::addVertex( Vertex t_vertex, int imposed_id )
{
  // NumVerts is only incremented here to count no of added
  // verts but it's not used as verts index.
  // Vertex was allocated already and we get here its pointer
  t_vertex.vmiu=0;
  t_vertex.tokens=0;
  t_vertex.netID=-1;  
  t_vertex.setDelay(0);
  t_vertex.delay_type = VAR_DELAY;//will potentially be set as FIXED

  t_vertex.ID = imposed_id;
  verts[ imposed_id ] = t_vertex;
  NumVerts = NumVerts + 1; //Count all added verts
}//eof
// ======================================================================= //
void tt_hgraph::addNet( int id )
{
  Net t_net;
  t_net.ID=id;
  t_net.weight=0; //Initialization to zero.
  nets.push_back( t_net );//Add it to nets vector
}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::BuildNets()
{
  // Create the nets vector with all nets in circuit
  // each nets(i) has a verts vector with pointers to all vertices 
  // that make up the net first element in that vector is all the 
  // time the source for that net also create the net for each verts(j)

  int nets_with_coresp=0;
  int counter=0;
  int t_id1=0;
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
    {
      t_id1 = i1->second.ID;
      // sanity check:
      //printf("  %d %s %s",t_id1 ,i1->second.Name(), i1->second.Oper());
      //if(t_id1<0) { printf("\n t_id1<0 inside BuildNets! for vertex %d", 
      // i1->second.ID); exit(1);}
      if(i1->second.fOUTverts.size() !=0) // adica daca are fanout
	{
	  if(i1->second.fOUTverts.size() ==1)
	    {
	      map<int,Element>::iterator i2=i1->second.fOUTverts.begin();
	      addNet( counter );
	      verts[t_id1].netID = counter;
	      nets[counter].verts.push_back(&verts[t_id1]); //add source
	      nets[counter].verts.push_back(&verts[i2->second.ID]);
	      nets[counter].delay_type = i1->second.delay_type;
	      if (i1->second.netID_fpga3d != -1) {
		nets_with_coresp++;
		// Adica this vertex drives a inter-clbs net in fpga3d.
		nets[counter].netID_fpga3d = i1->second.netID_fpga3d;
		// fg3 is global/extern top-level fpga3d
		fg3->net[i1->second.netID_fpga3d].netID_tim_hg = counter;
		//printf(" %d %d ",fg3->net[i1->second.netID_fpga3d].netID_tim_hg,
		//     nets[counter].netID_fpga3d);
	      }

	      counter++;
	    }
	  else // fanout >1
	    {
	      addNet( counter );
	      verts[t_id1].netID = counter;
	      nets[counter].verts.push_back(&verts[t_id1]);//add source
	      nets[counter].delay_type = i1->second.delay_type;
	      if (i1->second.netID_fpga3d != -1) {
		nets_with_coresp++;
		nets[counter].netID_fpga3d = i1->second.netID_fpga3d;
		// fg3 is global/extern top-level fpga3d
		fg3->net[i1->second.netID_fpga3d].netID_tim_hg = counter;
	      }

	      for(map<int,Element>::iterator i2=i1->second.fOUTverts.begin();
		  i2!=i1->second.fOUTverts.end();i2++)
		{
		  //add sink(s)
		  nets[counter].verts.push_back(&verts[i2->second.ID]);

		}
	      counter++;
	    }
	  //cout<<"  "<<t_id1<<" "<< (counter-1);
	}//if else
    }//for

NumNets = counter;
printf("\n tt_hgraph -- NumVerts=%d  NumNets=%d",NumVerts,NumNets);
printf("\n Nets with corespondents in net of fpga3d =%d, excluding "
       "global nets, which are not included in timing_graph_02.",
       nets_with_coresp);
}//eof
// ======================================================================= //



// ======================================================================= //
//
//
// Implementation of Statistical Timing Analysis (STA)
//
//
// ======================================================================= //



// ======================================================================= //
void tt_hgraph::Put_net_delay_info_into_timing_graph_02(float **net_delay)
{
// This function should be called only after net_delay was allocated and 
// populated with Elmore delay camputed with trees after detailed routing 
// has been done (more precisely, after a call of 
// load_net_delay_from_routing() ).
// It puts Elmore net-delay at all sinks of every net into the fanout 
// lists of my (cristinel.ababei) timing graph, so that at the end I 
// shall compute delay of final placed and (detailed) routed circuit.
 
  int block_id_of_source=0, block_id_of_sink=0;
  int inet_whose_source_is_sink=0;
  int id_nets=0, id_nets_sink=0;
  int id_verts=0, id_verts_of_sink=0;
  double delay_of_sink=0;
  int inet=0,ipin=0;
  double old_place_level_delay=0;

  for (inet=0;inet<fg3->num_nets;inet++) {
   if (fg3->is_global[inet] == false) 
    { 

      block_id_of_source = fg3->net[inet].blocks[0];// 0
      id_nets = fg3->net[inet].netID_tim_hg;//id of coresp net in timing hg
      id_verts = ( *nets[id_nets].verts.begin() )->ID;

      // Next is out of scope of this function but it's usefull
      // for print_to_file_criticalities_of_all_nets(). I added later 
      // on in here.
      fg3->net[inet].weight = nets[id_nets].weight;

      for (ipin=1;ipin<fg3->net[inet].num_pins;ipin++) 
	{
	  // Delay from source to current sink.
	  delay_of_sink = net_delay[inet][ipin];
	  block_id_of_sink = fg3->net[inet].blocks[ipin];// ipin

	  // Next 2 if's go along the same line as it went for
	  // populating this info in create_timing_tt_hgraph_02()
	  if ( fg3->block[block_id_of_sink].type == OUTPAD )
	    id_verts_of_sink =
	      fg3->block[block_id_of_sink].id_tt_hgraph_02[ 0 ];//po
	  if ( fg3->block[block_id_of_sink].type == CLB )
	    id_verts_of_sink =
	      fg3->block[block_id_of_sink].
	      id_tt_hgraph_02[ fg3->net[inet].blk_pin[ipin] ];

	  // .....................................................................
	  // Sanity check: look and see if this delay is smaller
	  // than what is was immediately after placement when it was
	  // "estimated" based on LUT-delay generated with vpr tool.
	  /*---
	  old_place_level_delay = verts[id_verts].fOUTverts[id_verts_of_sink].miu;
	  if ( fg3->block[block_id_of_source].type == CLB &&
	       fg3->block[block_id_of_sink].type == CLB )
	    if (old_place_level_delay > delay_of_sink) {
	      printf("\n Found source-sink pair [%d,%d] of net %d with %d"
		     " terminals, which has place-delay=%e BIGGER"
		     " than route-delay=%e.  And both are CLBs.",
		     block_id_of_source,block_id_of_sink,
		     inet, fg3->net[inet].num_pins,
		     old_place_level_delay, delay_of_sink);
	      printf("\n Locations of source: [%d,%d,%d] and sink: [%d,%d,%d].",
		     fg3->block[block_id_of_source].x,fg3->block[block_id_of_source].y,
		     fg3->block[block_id_of_source].z,
		     fg3->block[block_id_of_sink].x,fg3->block[block_id_of_sink].y,
		     fg3->block[block_id_of_sink].z);
	      //printf("\n Abort."); exit (1);
	    }//if
	    ---*/
	  // .....................................................................

	  // Put is the coresponding fanout in my tt graph.
	  verts[id_verts].fOUTverts[id_verts_of_sink].miu = delay_of_sink;
 	}//for ipin
    }//if (is_global[inet] == false)
  }//for inet

  printf("Done Put_net_delay_infor_into_timing_graph_02.\n");

}//eof


// ======================================================================= //


void tt_hgraph::Dump_delays_of_all_source_sink_pairs( char outFileName[] )
{
// Debugging purposes only.

  char *fname;
  fname=outFileName;
  ofstream mystream(fname);
  if(!mystream) 
    {cout<<"Abort:  Error opening file to write source-sink delays!";exit(1);}


  int block_id_of_source=0, block_id_of_sink=0;
  int id_nets=0;
  int id_verts=0, id_verts_of_sink=0;
  int inet=0,ipin=0;
  // Go thru all nets and look at all source-sink pairs
  // and dump them into outfile.
  for (inet=0;inet<fg3->num_nets;inet++)
    {
      block_id_of_source = fg3->net[inet].blocks[0];// 0
      id_nets = fg3->net[inet].netID_tim_hg;//id of coresp net in timing hg
      id_verts = ( *nets[id_nets].verts.begin() )->ID;
      for (ipin=1;ipin<fg3->net[inet].num_pins;ipin++) 
	{
	  block_id_of_sink = fg3->net[inet].blocks[ipin];// ipin
	  id_verts_of_sink = 
	    fg3->block[block_id_of_sink].
	    id_tt_hgraph_02[ fg3->net[inet].blk_pin[ipin] ];

	  mystream << verts[id_verts].fOUTverts[id_verts_of_sink].miu << " ";
 	}//for ipin
    }//for inet

  mystream.close();
  printf("Dump_delays_of_all_source_sink_pairs.\n");
}//eof


// ======================================================================= //
double tt_hgraph::Get_Static_Delay()
{
  // Returns normal max among all POs
  double max_delay =0;
  // === Find maximum delay as miu among all PO's
  max_delay =0;
  for(map<int,Vertex>::iterator i2=verts.begin();i2!=verts.end();i2++){
    if ( ( i2->second.Type() == OUTPAD ) ||
	 ( i2->second.FF == 1 ) ) // If it is a FF-driven clb vertex.
      {
	if(i2->second.arr > max_delay)
	  {
	    max_delay = i2->second.arr;
  } }	}
  //cout<<" max_delay="<<max_delay;
  return max_delay;
}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::Compute_Static_DelayForPOs()
{
  // Used to put LAT accumulated till a vertex in "vmiu" of that vertex.
  // Was overlaping practically what ComputeArrivalTimesFromPIsToPOs() does.
  // 
  // Therefore, instead of using it for getting the static delay
  // use ComputeArrivalTimesFromPIsToPOs(), which is exactly
  // STA!  Then look for max among outputs for "arr" not for "vmiu"!

  ComputeArrivalTimesFromPIsToPOs();

}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::ComputeArrivalTimesFromPIsToPOs()
{
  // Forward propagation to compute latest arrival time
  // needed for slack computation.
  
  queue<int> frontwave; 

  // Clean first; reset tokens:
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
    {
      i1->second.arr =0;
      i1->second.tokens =0;
    }
  int processedPI=0;
  // === first, go through all PIs
  for(map<int,Vertex>::iterator i2=verts.begin();i2!=verts.end();i2++)
    {
      if ( ( i2->second.Type() == INPAD ) ||
	   ( i2->second.FF == 1 ) ) // If it is a FF-driven clb vertex.
	{
	  //printf(" %d",i2->second.ID);
	  frontwave.push(i2->second.ID);
	}
    }
  // === second, process all gates in circuit, i.e. until "frontwave" 
  // queue is empty
  while(!frontwave.empty())
    {
      // take it from front of FIFO queue
      int ver_ID=frontwave.front();
      frontwave.pop(); // delete it also 
      // reset it here and prepare it in this way for next time
      verts[ver_ID].tokens=0; 
      //printf(" %d", ver_ID);
      ComputeArrivalTimesFromPIsToPOs_ProcessVertex(ver_ID, &frontwave);
      // Note that new elements are added to queue inside th eabove call
    }

}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::ComputeArrivalTimesFromPIsToPOs_ProcessVertex( int vertex_id,
                   queue<int> * frontwave )
{
  // Here we take the latest arrival time at the out of this vertex and 
  // send it to  all its fanout gates.  For each fanout gate compute the 
  // current_arrival_time, which is made latest_arrival_time of this 
  // fanout vertex only if it's bigger than the already present, increment
  // "tokens", and add to queue only if all inputs were processed.
  double cur_arr=0;
  double start_arrival=0;
  Vertex *cur_vertex = &verts[vertex_id];
  
  // ...................................................................
  // If it is a FF-driven clb vertex we do not continue with
  // propagation of latest arrival time computation, because this 
  // driven thru a FF vertex has to be split to form a PO + PI
  // in static analysis.  BUT, I start propagating as if this
  // vertex were a PI, which means a start_arrival=0.
  if (cur_vertex->FF == 1) { // Driven thru a FF.
    start_arrival = 0;
  }
  else {                     // NOT driven thrua FF.
    start_arrival = cur_vertex->arr;
  }
  // ...................................................................

  for (map<int,Element>::iterator i1=cur_vertex->fOUTverts.begin();
       i1!=cur_vertex->fOUTverts.end();i1++)
    { 
      // OBS:  POs will not be processed because they do not have fanout
      // anyway!
      Vertex *temp_vertex = &verts[i1->second.ID];
      cur_arr = start_arrival + i1->second.miu + temp_vertex->Delay();
      // Store accumulated delay in fanin of driven vertex:
      temp_vertex->fINverts[vertex_id].miu =
	start_arrival + i1->second.miu;
      if ( (cur_arr > temp_vertex->arr) &&
	   (temp_vertex->Type() != INPAD) ) {
	// I should not get into a INPAD by forward propagation!
	// However I check for it! 
	// Also, here I will end up setting the arrival time to
	// any vertex driven thru a FF too, but I should not add
	// it to the "frontwave" because I did it at the begining
	// when I added them together with INPADs.
	temp_vertex->arr = cur_arr;
      }
      
      if ( ( temp_vertex->Type() != INPAD ) &&
	   ( temp_vertex->FF != 1 ) )// Driven thru a FF vertices 
	                             // represent "virtual" stopping nodes,
	                             // and I do not add them to "frontwave" 
	                             // because I did it at the begining
	{
	  // Check to see whether we have to add this gate to queue 
	  // or we still have to wait until all its inputs are processed	.
	  temp_vertex->tokens++;
	  if ( temp_vertex->tokens == temp_vertex->fINverts.size() )
	    {
	      frontwave->push(i1->second.ID);
	      //cout<<" "<<temp_vertex->arr;
	    }
	}//if
    }//for
  
}//eof
// ======================================================================= //



// ======================================================================= //
void tt_hgraph::ComputeRequiredTimesFromPOsToPIs()
{
  // Backward propagation to compute required arrival time
  // needed for slack computation.

  queue<int> frontwave; 

  // Clean the prev. computation mess:
  for(map<int,Vertex>::iterator i01=verts.begin();i01!=verts.end();i01++)
    {
      i01->second.req =10000000.0; // i assume it as infinite!
      i01->second.slack =0.0;
    }
  // Reset tokens:
  for(map<int,Vertex>::iterator i0=verts.begin();i0!=verts.end();i0++)
    i0->second.tokens =0; // really necessary?

  // === first, find out max latest arrival time among all POs, 
  // and FF-driven vertices which are virtual POs,
  // and set req time this max+1 for all POs.  
  // then propagate backwards...
  double max_arr =0;
  for(map<int,Vertex>::iterator i2=verts.begin();i2!=verts.end();i2++){
    if ( ( i2->second.Type() == OUTPAD ) ||
	 ( i2->second.FF == 1 ) ) { // If it is a FF-driven clb vertex.
      if(i2->second.arr > max_arr) {
	max_arr =i2->second.arr;
  } }	}
  //cout<<"max_arr="<<max_arr;

  // === set req as max_arr+1 for all POs Type=3*
  // and FF-driven vertices which are virtual POs.
  max_arr = max_arr + 1;
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++) {
    if ( ( i1->second.Type() == OUTPAD ) ||
	 ( i1->second.FF == 1 ) ) {// If it is a FF-driven clb vertex.
      i1->second.req =max_arr;
      i1->second.slack =i1->second.req - i1->second.arr;
      //printf(" %f",i1->second.slack);
      frontwave.push(i1->second.ID);
    }	}

  // === backpropagates
  int processed_verts=0;
  while(!frontwave.empty())
    {
      int ver_ID=frontwave.front();
      frontwave.pop();
      // Rreset tokens it here and prepare it in this way for next time:
      verts[ver_ID].tokens =0; 
      processed_verts++;
      //printf(" %d", ver_ID);
      ComputeRequiredTimesFromPOsToPIs_ProcessVertex(ver_ID, &frontwave);
    }//while
  //printf("\n Enqueued no of verts during slack computation = %d",processed_verts);

  // === bias slacks such that all are positive
  BiasSlack();

  // === Put biased slacks into nets[].weight
  for(vector<Net>::iterator n=nets.begin();n!=nets.end();++n)
    {
      n->weight = verts[ (*n->verts.begin())->ID ].slack;
      //cout<<" "<<n->weight;
    }//do it every time when slack is updated
  
  // ===  make this slack actually hedge weight:
  // ***********************************************************
  // *** Order edges nonincreasing after their slack
  // ***********************************************************
  vector<IdWeightPair> ord_weights;
  for(vector<Net>::iterator i1_net=nets.begin();i1_net!=nets.end();i1_net++){
    IdWeightPair temp_pair;
    temp_pair.ID=i1_net->ID;
    temp_pair.Weight=i1_net->weight;
    ord_weights.push_back(temp_pair);
  }
  sort(ord_weights.begin(),ord_weights.end());//increasing order
  // ***********************************************************
  // *** Slack_Based_Weight                                  ***
  // ***********************************************************
  if ( EDGE_WEIGHT_COMPUTATION_SCHEME == 1 )
    {
      // 10% of most critical nets are biased with a constant factor
      // to emphasize thus their criticality. 
      vector<IdWeightPair>::reverse_iterator i7_vec =ord_weights.rbegin();
      double max_slack =nets[i7_vec->ID].weight + 1;
      int i=0;
      // ---> forward going through
      for(vector<IdWeightPair>::iterator i21_vec=ord_weights.begin();
	  i21_vec!=ord_weights.end();i21_vec++)
	{
	  i++;
	  if( i<(int)ceil( 0.1*NumNets ) ) //10% very critical
	    nets[i21_vec->ID].weight = (max_slack - nets[i21_vec->ID].weight) +100;
	  else
	    nets[i21_vec->ID].weight = (max_slack - nets[i21_vec->ID].weight) +1;
	}
    }//if
  // ***********************************************************
  // *** Slack_Based_Weight                                  ***
  // ***********************************************************
  if( EDGE_WEIGHT_COMPUTATION_SCHEME == 0 )
    {
      // Initial interval [min, max_old] is stretched into a larger
      // interval [min, max_new] to easier differentiate different nets.
      vector<IdWeightPair>::reverse_iterator i3_vec =ord_weights.rbegin();
      double max_slack_2 =nets[i3_vec->ID].weight;
      vector<IdWeightPair>::iterator i32_vec =ord_weights.begin();
      double amount1 = 
	(max_slack_2 - nets[i32_vec->ID].weight)/NORMALIZING_MAX_EDGE_CRITICALITY;
      //cout <<  " amount1 = " << amount1 ;
      // ---> backward going through
      for(vector<IdWeightPair>::reverse_iterator i33_vec=ord_weights.rbegin();
	  i33_vec!=ord_weights.rend();i33_vec++)
	{
	  nets[i33_vec->ID].weight = 
	    ((max_slack_2 - nets[i33_vec->ID].weight)/amount1) + 1;
	  //cout<<" "<<nets[i33_vec->ID].weight;
	}
    }//if
  // ***********************************************************
  // === Put criticality based on slack into .Crit of each driving node
  // used to set edge weight when constructiong sub-graphs inside Capo
  // related stuff?
  for(vector<Net>::iterator n2=nets.begin();n2!=nets.end();++n2)
    {
      verts[ (*n2->verts.begin())->ID ].Crit = n2->weight;
      //cout<<" "<<n2->weight ;
    }
  // ***********************************************************
  // printf("\nDone ComputeRequiredTimesFromPOsToPIs."); 
}//eof
// ======================================================================= //
void tt_hgraph::ComputeRequiredTimesFromPOsToPIs_ProcessVertex(int vertex_id,
		  queue<int> * frontwave )
{
  // Necessary only inside ComputeRequiredTimesFromPOsToPIs()

  double cur_req=0;
  double miu_of_fanin_wire_of_cur=0;
  Vertex *cur_vertex = &verts[vertex_id];
  int ver_id =vertex_id;

  if( cur_vertex->Type() != INPAD )
    {
      for(map<int,Element>::iterator i2=cur_vertex->fINverts.begin();
	  i2!=cur_vertex->fINverts.end();i2++)
	{ 
	  miu_of_fanin_wire_of_cur=0; //reset
	  map<int,Element>::iterator i2_fanin = 
	                  verts[i2->second.ID].fOUTverts.find( vertex_id );
	  if(i2_fanin != verts[i2->second.ID].fOUTverts.end())
	    {
	      miu_of_fanin_wire_of_cur = i2_fanin->second.miu;
	    }
	  else 
	    { printf("\n Abort:  Source fanin not found in slack computation!\n");
	    exit(1);}  
	  cur_req  = 
	    cur_vertex->req - (cur_vertex->Delay() + miu_of_fanin_wire_of_cur);
	  if(cur_req < verts[i2->second.ID].req)
	    {
	      verts[i2->second.ID].req =cur_req;
	      // SLACK itself computation:
	      verts[i2->second.ID].slack = cur_req - verts[i2->second.ID].arr;
	    }
	  
	  verts[i2->second.ID].tokens++;
	  // Driven thru a FF vertices represent "virtual PI" stopping nodes,
	  // and I do not add them to "frontwave" because I did it 
	  // at the begining, when I added them together with real POs.
	  if ( ( verts[i2->second.ID].tokens == 
		 verts[i2->second.ID].fOUTverts.size() ) &&
	       ( verts[i2->second.ID].FF != 1 ) ) // Aici e sarea si piperul!
	    {
	      frontwave->push(i2->second.ID);
	      //printf(" [%d]:%f",verts[i2->second.ID].ID,
	      //verts[i2->second.ID].slack);
	    }


	}//for
    }//if
  //ELSE:  do nothing in order not to cycle stop this back-going when 
  //       encountering PIs
}//end
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::BiasSlack()
{
  // Look for the min slack.  If it is negative then add to all slacks
  // its absolute val+1. these slacks will be assigned as hedge weight 
  // in hmetis and have to be positive.

  double min_slack =100000000, abs_min_slack=0;
  int min_slack_index;

  for(map<int,Vertex>::iterator i2=verts.begin();i2!=verts.end();i2++)
    { 
      //cout<<" "<<i2->second.ID;
      if(i2->second.slack < min_slack)
	{
	  min_slack =i2->second.slack;
	  min_slack_index =i2->second.ID;
	  //printf(" ..min_slack=%f",min_slack);
	} 
    }//for
  
  //printf("\n min_slack of verts[%d] = %f", min_slack_index, min_slack);

  if(min_slack <= 0)
    {
      printf("\n Biased slack this time!");
      abs_min_slack = fabs(min_slack) +1;
      for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
	{
	  i1->second.slack = i1->second.slack + abs_min_slack;
	  //cout << " " << i1->second.slack ;
	}
    }//if
}//end
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::UpDateSlack()
{
  // Redo slack computation because delay of wires have changed.
  // Needed to have weights/criticalities updated in this way.

  ComputeArrivalTimesFromPIsToPOs(); // --- it's STA itself
  ComputeRequiredTimesFromPOsToPIs();// --- also slack and Crit OVER-write	    

  printf("\n Done UpDateSlack().");

}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::SortSlacksAndPrintThemInFileInNonIncreasingOrder( 
					     char outFileName[] )
{
  // Debugging purposes only. Makes sense to call only after slacks
  // were computed at least once.

  char *fname;
  fname=outFileName;
  ofstream myresult(fname);
  if(!myresult) 
    { cout << "Abort:  Error opening file to write slacks in!"; exit(1); }
  // === copy edge slacks in a vector
  vector<IdWeightPair> ord_slacks;
  for(vector<Net>::iterator i1_net=nets.begin();i1_net!=nets.end();i1_net++)
    {
      IdWeightPair temp_pair;
      temp_pair.ID=i1_net->ID;
      temp_pair.Weight=i1_net->weight;
      ord_slacks.push_back(temp_pair);
    }
  // === sort them nonincreasing after their (biased already) slacks
  sort(ord_slacks.begin(),ord_slacks.end());
  // === dump them to file
  for(vector<IdWeightPair>::reverse_iterator i1_vec=ord_slacks.rbegin();
      i1_vec!=ord_slacks.rend();i1_vec++)
    {
      myresult<<nets[i1_vec->ID].weight<<endl;
    }
}//eof
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::Print_tt_hgraph( char outFileName[] )
{
  // Debugging purposes only.

  int i=0;
  map<int,Element>::iterator i2=0;
  char *fname;
  fname=outFileName;
  ofstream mystream(fname);
  if(!mystream) 
    {cout<<"Abort:  Error opening file to write tt_hgraph in!";exit(1);}

  mystream<<"\n***********"<<" Vertices "<<"***********"<<endl;
  mystream<<"ID"<<" "<<"Type"<<endl;
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
    {
      mystream<<i1->second.ID<<" "<<i1->second.Type()<<endl;
 
      mystream<<" fin("<<i1->second.fINverts.size()<<"): ";
      for(i2=i1->second.fINverts.begin();
	  i2!=i1->second.fINverts.end();i2++)
	{
	  mystream<<" "<<i2->second.ID;
	} mystream<<endl;
      mystream<<" fout("<<i1->second.fOUTverts.size()<<"): ";
      for(i2=i1->second.fOUTverts.begin();
	  i2!=i1->second.fOUTverts.end();i2++)
	{
	  mystream<<" "<<i2->second.ID<<" "<<i2->second.miu;
	} mystream<<endl;
      mystream<<endl;
    }

  mystream<<"\n***********"<<" Nets "<<"***********"<<endl;
  mystream<<"ID"<<" "<<"delay_type"<<"(delay)"<<" "<<"Verts"<<endl;
  i=0;
  for(vector<Net>::iterator ii1=nets.begin();ii1!=nets.end();ii1++)
  {
    mystream<<ii1->ID<<" "<<ii1->delay_type<<"("<<ii1->delay<<")  ";
    for(vector<Vertex*>::iterator ii2=ii1->verts.begin();
	ii2!=ii1->verts.end();ii2++)
      {
	mystream<<" "<<(*ii2)->ID;
      }
    mystream<<endl;
  }
  
  mystream.close();
}//eof
// ======================================================================= //

