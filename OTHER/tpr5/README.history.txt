
Cristinel Ababei (ababei@ece.umn.edu)


TPR == Three-D Placement and Routing
(placement started in Oct 2003 | routing started on Nov 15 2003)
-----------------------------------------------------------------

(Apr 13 2004)
Started making the whole animal work for sequential circuits as 
well.

(Apr 07 2004)
Delay estimation at place level is done using delay lookup
tables generated locally for each circuit.  The bug in VPR related
to this estimation is here partially corrected.

(Apr 07 2004)
After 1 very hard day I figured out why sometimes estimated
delay at place-level for a source-sink pair is bigger than
the one at routing-level.

(Mar 31 2004)
3D detailed routing appears to work with .arch files, which can
have only vias of length 1 or multiple/mixed vias.  Default
I am working with an arch with vias of length 1,2,and longvia.

(Mar 07 2004)
Binary search routing - varying x/y channel width - works now;
for a fixed z channel with (i.e., number of vias in chanz).

(Mar 04 2004)
Apparently whole detailed routing works just fine!
We can save routing file. Delay is reported based on
detailed routing Elmore delay.

(Mar 03 2004)
3D routing - breadth first search - seems to work for the 
first time.
Need to compute delay based on the detailed routing results.

(Jan 28 2004)
Re-start working on the 3D routing. Develop and debug.

(Nov 20 2003)
Put the project on hold. DAC due date is comming
and there is no chance to finish in time the routing.
Focus on placement only. Placer reports delay estimated
based on bounding-boxes. 

(Nov 15 2003)
Started TPR routing engine development. 
The starting infrastructure is the 3D placement.
Looking just at porting all c functions from VPR to my c++ TPR.
In a first stage the routing (breadth first search) works
for the 2D case only but without delay reporting after
because taht is inside the VPR graph.  
The 3D pain is yet to come... 
