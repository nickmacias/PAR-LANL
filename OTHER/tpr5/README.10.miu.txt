
Author:  Cristinel Ababei (ababei@ece.umn.edu) March 07 2004.

// -------------------------------------------------------------- //

This archive contains TPR, which stands for Three-D Placement
and Routing for FPGAs. 
It was built by porting to c++ all(most) the c code of VPR,
a placement and routing tool for 2D, developed by 
Vaughn Betz and other authors at the University of Toronto.
In what follows the reader is assumed to be already 
familiar with VPR.

To run TPR on the (small) sample circuit (e64 from the MCNC 
benchmark suite) and the sample FPGA architecture files type 
(assuming you are already in tpr directory):


tpr TESTS/e64-4lut.net TESTS/4lut.arch placements/e64.p routings/e64.r -nodisp -route_chan_width 12 -K 11 -vertical_delay 0 -vertical_uwl 1 -num_layers 3


Observations:
(*)  -nodisp was added to the command line above in order not to 
run the X-Windows graphics capabilities of this software 
(capabilities, which are yet under development...).

(*)  The logic block for this architecture contains only a
4 LUT plus a flip flop.  The routing wire segments all span 1 
logic block in this architecture.  This is basically the
4lut_sanitized.arch of VPR, which has added however vertical
segments which span only two adjacent layers.

// -------------------------------------------------------------- //

Contents of the archive:

./tpr

  COPYRIGHT.txt:        Copyright notice, which applies to all files.
  README.10.miu.txt:This file, describing release 1.0 miu.
  README.history.txt:   Development history, for records only.
  manual_tpr.pdf:       Manual of TPR in .pdf file format.
  Debugging.txt:        Few examples of command lines.
  DelayLUT.txt:         Table look-up with optimistic delays computed
			with VPR routing tool.  Entries are used during
			placement for delay estimation, assignment to cut nets. 

  *.cpp:     Source code for TPR.

  Makefile:  Makefile for TPR.  Currently set for Linux 
	     (Pink Tie: Red Hat clone) and g++; you may
             have to modify the library paths and compiler 
	     options on your machine.

  /include/*.h: Header files for TPR.

  /lib/libhmetis.a: hMetis library for Linux and Sun. Use one for your platform.

  /TESTS/4lut*.arch: Sample FPGA architecture files, 
		     with a logic block containing 1 4-LUT and 1 FF.
  /TESTS/4x4lut*.arch: Sample FPGA architecture files, 
		       with a logic block containing 4 4-input LUTs and 4 FFs.

  /TESTS/*.net: Netlists files, which come with VPR tool.

  /scripts/*.*: Various scripts.
  /results:     Used to save output files in. May have some scripts too.
  /placements:  Currently empty; used to save .p files in.
  /routings:    Currently empty; used to save .r files in.

// -------------------------------------------------------------- //
