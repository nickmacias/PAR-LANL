// ======================================================================= //
// Cristinel Ababei 10/10/2003
// ======================================================================= //
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>

#include "fpga3d.h"
#include "hmetisInterface.h"

using namespace std;

// ======================================================================= //


void s_fpga3d::init_hmetisInterface(void)
{
  // Used for transfering nodes and nets to hmetis interface; i.e.,
  // preparation for calling hmetis partitioner.
  // Instantiate MetisIntfc class and initialize the interface, i.e.
  // introduce into the interface all modules and then all nets with 
  // their connectivity.  Also, here, we set the net weights.
  int i=0,j=0,local_num_nets=0,local_num_blocks=0;
  // === Instatiating the Metis Interface class
  // : numpins is zero in my case; pins are seen as modules with area zero
  for (i=0;i<num_nets;i++) 
    {
      if (is_global[i] == false) local_num_nets++; 
      // No global nets. 
    }
  for (i=0;i<num_blocks;i++) 
    {
      if( ( block[i].type == INPAD ) && 
	  ( is_global[ block[i].nets[0] ] == true ) ) continue;
      local_num_blocks++;
    }
  metis = new MetisIntfc(local_num_blocks,local_num_nets);
  // cristinel.ababei:  Global nets are disregarded in the
  // place and route processes.  That is why I will not consider
  // global nets in the initial partitioning; neither later on.
  // Global INPADs will always be placed on layer 0.

  
  // === Adding Modules (i.e., nodes, vertices, whatever)    
  for (i=0;i<num_blocks;i++) 
    {
      if( ( block[i].type == INPAD ) && 
	  ( is_global[ block[i].nets[0] ] == true ) ) continue;
      // Skip blocks which are PI and drive global nets, such
      // as pclk, etc.  These blocks are only placed during
      // PI/PO assignment.   

      metis->AddModule( i ); //id is used to transfer to hmetis all blocks
      // I do not know if pads should be allowed
      // technologically at any layer or maybe only at the bottom or 
      // only at the top layers.  Hopefully they will be distributed
      // evenly among dif layers by hmetis and not clustered in one
      // layer only because there may be problems with the number
      // of IO pads available in one layer - which is smaller as
      // area that if placement had been flat 2D.
      metis->setModWeight( i, 1 ); // all blocks same weight because all
                                   // clb's have same area
      metis->setModPartition( i, -1 ); // free to move, not fixed
    }//for


  // ==== Adding connectivity information:  one net at a time
  for (i=0;i<num_nets;i++) 
    {
      if (is_global[i] == false) 
	{ 
	  int* mods = new int[ net[i].num_pins + 1 ];
	  for (j=0;j<net[i].num_pins;j++)
	    {
	      mods[j] = net[i].blocks[j]; // add this net-terminal
	    }
	  metis->AddNet( i, net[i].num_pins, mods );
	  metis->setNetWeight( i, 1 );
	  //metis->setNetWeight( i, net[i].weight );
	  
	  delete mods;
	}//if
    }//for
  
  printf("\nDone init_hmetisInterface.");
}//eof
// ======================================================================= //


// ======================================================================= //
/*---
void s_fpga3d::setting_condition_hmetis(void)
{
// Set conditions inside the MetisIntfc metis class interface to 
// prepare for the HMetis call. Here theoretically we should set the module
// partitions (i.e. modules are fixed, which would allow me a direct
// implementation of my second strategy? think about), module 
// weight--influences the balance

  int i=0; int j=0;
  
  // setting pin partition ...
  
  // setting modules movable or fixed ...
  for(i=0; i<numcells; ++i) {
    // module is movable
    // Obs: giving to the fixed modules 0 weights they don't affect
    // balancing but giving weight 1 they will affect balancing inside 
    // hmetis performance
    metis->setModPartition(i, -1);  
    metis->setModWeight(i, 1); 
  }
  
  // Setting UBFactor
  ubfactor = 2;
  
  return;
  
  cout<<"\n ----- Showing Net Info --"<<endl;
  for(i=0; i<numnets; ++i) {
    metis->ShowNetInfo(i);
    cout<<endl;
  }
  
  cout<<"\n ----- Showing Module Info --"<<endl;
  for(i=0; i<numcells; ++i) {
    metis->ShowModuleInfo(i);
    cout<<endl;
  }
  
  cout<<"\n ----- Showing Hyper Graph --"<<endl;
  metis->ShowHyperGraph();
  cout<<"\n ----- Showing Options --"<<endl;
  metis->ShowOptions();                                                       
  
}//eof
---*/
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::partitioning_to_layers(void)
{
  // num_layers:  number of layers in 3D
  // Initializes hmetis interface, populates it, calls hmetis,
  // and puts result back into block[]

  init_hmetisInterface();
  //printf("\n ...Done metis initialization.");
  
  //setting_condition_hmetis();
  //printf("\n ...Done metis further setting.");
  
  //printCells();
  //metis->ShowHyperGraph();
  
  printf("\nPerforming 1st partitioning_to_layers.");
  // actual call of HMetis
  metis->Partition( num_layers ); 

  printf("\nDone partitioning_to_layers.");
  //metis->ShowResults();
 
  get_hmetis_results_into_clbs();
  //printf("\n Cutsize = %d",cutset());
  
}//eof
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::get_hmetis_results_into_clbs(void)
{
  // Simply update block[].layer with the result from 
  // after calling hmetis
  int i=0;
  for (i=0;i<num_blocks;i++) 
    {

      if( ( block[i].type == INPAD ) && 
	  ( is_global[ block[i].nets[0] ] == true ) ) {
	// Skip blocks which are PI and drive global nets, such
	// as pclk, etc.  These blocks are only placed during
	// PI/PO assignment.  And they are always put in first 
	// layer 0.
	block[i].layer = 0;
	block[i].z = 0;
	pi_balance[ 0 ]++;
      } 

      else {
	block[i].layer = metis->getModPartition( i );//0,1,2,,...,num_layers-1
	block[i].z = block[i].layer;//0,1,2,,...,num_layers-1
	//cout << block[i].layer;
	balance[ block[i].layer ]++; //keep track of how many blocks in layers
	
	if ( block[i].type == INPAD ) 
	  pi_balance[ block[i].layer ]++;//pi_balance member of s_fpga3d
	else
	  if ( block[i].type == OUTPAD )
	    po_balance[ block[i].layer ]++;
	  else
	    clb_balance[ block[i].layer ]++;
      }

    }//for
 
  for (i=0;i<num_layers;i++) 
    {
      printf("\n === Layer %d: %d blocks - clb:%d pi:%d po:%d",
	     i,balance[i],clb_balance[i],pi_balance[i],po_balance[i]);
     }//for

}//eof
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::print_hmetis_result(void)
{

  metis->ShowResults();
  
  // Read the Module Partition from Result
  cout<<"\n==========================="
      <<"\n  RESULT of EXPERIMENT 1 - All nets have unit wt."
      <<"\n==========================="<<endl;
  int i=0;
  
  for (i=0;i<num_blocks;i++) 
    {
      if( ( block[i].type == INPAD ) && 
	  ( is_global[ block[i].nets[0] ] == true ) ) continue;
      // Skip blocks which are PI and drive global nets, such
      // as pclk, etc.  These blocks are only placed during
      // PI/PO assignment.   
      
      metis->ShowModuleInfo(i);
      cout<<endl;                                                             
    }//for
}//eof
// ======================================================================= //


// ======================================================================= //
