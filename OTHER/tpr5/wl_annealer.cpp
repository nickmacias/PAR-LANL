// ======================================================================= //
// Cristinel Ababei 11/14/2003
// ======================================================================= //

//#include <stdlib.h>
#include <iostream>

#include "wl_annealer.h"

#define DELTA 0.01
using namespace std;

// ======================================================================= //
bool stop;

// ======================================================================= //

wl_annealer::wl_annealer(s_fpga3d *fg,int layer_id):fg(fg),layer_id(layer_id)
{ 
  // Ctr whose main purpose is to populate id2block vector.
  tX=0;tY=0;temp_Ux=0;temp_Uy=0;temp_Vx=0;temp_Vy=0;
  magic=0;
  for (int i=0;i<fg->num_blocks;i++) 
    {
      if ( (fg->block[i].layer == layer_id)&&(fg->block[i].type == CLB) )
	{
	  id2block.push_back( fg->block[i].ID );
	  //printf(" %d",fg->block[i].ID);
	  magic++;
	  // 0 1 2 3 4 // id local here
	  // 0 3 6 7 8 // block
	}
    }//for
}//eof
// ======================================================================= //

// ======================================================================= //
double wl_annealer::cost_move_one_cell(s_block * b)
{
  // Cost este calculat numai pe baza nets care "ating"
  // acest block care deci e selectat ptr a fi mutat.
  // I should modify it so that nodes on kmost critical paths
  // shall not be moved?
  double cost=0;
  int j=0,max_pin=0;
  // --- Compute weighted cost.
  max_pin = fg->pins_per_clb;
  for (j=0;j<max_pin;j++)
    {
      if ( b->nets[j] != OPEN )
	{
	  cost = cost + 
	    fg->net[b->nets[j]].cost3D(fg) * fg->net[b->nets[j]].weight; 
	}
    }//for
  // --- Bias a lot cost if move is to congested clb.
  if( (fg->grid[layer_id][b->x][b->y] > 1) && fix ) cost+=10000000.;
  return cost;
}//eof
// ======================================================================= //

// ======================================================================= //
bool wl_annealer::anneal_move_one_cell(double temp)
{ 
  // Picks up a random block, generates randomly
  // new location, see if it's worth moving it according to
  // the SA philosophy.
  double oldcost=0,newcost=0;
  // Pick-up randomly a block.  "magic" is number of blocks in 
  // current Partition. 
  int block = id2block[ (int)((rand()/(RAND_MAX+1.))*(magic-1))+1 ];
  Z = &fg->block[ block ];
  //cout<<" "<<block<<" "<<Z->ID;
  // Store old cost.
  oldcost = cost_move_one_cell(Z);
  //cout<<" oc="<<oldcost;
  // Generate - randomly - new X,Y location.
  tX = (int)(((rand()/(RAND_MAX+1.))-0.5)*2*range);
  tX = min( fg->nx - Z->x, tX );
  tX = max( 1 - Z->x, tX );
  tY = (int)(((rand()/(RAND_MAX+1.))-0.5)*2*range);
  tY = min( fg->ny - Z->y, tY );
  tY = max( 1 - Z->y, tY );
  //cout<<" tX="<<tX<<" tY="<<tY;
  if (!(tX | tY)) return false;
  Z->setXY(fg->grid[layer_id], Z->x+tX, Z->y+tY, true);
  newcost = cost_move_one_cell(Z);
  //cout<<" nc="<<newcost;
  if (newcost <= oldcost) return fix;
  double prob = exp(-(newcost-oldcost)/temp);
  double val = (rand()/(RAND_MAX+1.));
  return prob > val;
}//eof
// ======================================================================= //

// ======================================================================= //
void wl_annealer::run_wl_anneal_move_one_cell(int seed,double cool,
					      int moves,double current)
{
  // Looks first for temperature and then does the annealing
  // for wl minimization.
  srand(seed);
  stop = fix = false;
  double hot=1,rstep=0.995;
  range = 2.99;
  int accepted;
  cout<<endl;
  while(fabs((double)accepted/(double)moves - current) > DELTA)
    {
      cout<<"looking for HOT, temp="<<hot<<"   ";
      accepted = 0;
      for(int x=0;x<moves;++x)
	{
	  if(anneal_move_one_cell(hot)) ++accepted;
	  if(tX || tY) Z->setXY(fg->grid[layer_id],Z->x-tX,Z->y-tY,true);
	}
      cout<<"got "<<((double)accepted/(double)moves)<<" < "<<current<<endl;
      hot *= (((double)accepted/(double)moves) < current)?1.2:0.7;
    }
  fix = true;
  cout<<"HOT="<<hot<<", range="<<range<<", movesper="<<moves<<endl;
  accepted = moves;
#define STOP 0.001
  while(((double)accepted/(double)moves>STOP) && (range>=1.) && !stop)
    {
      accepted = 0;
      for(int x=0;x<moves;++x)
	{
	  if (anneal_move_one_cell(hot)) ++accepted;
	  else Z->setXY(fg->grid[layer_id],Z->x-tX,Z->y-tY,true);
	}
      range = range*rstep;
      hot *= cool;
      cout<<"new range="<<range<<", new temp="<<hot;
      cout<<"   accepted percentage: "<<(float)accepted/moves;
      cout<<"   last Cost = " <<fg->get_3D_WL()<<endl;
    } 
  cout<<"Done wr_annealing at layer="<<layer_id<<endl;
}//eof
// ======================================================================= //


// ======================================================================= //
