.define gas(31,20,\
;Lattice Gas Cellular Automata as described in http://homepage.univie.ac.at/Franz.Vesely/cp0102/dx/node124.html

;*** define inputs and outputs to supercell

;*** western inputs
particleEin:in,\ ; the particle that enters the west is the east bit, TRAVELLING eastward
clockInW:in,\

;*** western outputs
particleWout: out,\

;*** eastern inputs
particleWin:in,\ ; the particle that enters the east is the west bit, TRAVELLING westward

;*** eastern outputs
particleEout: out,\
clockOutE:out,\

;*** northern inputs
particleSin:in,\ ; the particle that enters the north is the south bit, TRAVELLING southward
clockInN:in,\

;*** northern outputs
particleNout: out,\

;*** southern inputs
particleNin:in,\  ; the particle that enters the south is the north bit, TRAVELLING northward

;*** southern outputs
particleSout: out,\
clockOutS:out\


) ;end define gas

;*** Fix input/output locations so supercells line up

;*** I/O. Put these where supercells will line up. I put the inputs and outputs together
;.locate <name>, <which line>, <start r or c> <end r or c>
.locate clockInW, w, 12, 12
.locate clockOutE, e, 12, 12

.locate particleEin, w , 6, 6
.locate particleEout, e , 6, 6

.locate particleWout, w, 8, 8
.locate particleWin, e, 8, 8

.locate clockInN, n, 1, 1  
.locate clockOutS, s, 1, 1

.locate particleNin, s, 3, 3  ; note, bit N comes from the S, traveling Nward
.locate particleNout, n, 3, 3

.locate particleSin, n, 18, 18
.locate particleSout, s, 18, 18


.library "../MAIN.LIB" ;generic input library
.libout "GAS.LIB" ;save compiled cell for subsequent tiling

;*** Instantiate components

;*** 4 flip flops for the gas particles traveling outward from this supercell in the Northward, Eastward, Southward and Westward directions

; .instance <name>=<function><!@[row,col]> ;@ part is optional
.instance  particleN=dff_posedge() ; a positive-edge triggered Master Slave Flip Flop in the main library
.instance  particleE=dff_posedge()
.instance  particleS=dff_posedge()
.instance  particleW=dff_posedge()

;*** components for the calculation of u: u = [(e xor n) and (w xor s)] and (e xor !w)
.instance u1=xor()  ; need an xor for (e xor n). Call it u1.
.instance u2=xor() ; for(w xor s)
.instance u3=and2()  ; for u3=u1 AND u2
.instance u4=not() ; (!w)
.instance u5=xor() ; for (e xor u4)
.instance u6=and2() ; for u3 AND u5

;**** 4 xors to produce the next generation of particle directions for the flip flops (n = n xor u, w = w xor u, e = e xor u, s = s xor u

.instance n1=xor() ; n1.out = n xor u
.instance e1=xor() ; e1.out = e xor u
.instance s1=xor() ; s1.out = s xor u
.instance w1=xor() ; w1.out = w xor u

;*** or the clock inputs
.instance cS=or2()
.instance cE=or2()

;*** Define the wiring of components
;.net(<from>, <to>)
; n = n xor u
.net(particleNin, n1.a)
.net(u6.x, n1.b)
.net(n1.x, particleN.d) ; set the ff new value

; e = e xor u
.net(particleEin, e1.a)
.net(u6.x, e1.b)
.net(e1.x, particleE.d) ; set the ff new value

; s = s xor u
.net(particleSin, s1.a)
.net(u6.x, s1.b)
.net(s1.x, particleS.d) ; set the ff new value

; w = w xor u
.net(particleWin, w1.a)
.net(u6.x, w1.b)
.net(w1.x, particleW.d) ; set the ff new value 

; create u
;*** components for the calculation of u: u = [(e xor n) and (w xor s)] and (e xor !w)
.net(particleEin, u1.a) ; (e xor n)
.net(particleNin, u1.b) ; (e xor n)
.net(particleWin, u2.a) ; (w xor s)
.net(particleSin, u2.b) ; (w xor s)
.net(u1.x, u3.a) ;(e xor n) AND (w xor s)
.net(u2.x, u3.b) ;(e xor n) AND (w xor s)
.net(particleWin, u4.a) ; !w
.net(u4.x, u5.b) ; (e xor !w)
.net(particleEin, u5.a) ; (e xor !w)
.net(u3.x, u6.a) ; [u1 AND u2] AND (e xor !w)
.net(u5.x, u6.b) ; [u1 AND u2] AND (e xor !w)

; put clock line to Master Slave Flip Flops
.net(clockInW, particleN.clk) 
.net(clockInW, particleE.clk) 
.net(clockInW, particleS.clk) 
.net(clockInW, particleW.clk) 

; hook flip flops to outside world
.net(particleN.q, particleNout)  
.net(particleS.q, particleSout)  
.net(particleW.q, particleWout)  
.net(particleE.q, particleEout) 

; pass clock to neighbors
.net(clockInW, cE.a)
.net(clockInN, cE.b)
.net(cE.x, clockOutE)

.net(clockInW, cS.a)
.net(clockInN, cS.b)
.net(cS.x, clockOutS)

;*** End of Input***


