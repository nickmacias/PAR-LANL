#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int rread(int,char *,int); 
int iread();

main(int argc,char **argv)
{
  int i,j,k,l,ifp,h,w,desc_len,left,leftsize;
  FILE *fp;
  int bitnum,sum1,sum0,pow_route_sum,pow_all_sum,r,c,ttcount,route;
  char ttbyte;
  char in,out,program[16],buffer[80],desc[1024];
  char *matrix;

  if (argc != 3){
    fprintf(stderr,"Usage: %s file.bin file.cuse\n",argv[0]);
    return(1);
  }

// read binary file

  ifp=open(argv[1],O_RDONLY);
  if (ifp < 0){
    fprintf(stderr,"ERROR: Cannot open %s\n",argv[1]);
    return(1);
  }

  rread(ifp,buffer,80); /* read comment */ 
  buffer[79]='\0';
  
  h=iread(ifp);
  w=iread(ifp); /* read dims */

  printf("Grid is %dx%d\n%s\n",h,w,buffer);

// Allocate matrix memory
  matrix=malloc(16*h*w);
  if (matrix==NULL){
    fprintf(stderr,"Out of memory\n");
    return(1);
  }

  k=l=0; // running output location in matrix[] and input loc in program[]
  
  for (i=0;i<h;i++){
    for(j=0;j<w;j++){
      rread(ifp,&in,1); // inputs
      rread(ifp,&out,1); // output
      rread(ifp,program,16); // PROGRAM!
      desc_len=iread(ifp); /* read # bytes in desc */
      if (desc_len > 1023) desc_len=1023;
      if (desc_len > 0) rread(ifp,desc,desc_len); /* and read description */
      else strcpy(desc,"");
      desc[1023]='\0';                 

      for (l=0;l<16;l++) matrix[k++]=program[l];
    }
  }
  close(ifp);

// Now we've loaded the binary circuit into matrix[]
// 16 TT bytes begin at matrix[(width*row+col)*16]
// Read cell utilization file and process
  fp=fopen(argv[2],"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"ERROR: Can't read utilization file <%s>\n",argv[2]);
    return(1);
  }

  pow_route_sum=pow_all_sum=0;

// Now read the file information
  while (NULL != fgets(buffer,120,fp)){
    if (2==sscanf(buffer,"route:[%d,%d]",&r,&c)) route=1;
    else if (2==sscanf(buffer,"comp:[%d,%d]",&r,&c)) route=0;
    else {
      fprintf(stderr,"Error: found <%s>. Does not look like .cuse file\n",
              buffer);
      fclose(fp);return(1);
    }

// Now calc power usage on [r,c]
    for (i=0;i<8;i++){ // for each column
      sum0=sum1=0; // Count 1s and 0s
      for (ttcount=0;ttcount<16;ttcount++){
        ttbyte=matrix[((r*w+c)*16)+ttcount];
        if (ttbyte & (1<<i)) ++sum1; else ++sum0;
      }
      j=(sum1>sum0)?(sum1-sum0):(sum0-sum1);
      j=16-j; // likelihood of output change
      //printf("[%d,%d]%d %d:%d (%d)\n",r,c,route,sum1,sum0,j);
      pow_all_sum+=j;
      if (route==1) pow_route_sum+=j;
    } // end of all columns
    //printf("\n");
  } // EOF
  fclose(fp);

  printf("Total power: %d (%d from routing, %d from components)\n",
          pow_all_sum,pow_route_sum,pow_all_sum-pow_route_sum);
}

/*
 * (*&$*(#& microsoft totally screws up binary IO...it writes CR in front of any
 * LF, and if it reads Ascii 26, it fails on future read()s
 */

rread(fp,buffer,len)
int fp,len;
char *buffer;
{
 int i;
 char in[2];

 for (i=0;i<len;i++){
   if (2 != read(fp,in,2)) fprintf(stderr,"ERROR: Failed on read\n");
   buffer[i]=((in[0]<<4)&0xf0) | (in[1]&0xf);
 }
 return(len);
}
  
wwrite(fp,buffer,len)
int fp,len;
char *buffer;
{
   int i;
   char out[2];
   
   for (i=0;i<len;i++){
     out[0]=((buffer[i]>>4)&0xf) | 0x20;  
     out[1]=((buffer[i])&0xf) | 0x20; 
     if (2 != write(fp,out,2)) fprintf(stderr,"ERROR: Failed on write\n");
   }
   return(len);
}

iread(fp)   /* Read a 16-bit integer */
int fp;
{
  char c1,c2;
  unsigned int i,j;
  
  rread(fp,&c1,1);i=0xff&c1;
  rread(fp,&c2,1);j=0xff&c2;
  return(i+256*j);
}                 

void iwrite(fp,i)
int fp,i;
{
  char c;
  c=0xff&i;
  wwrite(fp,&c,1);
  c=(i>>8)&0xff;
  wwrite(fp,&c,1);
}
