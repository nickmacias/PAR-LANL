#include "par.h"

dump_circuit(struct circuit *circuit)
{
  struct instance *inst; /* temporary variable */
  struct netlist *templist;
  struct libentry *le;
  struct netnode *tempnode;
  struct netnode *nn;
  int i;


/* Show results of parsing */
  printf("Circuit Name=%s\n",circuit->name);
  printf("dims=%dx%d\n",circuit->height,circuit->width);

  printf("EDGE I/O:\n");
  i=0;
  while (circuit->io[i] != (char *)NULL){
    printf("%s:%s:side=%c;cells=%d,%d\n",
           circuit->io[i],
           (circuit->iodir[i]==0)?"in":"out",
           circuit->ioside[i],
           circuit->iocell1[i],
           circuit->iocell2[i]);
    ++i;
  }

  printf("\nINSTANCES:\n");
  inst=next_instance(circuit,1); /* Start new instance retrieval */
  while (NULL != (inst=next_instance(circuit,0))){
    printf("%s(%s) IO assignments: ",inst->name,inst->basename);
    i=0; /* Dump the input/output mapping */
    while (inst->ioname[i]!=(char *)NULL){
      printf("%s(index=%d) ",inst->ioname[i],inst->ioindex[i]);
      ++i;
    }
    printf("\n");
    printf("Component is %dx%d\n",inst->libdef->h,inst->libdef->w);
  }


/***
  printf("\nLIBRARY ENTRIES:\n");
  le=circuit->library;
  while (le->next != (struct libentry*)NULL){
    le=le->next;
    printf("%s\n",le->basename);
// dump the I/O info for this component
    for (i=0;i<le->numio;i++){
      printf("IO[%d]:%s=[%d,%d]%s\n",i,le->io[i].iobasename,
                                     le->io[i].row,
                                     le->io[i].col,
                                     le->io[i].side);
    }
    printf("\n");
  }
***/

  printf("\n\nNETS:\n");
  templist=circuit->nets;
  while (templist->next != NULL){
    templist=templist->next;
    tempnode=templist->net;
    printf("[");
    while (tempnode->next != NULL){
      tempnode=tempnode->next;
      if (tempnode->edge==0) printf("%s:%d ",tempnode->c->name,tempnode->ioindex);
      else printf("EDGE:%d ",tempnode->ioindex);
    }
    printf("]\n");
  }

/* Now test the network traversal calls */
  printf("\n\nTESTING NETWORK RETRIEVAL PROCEDURES\n");
  get_net(circuit,1);
  while (0 == get_net(circuit,0)){
    printf("NEW NETWORK: ");
    while ((nn=get_node()) != (struct netnode*)NULL){
      if (nn->edge==0) printf("%s.%d ",nn->c->name,nn->ioindex);
      else printf("EDGE:%d ",nn->ioindex);
    }
    printf("\n");
  }
}
