#include <stdio.h>
#include "par.h"

static int wirenum=0;


struct treenode *init_rt(struct circuit *circuit,struct matrix *matrix,
                         struct netnode *source,int srcnum)
{
  struct treenode *root;
  struct cellport *cell;
  struct iodef iodef;
  int r,c,sideind;

  root=malloc(sizeof(struct treenode));
  mcheck((char *)root);
  root->n=root->s=root->w=root->e=NULL;
  root->back=NULL; // Only happens for tree root!
// See if source is from a cell, or an edge
  if (source->edge==1){ // edge
    root->data=new_pqnode(NULL,
                          circuit->placedrow[source->ioindex],
                          circuit->placedcol[source->ioindex],
                          circuit->placedside[source->ioindex],
                          1,srcnum,NULL);
  } else { // normal cell output
// need to find [r,c]side in circuit via base, offset and rotation
    iodef=source->c->libdef->io[source->ioindex]; // def of this I/O port
    rotate_iodef(&iodef,source->c->libdef->h,source->c->libdef->w,
                 source->c->rotation);
    r=iodef.row + source->c->ulrow;
    c=iodef.col + source->c->ulcol;
    cell=&matrix->cells[r*matrix->width + c];
    switch(iodef.side[1]){
      case 'n': sideind=N;break;
      case 's': sideind=S;break;
      case 'w': sideind=W;break;
      case 'e': sideind=E;break;
    }
//    cell->visited[sideind]='1';
    root->data=new_pqnode(cell,r,c,iodef.side[1],
                          0,srcnum,NULL);
  }
// store backpointer
  root->data->rtnode=root;
  return(root);
}

// Build a new Priority Queue node
struct pqnode *new_pqnode(struct cellport *cell,int r, int c,
                          char side,int edge,
                          int source,struct pqnode *parent)
{
  struct pqnode *n;
  int sideind;

// Find side index
  switch(side){
    case 'n': sideind=N;break;
    case 's': sideind=S;break;
    case 'w': sideind=W;break;
    case 'e': sideind=E;break;
  }

  n=malloc(sizeof(struct pqnode));
  mcheck((char *)n);
  n->cost=0;
  n->pqcost=0;
  n->cell=cell;
  n->r=r;n->c=c;n->side=side;n->edge=(edge==1)?'1':' ';
  n->ss=source;
  n->parent=parent;
  n->next=NULL;
  n->rtnode=NULL; // pointer to routing tree
  return(n);
}

// Initialize a priority queue
init_pq(struct pqnode *queue,struct treenode *tree)
{
// Note that queue is a dummy head node - points to first real node
  if (tree==NULL) return;
  queue->next=malloc(sizeof(struct pqnode)); // new node
  mcheck((char *)queue->next);
  queue=queue->next; /* New node */
  queue->cost=tree->data->cost;
  queue->pqcost=0; // cost in PQ
  queue->rtnode=tree;
  queue->cell=tree->data->cell;
  queue->r=tree->data->r;
  queue->c=tree->data->c;
  queue->side=tree->data->side;
  queue->edge=tree->data->edge;
  queue->ss=tree->data->ss;
  queue->next=NULL;
  init_pq(queue,tree->n);
  init_pq(queue,tree->s);
  init_pq(queue,tree->w);
  init_pq(queue,tree->e); // if these are non-empty, they'll be traversed/added
}

// pop lowest-cost pqnode and return
struct pqnode *remove_pq_node(struct pqnode *queue)
{
  struct pqnode *ret;
// REMEMBER!!! queue is POINTER to initial node!!!
  if (queue->next==NULL) return(queue->next); // probably an error?
//  queue=queue->next; // this is the initial node
  ret=queue->next;  // queue is pointer to first node
  if (ret != NULL) queue->next=ret->next; // remove from linked list!
  return(ret);
}

struct treenode *newtreenode(struct pqnode *data)
{
  struct treenode *new;
  new=malloc(sizeof (struct treenode));
  mcheck((char *)new);
  new->n=new->s=new->w=new->e=new->back=NULL;
  new->data=data;
  new->backside=' ';
  return(new);
}

// Node fanout routines
static struct pqnode *fannoderet;

// If we're fanning out FROM an occupied cell, only go from a source
// If we're fanning INTO an occupied cell, only go to a sink
//
// ALSO: Don't fan OUT from a side where visited[] is set...

struct pqnode *next_fanout(int netnum,struct pqnode *fannode,
                           struct matrix *matrix,
                           int fancount)
{
  struct cellport *cell;
  int r,c,cost,visindex,edge,i,source,source2,edge2,viout;
  char side;
  int turn; // Extra cost penalty for turns

#ifdef DEBUG
printf("Fanning out: netnum=%d, fancount=%d. Current node:\n",
       netnum,fancount);
dump_pqnode(fannode);
#endif
  if ((fannode->edge=='1') && (fannode->ss!=netnum)){ // Edge source (input)
    return(NULL);
  }

  if (fannode->edge==' '){
    if (fannode->cell->occupied=='1'){ // only fanout from SOURCE side
      if ((fancount==N) && (fannode->edge==' ') &&
           ((fannode->cell->ss[DNO]!=netnum) ||
            (fannode->cell->edge[DNO]!=' '))) return(NULL);
      if ((fancount==S) && (fannode->edge==' ') &&
           ((fannode->cell->ss[DSO]!=netnum) ||
            (fannode->cell->edge[DSO]!=' '))) return(NULL);
      if ((fancount==W) && (fannode->edge==' ') &&
           ((fannode->cell->ss[DWO]!=netnum) ||
            (fannode->cell->edge[DWO]!=' '))) return(NULL);
      if ((fancount==E) && (fannode->edge==' ') &&
           ((fannode->cell->ss[DEO]!=netnum) ||
            (fannode->cell->edge[DEO]!=' '))) return(NULL);
      } // end of tests when current cell is occupied
    }

// Get information for cell we're fanning out TO

  if (fancount==N){ // check North
    if (fannode->r==0) return(NULL);
    r=fannode->r-1;c=fannode->c;side='s'; // input side
    cell=&(matrix->cells[r*matrix->width + c]);
    turn=(fannode->side=='s')?0:2;
    cost=(1+turn+cell->hcost[S])*cell->pcost[S];
    source=cell->ss[DSI];
    edge=cell->edge[DSI];
    visindex=S;
    viout=N;
  }

  if (fancount==S){ // check South
    if (fannode->r == matrix->height-1) return(NULL);
    r=fannode->r+1;c=fannode->c;side='n';
    cell=&matrix->cells[r*matrix->width + c];
    turn=(fannode->side=='n')?0:2;
    cost=(1+turn+cell->hcost[N])*cell->pcost[N];
    source=cell->ss[DNI];
    edge=cell->edge[DNI];
    visindex=N;
    viout=S;
  }

  if (fancount==W){ // check West
    if (fannode->c == 0) return(NULL);
    r=fannode->r;c=fannode->c-1;side='e';
    cell=&matrix->cells[r*matrix->width + c];
    turn=(fannode->side=='e')?0:2;
    cost=(1+turn+cell->hcost[E])*cell->pcost[E];
    source=cell->ss[DEI];
    edge=cell->edge[DEI];
    visindex=E;
    viout=W;
  }

  if (fancount==E){ // check East
    if (fannode->c == matrix->width-1) return(NULL);
    r=fannode->r;c=fannode->c+1;side='w';
    cell=&matrix->cells[r*matrix->width + c];
    turn=(fannode->side=='w')?0:2;
    cost=(1+turn+cell->hcost[W])*cell->pcost[W];
    source=cell->ss[DWI];
    edge=cell->edge[DWI];
    visindex=W;
    viout=E;
  }

// See if this node contains an edge output (sink)
  if ((fannode->edge != '1') && (cell->occupied=='1')){
    if (((cell->edge[DNO]=='1') && (cell->ss[DNO]==(-netnum))) ||
        ((cell->edge[DSO]=='1') && (cell->ss[DSO]==(-netnum))) ||
        ((cell->edge[DWO]=='1') && (cell->ss[DWO]==(-netnum))) ||
        ((cell->edge[DEO]=='1') && (cell->ss[DEO]==(-netnum)))){
      source=(-netnum);
      visindex=(-1); // flag!
    }
  }

  if (cell->occupied=='1'){ // we need to be driving a sink on this net!
    if (source!=(-netnum)) return(NULL);
  }

// We're OK so far - either fanning into an empty cell, or a desired sink
// See if we've been here before...
  if (visindex >= 0){
    if (cell->visited[visindex] != ' ') return(NULL); // Already been here!
// did we already fanout FROM this node/side?
/***
    if (fannode->edge != '1')
      if (fannode->cell->visited[viout]!=' ') return(NULL);
***/
    cell->visited[visindex]='1';
  } else { // mark entire node as visited!
    if ((cell->visited[N]!=' ') ||
        (cell->visited[S]!=' ') ||
        (cell->visited[W]!=' ') ||
        (cell->visited[E]!=' ')) return(NULL);
    cell->visited[N]=cell->visited[S]=cell->visited[W]=cell->visited[E]='1';
  }

// Now setup node to return
  fannoderet=malloc(sizeof (struct pqnode));
  mcheck((char *)fannoderet);
  fannoderet->cost=cost;
// pqcost will be set by caller
  fannoderet->cell=cell;
  fannoderet->r=r;
  fannoderet->c=c;
  fannoderet->side=side;
  fannoderet->edge=edge;
  fannoderet->ss=source;
  fannoderet->next=NULL;
  fannoderet->parent=fannode;	// node from which we fanned out
  fannoderet->rtnode=NULL;	// Not in RT, since we got here by fanning out
  return(fannoderet);
}

// add a node to the priority queue
add_pq_node(struct pqnode *queue,struct pqnode *node,int cost)
// here, cost is the PQ cost...
{
  struct pqnode *prev;

  node->pqcost=cost;
  prev=queue;
  queue=queue->next; // may be null

  while (queue != NULL){  // move down until we find proper spot
    if (queue->pqcost >= cost){ // insert before queue
      prev->next=node;node->next=queue;
      return;
    }
    prev=queue;queue=queue->next;
  }
// we've hit end of queue! Insert at back
   prev->next=node;node->next=NULL;
}

// Found a source - traceback and add new path to routing tree
backtrace(struct pqnode *sink)
{
  struct treenode *subtree;
struct pqnode *orig;orig=sink; // DEBUG

// Show the currently-discovered path
//  printf("[%d,%d]%c ",sink->r,sink->c,sink->side);

// While running (backwards) through the PQ nodes in this path,
// build a sub-routingtree from them.
// Continue until you reach a PQ node that's already part of the routing tree
// and join the subtree there.

// Can assume (???) that sink is not already in routing tree...
  subtree=malloc(sizeof(struct treenode));  // final leaf
  mcheck((char *)subtree);
  subtree->n=subtree->s=subtree->w=subtree->e=NULL;
  subtree->back=NULL;subtree->backside=' ';
  subtree->data=sink;
  sink->rtnode=subtree; // terminal node

  while (sink->parent != NULL){
// See if parent is in RT
    if (sink->parent->rtnode != NULL){  // join subtree to main routing tree
      switch(sink->side){
        case 'n':sink->parent->rtnode->n=subtree;break;
        case 's':sink->parent->rtnode->s=subtree;break;
        case 'w':sink->parent->rtnode->w=subtree;break;
        case 'e':sink->parent->rtnode->e=subtree;break;
        default: fprintf(stderr,"Backtracking PQ, but node's side is blank\n");
                 exit(1);
      }
// and set parent member
// %%% DO WE NEED TO DO THIS???
      sink->parent->parent=sink->parent->rtnode->data->parent;

//    printf("<-- [%d,%d]%c  ",sink->r,sink->c,sink->side);
//while(sink->parent!=NULL){
    //sink=sink->parent;
    //printf("<-- [%d,%d]%c  ",sink->r,sink->c,sink->side);
//}
//  printf("\n");
return;

// DID THAT REALLY WORK? %%%%%%%%%%%%%%%%%
    } else { // extend subtree (new parent)
      subtree->back=malloc(sizeof(struct treenode)); // backlink to parent
      mcheck((char *)subtree);
      switch(sink->side){ // link from parent to sub-node
        case 'n':subtree->backside='s';break;
        case 's':subtree->backside='n';break;
        case 'w':subtree->backside='e';break;
        case 'e':subtree->backside='w';break;
      }

// *** %%% Don't need back or backside???

      subtree->back->n=subtree->back->s=
      subtree->back->w=subtree->back->e=NULL;
      switch(sink->side){
        case 'n':subtree->back->n=subtree;break;
        case 's':subtree->back->s=subtree;break;
        case 'w':subtree->back->w=subtree;break;
        case 'e':subtree->back->e=subtree;break;
        default: fprintf(stderr,"Backtracking PQ, but node's side is blank\n");
      }
      subtree=subtree->back; // move to parent
      subtree->back=NULL;subtree->backside=' ';
      subtree->data=sink->parent;
      sink->parent->rtnode=subtree;
//%%%
    }
    sink=sink->parent;
//    printf("<-- [%d,%d]%c  ",sink->r,sink->c,sink->side);
  }
  //printf("\n");
// Check RT here in GDB!
}

add_treenode(struct treenode *root,char side,struct treenode *new)
{
  switch (side){
    case 'n':root->n=new;break;
    case 's':root->s=new;break;
    case 'w':root->w=new;break;
    case 'e':root->e=new;break;
    default:fprintf(stderr,"Unknown side <%c> in add_treenode",side);
  }
  new->back=root;
  new->backside=side;
}

//traceback(struct treenode *t)
//{
//  while (t != NULL){
//    printf("Data=[%d,%d]%c, came from %c of parent. ",
//      t->data->r,t->data->c,t->data->side,
//      t->backside);
//    t=t->back;
//  }
//}
//
//  struct treenode *t1,*t2,*t3;
//  t1=newtreenode(11); t2=newtreenode(2); t3=newtreenode(333);
//  add_treenode(t1,'s',t2);
//  add_treenode(t2,'w',t3);
//
//  traceback(t3);
//  traceback(t3);

clear_visited(struct matrix *matrix)
{
  int i;
  for (i=0;i<matrix->width*matrix->height;i++)
    matrix->cells[i].visited[N]=
    matrix->cells[i].visited[S]=
    matrix->cells[i].visited[W]=
    matrix->cells[i].visited[E]=' ';
}

set_visited(struct matrix *matrix, int r, int c, char side, int netnum)
{
  int i,ind;
  struct cellport *cell;

  i=r*matrix->width + c;
  cell=&matrix->cells[i];

  if (((cell->edge[DNO]=='1') && (cell->ss[DNO]==(-netnum))) ||
      ((cell->edge[DSO]=='1') && (cell->ss[DSO]==(-netnum))) ||
      ((cell->edge[DWO]=='1') && (cell->ss[DWO]==(-netnum))) ||
      ((cell->edge[DEO]=='1') && (cell->ss[DEO]==(-netnum)))){
    matrix->cells[i].visited[N]=
    matrix->cells[i].visited[S]=
    matrix->cells[i].visited[W]=
    matrix->cells[i].visited[E]='1';
  }

  switch (side){
    case 'n':ind=N;break;
    case 's':ind=S;break;
    case 'w':ind=W;break;
    case 'e':ind=E;break;
    default:fprintf(stderr,"Unknown side <%c> in set_visited\n",side);
            return;
  }
  i=r*matrix->width + c;
  matrix->cells[i].visited[ind]='1';
}

// sweep through the routing trees, see what's shared, and adjust costs
adjust_costs(struct matrix *matrix,struct treenode **routing_tree_array,
             int num_nets)
{
  int netnum,i;
  struct treenode *routing_tree;

// First, clear all the "counted" flags in the matrix structure
  for (i=0;i<matrix->height*matrix->width;i++){
    matrix->cells[i].counted[N]=
    matrix->cells[i].counted[S]=
    matrix->cells[i].counted[W]=
    matrix->cells[i].counted[E]=' '; // clear for upcoming count
    if (matrix->cells[i].occupied == ' ')
      matrix->cells[i].pcost[N]=matrix->cells[i].pcost[S]=
      matrix->cells[i].pcost[W]=matrix->cells[i].pcost[E]=1;
// penalty costs are counted (1=no conflict, including not-in-use)
// (historical costs are never reset)
  }

// now tally the usage of each cell input
  for (netnum=0;netnum<num_nets;netnum++){
    routing_tree=routing_tree_array[netnum];
    TnT(matrix,routing_tree); // Traverse 'n Tally
  }
}

// Traverse and Tally - sweep through the tree and add up node input use
TnT(struct matrix *matrix,struct treenode *routing_tree)
{
  struct cellport *cell;
  int i,count_index;

  if (routing_tree == NULL) return;
  cell=routing_tree->data->cell;
  if (cell != NULL){ // NULL means EDGE input - no tallying allowed/needed
   if (cell->occupied != '1'){
    switch(routing_tree->data->side){ // which side are we tallying?
      case 'n':count_index=N;break;
      case 's':count_index=S;break;
      case 'w':count_index=W;break;
      case 'e':count_index=E;break;
      default:fprintf(stderr,"RT node has undefined side <%c>\n",
                      routing_tree->data->side);
              count_index=0;
    }
    if (cell->counted[count_index]==' '){ // first use 
      cell->counted[count_index]='1';
// pcost remains at 1
    } else { // this is at least the second use
      switch (count_index){
        case N:cell->pcost[N]+=1;break;
        case S:cell->pcost[S]+=1;break;
        case W:cell->pcost[W]+=1;break;
        case E:cell->pcost[E]+=1;break;
      }
// Is this precisely the SECOND use?
      if (cell->counted[count_index]=='1'){ // yes - bump hcost
        cell->counted[count_index]='2'; // already seen at least 2 uses
        switch (count_index){
          case N:cell->hcost[N]+=1;break;
          case S:cell->hcost[S]+=1;break;
          case W:cell->hcost[W]+=1;break;
          case E:cell->hcost[E]+=1;break;
        }
      } // else we've already seen 2 nets, so leave hcost unchanged
    } // end of multi-use action
   } // else cell is occupied
  } // else edge input

// Now tally the subtrees
  TnT(matrix,routing_tree->n);
  TnT(matrix,routing_tree->s);
  TnT(matrix,routing_tree->w);
  TnT(matrix,routing_tree->e);
}

// clear all visited nodes, and set based on routing_tree
reload_visited(struct matrix *matrix,struct treenode *routing_tree,
               int netnum)
{
  struct treenode *root;

  clear_visited(matrix); // first clear all nodes
  root=routing_tree;
  TnSV(matrix,root,netnum);  // Traverse and Set node and children
}

TnSV(struct matrix *matrix,struct treenode *root,int netnum)
{
  if (root==NULL) return;
  set_visited(matrix, root->data->r,root->data->c,root->data->side,netnum);
  TnSV(matrix,root->n,netnum);
  TnSV(matrix,root->s,netnum);
  TnSV(matrix,root->w,netnum);
  TnSV(matrix,root->e,netnum);
}

// reset circuit structure for next routing sweep
// %%% DO WE REALLY WANT TO clear all the routing trees at once,
// or one at a time? Does it matter? Maybe not...
reset_matrix(struct matrix *matrix)
{
  int r,c;
  struct cellport *cellport;
  for (r=0;r<matrix->height;r++){
    for (c=0;c<matrix->width;c++){
      cellport=&matrix->cells[r*matrix->width + c];
      cellport->pcost[N]=1;
      cellport->pcost[S]=1;
      cellport->pcost[W]=1;
      cellport->pcost[E]=1; // was 0 - but 1 makes it route better!
// 0 seems to stay at 0, which nullifies hcost in cost calc...
      cellport->visited[N]=' ';
      cellport->visited[S]=' ';
      cellport->visited[W]=' ';
      cellport->visited[E]=' ';
      cellport->counted[N]=' ';
      cellport->counted[S]=' ';
      cellport->counted[W]=' ';
      cellport->counted[E]=' ';
    }
  }
}

FILE *xml; // evil global pointer for XML output
// we're not really intending for this module to be used long-term,
// so let's keep things simple...

// traverse all routes...
compute_routes(struct matrix *matrix,struct circuit *circuit,
            int num_nets,
            struct treenode **routing_tree_array)
{
  struct pqnode *pqnode;
  struct treenode *treenode,*root;
  int netnum,i,r,c,j,k;
  int num_inst;
  struct instance *instance;
  char buffer[1024];
  char newside;

// First, initialize the XML file
  sprintf(buffer,"%s.xml",circuit->name);
  xml=fopen(buffer,"w");
  if (xml <= (FILE*) NULL){
    fprintf(stderr,"ERROR: Cannot create output xml file <%s>\n",buffer);
    exit(1);
  }

// Output XML header
  fprintf(xml,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(xml,"<!DOCTYPE design SYSTEM \"");
  fprintf(xml,"./%s",circuit->name);
  fprintf(xml,"\">\n\n");
  fprintf(xml,"<design name=\"%s\" cellref=\"%s\" libref=\"work\">\n",
               circuit->name,circuit->name);
  fprintf(xml,"  <library name=\"MAIN.LIB\" type=\"technology-speccific\">\n");

// now let's find components

  num_inst=count_instances(circuit);
  for (i=0;i<num_inst;i++){
    instance=get_indexed_instance(circuit,i);
    fprintf(xml,"    <cell name=\"%s\">\n",instance->basename);
// Show I/O ports
    for (j=0;j<instance->libdef->numio;j++){
      fprintf(xml,"      <%s name=\"%s\" width=\"1\"/>\n",
             (instance->libdef->io[j].side[2]=='i')?"INPUT":"OUTPUT",
             instance->libdef->io[j].iobasename); 
    } // end of I/O for this cell
    fprintf(xml,"    </cell>\n");
  } // end of instances

// special ROUTING cell
  fprintf(xml,"    <cell name=\"WIRE\">\n");
  fprintf(xml,"      <INPUT name=\"in\" width=\"1\"/>\n");
  fprintf(xml,"      <OUTPUT name=\"out\" width=\"1\"/>\n");
  fprintf(xml,"    </cell>\n");

// end of main library
  fprintf(xml,"  </library>\n\n");

// Now start library block for circuit itself
  fprintf(xml,"  <library name=\"work\" type=\"user-defined\">\n");
  fprintf(xml,"    <cell name=\"%s\">\n\n",circuit->name);

// list inputs and outputs
  for (i=0;i<circuit->numio;i++){
    fprintf(xml,"      <%s name=\"%s\" width=\"1\"/>\n",
            (circuit->iodir[i]==0)?"INPUT":"OUTPUT",
            circuit->io[i]);
  }
  fprintf(xml,"\n");

// list instances next
  for (i=0;i<num_inst;i++){
    instance=get_indexed_instance(circuit,i);
    fprintf(xml,"      <inst name=\"cell_%d_%d\" cellref=\"%s\"/>\n",
            instance->ulrow,instance->ulcol,
            instance->basename);
  }

// need to output nets...but do that below, during routing sweep

// Initialize finalcells[] matrix
  matrix->finalcells=malloc(sizeof (struct finalcells)*
                            matrix->height*matrix->width);
  mcheck((char *)matrix->finalcells);
  for (i=0;i<matrix->height*matrix->width;i++){
    matrix->finalcells[i].nn=matrix->finalcells[i].ns=
    matrix->finalcells[i].nw=matrix->finalcells[i].ne=
    matrix->finalcells[i].sn=matrix->finalcells[i].ss=
    matrix->finalcells[i].sw=matrix->finalcells[i].se=
    matrix->finalcells[i].wn=matrix->finalcells[i].ws=
    matrix->finalcells[i].ww=matrix->finalcells[i].we=
    matrix->finalcells[i].en=matrix->finalcells[i].es=
    matrix->finalcells[i].ew=matrix->finalcells[i].ee=' ';
  }

  for (netnum=0;netnum<num_nets;netnum++){
    //printf("Net %d: ",netnum);
    root=routing_tree_array[netnum];
    if (root==NULL){
      printf("ERROR: netnum %d has null routing tree\n",netnum);
    } else {
      show_route(matrix,1,root->data->r,root->data->c,
                 root->data->side,root->data->edge,
                 root->n,circuit);
      show_route(matrix,1,root->data->r,root->data->c,
                 root->data->side,root->data->edge,
                 root->s,circuit);
      show_route(matrix,1,root->data->r,root->data->c,
                 root->data->side,root->data->edge,
                 root->w,circuit);
      show_route(matrix,1,root->data->r,root->data->c,
                 root->data->side,root->data->edge,
                 root->e,circuit);
      //printf("\n");
    }
  }


// need to output the output port connections...
  for (i=0;i<circuit->numio;i++){
    if (circuit->iodir[i]==1){ // output
      newside=circuit->placedside[i];
      r=circuit->placedrow[i];
      c=circuit->placedcol[i];
      fprintf(xml,"      <net name=\"wire%d\" ",++wirenum);
      fprintf(xml,"instref1=\"cell_%d_%d_%c\" portref1=\"out\" ",
              r,c,newside);
      fprintf(xml,"instref2=\"%s\" portref2=\"%s\"/>\n",
              circuit->name,circuit->io[i]);
    }
  }

// and output trailing XML
  fprintf(xml,"\n    </cell>\n  </library>\n</design>\n");
}

show_route(struct matrix *matrix,int first,int r,int c,char side,
           char edge,struct treenode *root,struct circuit *circuit)
{
  char outside,newside;
  int rr,cc;

  if (root==NULL) return;

  switch (root->data->side){
    case 'n':outside='s';break;
    case 's':outside='n';break;
    case 'w':outside='e';break;
    case 'e':outside='w';break;
  }

  if (first==1){ // should be source node
    if (edge == '1'){
      do_route(matrix,r,c,side,outside,circuit);
    } // else printf("([%d,%d]%c is source) ",r,c,side);
//printf("XML: [%d,%d]%c is edge source\n",r,c,side);
  }

  if (first==0) do_route(matrix,r,c,side,outside,circuit);

// see if root->data->[r,c] is an edge
  rr=root->data->r;
  cc=root->data->c;
  if (root==NULL) return;
  newside=' ';
  if (root->data->cell->edge[DNI]=='1') newside='n';
  if (root->data->cell->edge[DSI]=='1') newside='s';
  if (root->data->cell->edge[DWI]=='1') newside='w';
  if (root->data->cell->edge[DEI]=='1') newside='e';
  if (root->data->cell->edge[DNO]=='1') newside='n';
  if (root->data->cell->edge[DSO]=='1') newside='s';
  if (root->data->cell->edge[DWO]=='1') newside='w';
  if (root->data->cell->edge[DEO]=='1') newside='e';
  if (newside != ' '){
    do_route(matrix,rr,cc,root->data->side,newside,circuit);
    //printf("[%d,%d](EDGE)%c->%c ",rr,cc,root->data->side,newside);
  }

  if (root->data->cell->occupied=='1'){
    //printf("\nXML: Component port at [%d,%d]%c\n",r,c,outside);
// but not if "component" is on an edge...
  }

  show_route(matrix,0,root->data->r,root->data->c,
             root->data->side,root->data->edge,
             root->n,circuit);
  show_route(matrix,0,root->data->r,root->data->c,
             root->data->side,root->data->edge,
             root->s,circuit);
  show_route(matrix,0,root->data->r,root->data->c,
             root->data->side,root->data->edge,
             root->w,circuit);
  show_route(matrix,0,root->data->r,root->data->c,
             root->data->side,root->data->edge,
             root->e,circuit);
}

static char get_input_name_return[1024];

char *get_input_name(struct circuit *circuit,int r,int c)
{
  int i;
  for (i=0;i<circuit->numio;i++){
    if ((r==circuit->placedrow[i])&&
        (c==circuit->placedcol[i])){
      strcpy(get_input_name_return,circuit->io[i]);
      return(get_input_name_return);
    }
  }
}

do_route(struct matrix *matrix,int r,int c,char side1,char side2,
         struct circuit *circuit)
{
  int i;
  int r1,c1; // [r,c] for source cell
  char side; // side for source cell
  struct instance *instance;
  char buffer[1024];

  //printf("XML Routing:[%d %d]%c->%c ",r,c,side1,side2);
// show an instance for this cell (but have we done this already?)
  fprintf(xml,"\n      <inst name=\"cell_%d_%d_%c\" cellref=\"WIRE\"/>\n",
          r,c,side2);

// now show the net

  switch(side1){
    case 'w':r1=r;c1=c-1;side='e';break;
    case 'e':r1=r;c1=c+1;side='w';break;
    case 'n':r1=r-1;c1=c;side='s';break;
    case 's':r1=r+1;c1=c;side='n';break;
  }


  fprintf(xml,"      <net name=\"wire%d\" ",++wirenum);
  fprintf(xml,"instref1=\"cell_%d_%d_%c\" portref1=\"in\" ",
          r,c,side2);

// need to decide what this cell's INPUT is connected to...
  if (c1<0){ // WESTERN edge input
    fprintf(xml," instref2=\"%s\" portref2=\"%s\"/>\n",
            circuit->name, get_input_name(circuit,r1,0));
  } else if (c1>=circuit->width){ // EASTERN edge input
    fprintf(xml," instref2=\"%s\" portref2=\"%s\"/>\n",
            circuit->name, get_input_name(circuit,r1,circuit->width-1));
  } else if (r1<0){ // NORTHERN edge input
    fprintf(xml," instref2=\"%s\" portref2=\"%s\"/>\n",
            circuit->name, get_input_name(circuit,0,c1));
  } else if (r1>=circuit->height){ // EASTERN edge input
    fprintf(xml," instref2=\"%s\" portref2=\"%s\"/>\n",
            circuit->name, get_input_name(circuit,circuit->height-1,c1));
  } else {

// not an edge connection - but is it a component we're reading from?

    instance=getcell(circuit,r1,c1)->instance;
    if ((r1==0)||(c1==0)||(r1==circuit->height-1)||(c1==circuit->width-1)||
        (instance==NULL)){ // not a component
      fprintf(xml," instref2=\"cell_%d_%d_%c\" portref2=\"out\"/>\n",
              r1,c1,side);
    } else { // COMPONENT
// find output from [r1,c1] that feeds side "side1" (which is stored in "side")
// First, rotate "side" *backwards* (CCW)
      for (i=0;i<instance->rotation;i++){
        switch(side){
          case 'n':side='w';break;
          case 'w':side='s';break;
          case 's':side='e';break;
          case 'e':side='n';break;
        }
      }

// now find port on that side
      strcpy(buffer,"X");
      for (i=0;i<instance->libdef->numio;i++){
        if ((side == instance->libdef->io[i].side[1]) &&
            (instance->libdef->io[i].side[2]=='o')){
          strcpy(buffer,instance->libdef->io[i].iobasename);
        }
      }
      fprintf(xml," instref2=\"cell_%d_%d\" portref2=\"%s\"/>\n",
              r1,c1,buffer);
    }
  }

// last check - are we outputting to a component?
// If so, we need to output that component's INPUT wire here

  switch(side2){ // output side
    case 'w':r1=r;c1=c-1;side='e';break;
    case 'e':r1=r;c1=c+1;side='w';break;
    case 'n':r1=r-1;c1=c;side='s';break;
    case 's':r1=r+1;c1=c;side='n';break;
  }

  instance=getcell(circuit,r1,c1)->instance;
  if ((r1>0)&&(c1>0)&&(r1<circuit->height-1)&&(c1<circuit->width-1)&&
      (instance!=NULL)){ // component!

    fprintf(xml,"      <net name=\"wire%d\" ",++wirenum);
    fprintf(xml,"instref1=\"cell_%d_%d_%c\" portref1=\"out\" ",
            r,c,side2);

// Again, rotate "side" *backwards* (CCW)
    for (i=0;i<instance->rotation;i++){
      switch(side){
        case 'n':side='w';break;
        case 'w':side='s';break;
        case 's':side='e';break;
        case 'e':side='n';break;
      }
    }

// now find port on that side
    strcpy(buffer,"X");
    for (i=0;i<instance->libdef->numio;i++){
      if ((side == instance->libdef->io[i].side[1]) &&
          (instance->libdef->io[i].side[2]=='i')){
        strcpy(buffer,instance->libdef->io[i].iobasename);
      }
    }
    fprintf(xml," instref2=\"cell_%d_%d\" portref2=\"%s\"/>\n",
            r1,c1,buffer);
  }
  //fprintf(xml,"[%d,%d]%c COMP!\n",r1,c1,side);

  i=r*matrix->width + c; // index into finalcells[]
  if ((side1=='n')&&(side2=='n')) matrix->finalcells[i].nn='1';
  if ((side1=='n')&&(side2=='s')) matrix->finalcells[i].ns='1';
  if ((side1=='n')&&(side2=='w')) matrix->finalcells[i].nw='1';
  if ((side1=='n')&&(side2=='e')) matrix->finalcells[i].ne='1';
  if ((side1=='s')&&(side2=='n')) matrix->finalcells[i].sn='1';
  if ((side1=='s')&&(side2=='s')) matrix->finalcells[i].ss='1';
  if ((side1=='s')&&(side2=='w')) matrix->finalcells[i].sw='1';
  if ((side1=='s')&&(side2=='e')) matrix->finalcells[i].se='1';
  if ((side1=='w')&&(side2=='n')) matrix->finalcells[i].wn='1';
  if ((side1=='w')&&(side2=='s')) matrix->finalcells[i].ws='1';
  if ((side1=='w')&&(side2=='w')) matrix->finalcells[i].ww='1';
  if ((side1=='w')&&(side2=='e')) matrix->finalcells[i].we='1';
  if ((side1=='e')&&(side2=='n')) matrix->finalcells[i].en='1';
  if ((side1=='e')&&(side2=='s')) matrix->finalcells[i].es='1';
  if ((side1=='e')&&(side2=='w')) matrix->finalcells[i].ew='1';
  if ((side1=='e')&&(side2=='e')) matrix->finalcells[i].ee='1';
}

dump_pqnode(struct pqnode *node)
{
if (node==NULL) {printf("Node is null\n");return;}
  printf("NODE: Cost=%d pqcost=%d [%d,%d]%c edge=%c\n",node->cost,node->pqcost,
          node->r,node->c,node->side,node->edge);
  
  if (node->cell != NULL){
    printf("node->cell->SS[NSWEI NSWEO]=%d %d %d %d %d %d %d %d\n",
	node->cell->ss[DNI], node->cell->ss[DSI],
	node->cell->ss[DWI], node->cell->ss[DEI],
	node->cell->ss[DNO], node->cell->ss[DSO],
	node->cell->ss[DWO], node->cell->ss[DEO]);
  }
}

/*
 * free_pq()
 * Sweep the queue, and release memory for any entries not in RT
 */
free_pq(struct pqnode *node)
{
  struct pqnode *next,*this;

  this=node->next;
  while (this != NULL){
    next=this->next; /* Points to next node */
  //  if (this->rtnode == NULL){
      free(this);
  //  }

    this=next; // Point to next node
  }
}

free_rt(struct treenode *root)
{
  struct treenode *n, *s, *w, *e, *back;
  if (root==NULL) return;
  free_rt(root->n);
  free_rt(root->s);
  free_rt(root->w);
  free_rt(root->e);
  free(root->data); // pqnode
  free(root);
}

/* Add a node to the pqnode_list structure */
add_pqnode_list(struct pqnode_list *pqnode_list,
                struct pqnode *pqnode)
{
   struct pqnode_list *new;

   new=malloc(sizeof (struct pqnode_list));
   mcheck((char *)new);
   new->node=pqnode;
   new->next=pqnode_list->next;
   pqnode_list->next=new;
}


/* Free the pqnode_list structure */
free_pqnode_list(struct pqnode_list *pqnode_list)
{
  struct pqnode_list *tmp,*tmp2;

  tmp=tmp2=pqnode_list->next;
  while (tmp != NULL){
    if (tmp->node->rtnode==NULL) free(tmp->node);
    tmp=tmp->next; // Next node
    free(tmp2);  // release this pqnode_list node
    tmp2=tmp;
  }
  pqnode_list->next=NULL;
}

// evil static vars for computing statistics
static struct stat_block stat_ret;

// Calculate path lengths etc.
struct stat_block *compute_stats(struct circuit *circuit,
              struct matrix *matrix,
              int num_nets,
              struct treenode **routing_tree_array)
{
  struct pqnode *pqnode;
  struct treenode *treenode,*root;
  int netnum,i,r,c,j;

// Clear stats
  stat_ret.Glen_str=stat_ret.Glen_str_n=stat_ret.Glen_turn=
  stat_ret.Glen_turn_n=stat_ret.Glen_sum=stat_ret.Glen_sum_n=0;
  stat_ret.Mlen_str=stat_ret.Mlen_turn=stat_ret.Mlen_sum=(-1);

// Now trace path from each edge input...
  for (i=0;i<circuit->numio;i++){
    if (circuit->iodir[i]==0){ // edge source: trace to edge out

// Clear the COUNTED structure
      for (j=0;j<circuit->height*circuit->width;j++){
        circuit->matrix[j].counted[N]=
        circuit->matrix[j].counted[S]=
        circuit->matrix[j].counted[W]=
        circuit->matrix[j].counted[E]=' ';
      }

      trace_path(circuit,circuit->placedrow[i],
          circuit->placedcol[i],
          circuit->placedside[i],
          matrix->finalcells,0,0,0);
    }
  } // All edge IO done. Show statistics
  return(&stat_ret);
}

// trace path from given [r,c]side, and update len etc.
trace_path(struct circuit *circuit,
           int r,int c,char side,
           struct finalcells *fc,
           int len_str,int len_turn,int len_sum)
// three lengths are tallied: straight-wires, turns, and total
// If components are instantiated in the path, use the MAX values
// for tallying these lengths
{
  int h,w;
  int index,i,row,col,oldr,j,height,width,temp;
  struct instance *instance;
  char s;

  h=circuit->height;w=circuit->width;

//printf("[%d,%d]%c<%d %d %d>",r,c,side,len_str,len_turn,len_sum);
  index=r*w+c; // Index of current cell routing

/*** CHECK FOR EDGE HIT (DONE) ***/
  if ((r<0)||(r==h)||(c<0)||(c==w)){
// Hit an edge - tally statistics for this IN->OUT path
    stat_ret.Glen_str+=len_str;++stat_ret.Glen_str_n;
    stat_ret.Glen_turn+=len_turn;++stat_ret.Glen_turn_n;
    stat_ret.Glen_sum+=len_sum;++stat_ret.Glen_sum_n;

// Check for new max
    if (len_str > stat_ret.Mlen_str) stat_ret.Mlen_str=len_str;
    if (len_turn > stat_ret.Mlen_turn) stat_ret.Mlen_turn=len_turn;
    if (len_sum > stat_ret.Mlen_sum) stat_ret.Mlen_sum=len_sum;

    //printf("Hit edge! <%d %d %d>\n\n",len_str,len_turn,len_sum);
    return;
  }

// See if we've been here before
// Compute side index
  switch(side){
    case 'n':i=N;break;
    case 's':i=S;break;
    case 'w':i=W;break;
    case 'e':i=E;break;
  }
  if (circuit->matrix[index].counted[i] != ' ') return; // Already been here
  circuit->matrix[index].counted[i] = '1'; // Mark this

// otherwise, walk this path one more step and recurse as necessary
  if (side=='w'){
    if (fc[index].we=='1'){
      //printf("we.");
      trace_path(circuit,r,c+1,'w',fc,
                 len_str+1,len_turn,len_sum+1);
    }
    if (fc[index].wn=='1'){
      //printf("wn.");
      trace_path(circuit,r-1,c,'s',fc,
                 len_str,len_turn+1,len_sum+1);
    }
    if (fc[index].ws=='1'){
      //printf("ws.");
      trace_path(circuit,r+1,c,'n',fc,
                 len_str,len_turn+1,len_sum+1);
    }
  }

  if (side=='n'){
    if (fc[index].ne=='1'){
      //printf("ne.");
      trace_path(circuit,r,c+1,'w',fc,
                 len_str,len_turn+1,len_sum+1);
    }
    if (fc[index].nw=='1'){
      //printf("nw.");
      trace_path(circuit,r,c-1,'e',fc,
                 len_str,len_turn+1,len_sum+1);
    }
    if (fc[index].ns=='1'){
      //printf("ns.");
      trace_path(circuit,r+1,c,'n',fc,
                 len_str+1,len_turn,len_sum+1);
    }
  }
  
  if (side=='s'){
    if (fc[index].se=='1'){
      //printf("se.");
      trace_path(circuit,r,c+1,'w',fc,
                 len_str,len_turn+1,len_sum+1);
    }
    if (fc[index].sn=='1'){
      //printf("sn.");
      trace_path(circuit,r-1,c,'s',fc,
                 len_str+1,len_turn,len_sum+1);
    }
    if (fc[index].sw=='1'){
      //printf("sw.");
      trace_path(circuit,r,c-1,'e',fc,
                 len_str,len_turn+1,len_sum+1);
    }
  }

  if (side=='e'){
    if (fc[index].ew=='1'){
      //printf("ew.");
      trace_path(circuit,r,c-1,'e',fc,
                 len_str+1,len_turn,len_sum+1);
    }
    if (fc[index].en=='1'){
      //printf("en.");
      trace_path(circuit,r-1,c,'s',fc,
                 len_str,len_turn+1,len_sum+1);
    }
    if (fc[index].es=='1'){
      //printf("es.");
      trace_path(circuit,r+1,c,'n',fc,
                 len_str,len_turn+1,len_sum+1);
    }
  }

/*** SEE IF THERE'S A COMPONENT HERE, AND IF SO, ***/
/*** FAN OUT FROM ALL OUTPUTS ***/

  instance=circuit->matrix[index].instance;
  if ((instance != NULL) && (instance != EDGEINST)){
    //printf("Hit component: %dx%d rot=%d\nIO: ",
      //instance->libdef->h, instance->libdef->w,
      //instance->rotation);
    for (i=0;i<instance->libdef->numio;i++){
      //printf("I/O %d(%s) [%d,%d]%s ",
       //i,instance->libdef->io[i].iobasename,
       //instance->libdef->io[i].row,
       //instance->libdef->io[i].col,
       //instance->libdef->io[i].side);

// See if this is an output.
// If so, rotate, add [R,C] to base, and traverse that path
// after adding component's len values

// NOTE that rotating involves adjusting [R,C], and the side,
// as well as a local notion of HxW (since W is used to
// rotate [R,C])

      if (instance->libdef->io[i].side[2]=='o'){ // output`
// rotate [row,col]
        row=instance->libdef->io[i].row;
        col=instance->libdef->io[i].col;
        s=instance->libdef->io[i].side[1];
        height=instance->libdef->h;
        width=instance->libdef->w;

        for (j=0;j<instance->rotation;j++){
          oldr=row;
          row=col;
          col=(height-1)-oldr;

          temp=width;width=height;height=temp;

// and rotate side
          switch(s){
            case 'n':s='e';break;
            case 'e':s='s';break;
            case 's':s='w';break;
            case 'w':s='n';break;
          }
        } // [r,c]side are set now. Add base
        col+=instance->ulcol;row+=instance->ulrow;

//printf("post-rotate: OUTPUT at [%d,%d]%c\n",row,col,s);
// Now traverse the net from this output
        switch(s){
          case 'n':trace_path(circuit,row-1,col,'s',fc,
                              len_str+instance->libdef->stats.Mlen_str,
                              len_turn+instance->libdef->stats.Mlen_turn,
                              len_sum+instance->libdef->stats.Mlen_sum);
                   break;
          case 's':trace_path(circuit,row+1,col,'n',fc,
                              len_str+instance->libdef->stats.Mlen_str,
                              len_turn+instance->libdef->stats.Mlen_turn,
                              len_sum+instance->libdef->stats.Mlen_sum);
                   break;
          case 'w':trace_path(circuit,row,col-1,'e',fc,
                              len_str+instance->libdef->stats.Mlen_str,
                              len_turn+instance->libdef->stats.Mlen_turn,
                              len_sum+instance->libdef->stats.Mlen_sum);
                   break;
          case 'e':trace_path(circuit,row,col+1,'w',fc,
                              len_str+instance->libdef->stats.Mlen_str,
                              len_turn+instance->libdef->stats.Mlen_turn,
                              len_sum+instance->libdef->stats.Mlen_sum);
                   break;
        }
      } // end of this output
    } // end of all I/O
    //printf("\n");
  } // end of instance
}
