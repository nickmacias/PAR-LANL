edif ::=
	'(''edif' edifFileNameDef
	   edifVersion
	   edifLevel
	   keywordMap
	   { <status > | external | library | design |
	   comment | userdata }
	')'

acload ::=
	'(''acload' 
	 (miNoMaxValue | miNoMaxDisplay)
	')'

after ::=
	'(''after'
	     miNoMaxValue
         { logicAssign | follow | maintain | comment | userData }
	')'

annotate ::=
	'(''annotate'
	   ( stringValue | stringDisplay )
	')'

apply ::=
	'(''apply'
	 cycle
	 { logicInput | logicOutput | comment | userData }
	 ')'

arc ::=
	'(''arc' pointValue pointValue pointValue')'

array ::=
	'(''array' nameDef integerValue {integerValue} ')'

arrayMacro ::=
	'(''arrayMacro' plug')'

arrayRelatedInfo ::=
	'(''arrayRelatedInfo'
	     (baseArray | arraySite | arrayMacro)
	     {comment | userData }
	')'

arraySite ::=
	'(''arraySite' socket')'

atLeast ::=
	'(''atLeast' numberValue')'

atMost ::=
	'(''atmost' numberValue')'

author ::=
	'(''author' stringToken')'

baseArray ::=
	'(''baseArray'')'

becomes ::=
	'(''becomes' (logicNameRef | logicList | logicOneof) ')'

between ::=
	'(''between'
	    (atLeast | greaterThan)
	    (atMost | lessThan)
	')'

boolean ::=
	'(''boolean'
	    {booleanValue | booleanDisplay | boolean}
	')'

booleandisplay ::=
	'(''booleandisplay' booleanValue {display} ')'

booleanMap ::=
	'(''booleanMap' booleanValue ')'

booleanValue ::=
	false | true

borderPattern ::=
	'(''borderPattern'
	    integerValue
	    integerValue
	    boolean
	')'

borderWidth ::=
	'(''borderWidth' integerValue ')'

boundingBox ::=
	'(''boundingBox' rectangle ')'

cell ::=
	'(''cell' cellNameDef
	     cellType
	     {<status> | <viewMap> | view |
     	     comment | userData | property }
	')'

cellNameDef ::=
	nameDef

cellNameRef ::=
	nameRef

cellRef ::=
	'(''cellRef' cellNameRef [libraryRef]')'

cellType ::=
	'(''cellType'
	     ('TIE' | 'RIPPER' | 'GENERIC')
	')'

change ::=
	'(''change'
	    (portNameRef | portRef | portList)
	    [transition | becomes]
	')'

circle ::=
	'(''circle' pointValue pointValue {property} ')'

color ::=
	'(''color' scaledinteger scaledinteger scaledinteger ')'

comment ::=
	'(''comment' { stringToken } ')'

commentGraphics ::=
	'(''commentGraphics'
	   { annotate | figure | instance | < boundingBox > |
	   property | comment | userData }
	')'

compound ::=
	'(''compound' { logicNameRef } ')'

connectLocation ::=
	'(''connectLocation' {figure } ')'

contents ::=
	'(''contents'
	   { instance | offPageConnector | figure |
	   section | net | netBundle | page | commentGraphics |
	   portImplementation | timing | simulate | when | follow |
	   logicPort | < boundingBox > | comment | userData }
	')'

cornerType ::=
	'(''cornerType' ( 'EXTEND' | 'TRUNCATE' | 'ROUND' ) ')'


criticality ::=
	'(''criticality' (integerValue | integerDisplay) ')'

currentMap ::=
	'(''currentMap' MinomaxValue ')'

curve ::=
	'(''curve' {arc | pointValue } ')'

cycle ::=
	'(''cycle' integerValue [duration] ')'

dataOrigin ::=
	'(''dataOrigin' stringToken [ version ] ')'

dcFaninLoad ::=
	'(''dcFaninLoad'
	    (numberValue | numberDisplay)
	')'

dcFanoutLoad ::=
	'(''dcFanoutLoad'
	    (numberValue | numberDisplay)
	')'

dcMaxFanin ::=
	'(''dcMaxFanin'
	    (numberValue | numberDisplay)
	')'

dcMaxFanout ::=
	'(''dcMaxFanout'
	    (numberValue | numberDisplay)
	')'

delay ::=
	'(''delay'
	    (miNoMaxValue | miNoMaxDisplay)
	')'

delta ::=
	'(''delta' { pointValue } ')'

derivation ::=
	'(''derivation'
	   ('CALCULATED' | 'MEASURED' | 'REQUIRED')
	')'

design ::=
	'(''design' designNameDef
	    cellRef
	    { < status > | comment | property | userdata }
	')'

designator ::=
	'(''designator'
	    (stringValue | stringDisplay)
	')'

designNameDef ::=
	NameDef

difference ::=
	'(''difference'
	   ( figureGroupRef | figureOp )
	   { figureGroupRef | figureOp }
	')'

direction ::=
	'(''direction'
	    ('INPUT' | 'OUTPUT' | 'INOUT' )
	')'

display ::=
	'(''display'
	   (figureGroupNameRef | figureGroupOverride)
	   [ justify ]
	   [ orientation ]
	   [ origin ]
	')'

dominates ::=
	'(''dominates' {logicNameRef} ')'

dot ::=
	'(''dot' pointValue { property }')'

duration ::=
	'(''duration' numberValue ')'

e ::=
	'(''e' integerToken integerToken ')'

ediffileNameDef ::=
	NameDef

edifLevel ::=
	'(''edifLevel' integerToken ')'

edifVersion ::=
	'(''edifVersion' integerToken integerToken integerToken ')'

enclosureDistance ::=
	'(''enclosureDistance' ruleNameDef
	     figureGroupObject
	     figureGroupObject
	     (range | singleValueSet)
	     { comment | userData }
	')'

endType ::=
	'(''endType' ( 'EXTEND' | 'ROUND' | 'TRUNCATE' ) ')'

entry ::=
	'(''entry'
	    (match | change | steady)
	    (logicRef | portRef | noChange | table)
	    [ delay | loadDelay ]
	')'

event ::=
	'(''event'
	   (portRef | portList | portGroup | netRef | netGroup)
	   { transition | becomes }
	')'

exactly ::=
	'(''exactly' numberValue ')'

external ::=
	'(''external' libraryNameDef
	    edifLevel
	    technology
	    {< status > | cell | comment | userData }
	')'

fabricate ::=
	'(''fabricate' layerNameDef figureGroupNameRef ')'

false ::=
	'(''false'')'

figure ::=
	'(''figure'
	    (figureGroupNameRef | figureGroupOverride)
  	    { circle |	dot | openShape | path | polygon | rectangle | shape |
	    comment | userData }
	')'

figureArea ::=
	'(''figureArea' ruleNameDef
	     figureGroupObject
	     (range | singleValueSet)
	     { comment | userData }
	')'

figureGroup ::=
	'(''figureGroup' figureGroupNameDef
	    { < cornerType > | < endType > | < pathWidth > | < borderWidth > |
	    < color > | < fillPattern > | < borderPattern > | < textHeight > | 
	    <visible> | includeFigureGroup | comment | property | userData }
	')'

figuregroupNameDef ::=
	NameDef

figuregroupNameRef ::=
	NameRef

figureGroupObject ::=
	'(''figureGroupObject'
	    (figureGroupNameRef | figureGroupRef | figureOp)
	')'

figureGroupOverride ::=
	'(''figureGroupOverride' figureGroupNameRef
	     { < cornerType > | < endType > |
	     < pathWidth > | < borderWidth > |
	     < color > | < fillPattern  > | 
	     < borderPattern > | < textHeight > |
	     < visible > | comment | property | userData }
	')'

figureGroupRef ::=
	'(''figureGroupRef' figureGroupNameRef [ libraryRef ]')'

figureOp ::=
	intersection | union | difference | inverse | oversize

figurePerimeter ::=
	'(''figurePerimeter' ruleNameDef
	    figureGroupObject
	    (range | singleValueSet)
	    { comment | userData }
	')'

figureWidth ::=
	'(''figureWidth' ruleNameDef
	    figureGroupObject
	    (range | singleValueSet)
	    { comment | userData }
	')'

fillPattern ::=
	'(''fillPattern' integerValue integerValue boolean ')'

follow ::=
	'(''follow'
	    (portNameRef | portRef)
	    (portRef | table)
	    [ delay | loadDelay ]
	')'

forbiddenEvent ::=
	'(''forbiddenEvent' timeInterval { event } ')'

form ::=
	   edif | acload | after | annotate | apply |
           arc | array | arrayMacro | arrayRelatedInfo |
           arraySite | atLeast | atmost | author | 
           baseArray | becomes | between | boolean |
           booleandisplay | booleanMap | borderPattern |
           borderWidth | boundingBox | cell | cellRef |
           cellType | change | circle | color | comment |
           commentGraphics | compound | connectLocation |
           contents | cornerType | criticality | currentMap |
           curve | cycle | dataOrigin | dcFaninLoad |
           dcFanoutLoad | dcMaxFanin | dcMaxFanout |
           delay | delta | derivation | design | designator |
           difference | direction | display | dominates |
           dot | duration | e | edifLevel | edifVersion |
           enclosureDistance | endType | entry | event |
           exactly | external | fabricate | false |
           figure | figureArea | figureGroup | figureGroupObject |
           figureGroupOverride | figureGroupRef | figurePerimeter |
           figureWidth | fillPattern | follow | forbiddenEvent |
           globalPortRef | greaterThan |
           gridMap | ignore | includeFigureGroup | initial |
           instance | instanceBackAnnotate | instanceGroup |
           instanceMap | instanceRef | integer | integerDisplay |
           interface | interFigureGroupSpacing | intersection |
           intraFigureGroupSpacing | inverse | isolated |
           joined | justify | keywordDisplay | keywordLevel |
           keywordMap | lessThan | library | libraryRef |
           listOfNets | listOfPorts | loadDelay | logicAssign |
           logicInput | logicList | logicMapInput | logicMapOutput |
           logicOneOf | logicOutput | logicPort | logicRef |
           logicValue | logicWaveform | maintain | match |
           member | minomax | minomaxDisplay | mnm |
           multipleValueSet | mustJoin | name | net |
           netBackAnnotate | netBundle | netDelay | netGroup |
           netMap | netRef | noChange | nonPermutable |
           notAllowed | notchSpacing | number | numberDefinition |
           numberDisplay | offPageConnector | offsetEvent |
           openShape | orientation | origin | overhangDistance |
           overlapDistance | oversize | owner | page |
           pageSize | parameter | parameterAssign |
           parameterDisplay | path | pathDelay | pathWidth |
           permutable | physicalDesignRule | plug |
           point | pointDisplay | pointList | polygon |
           port | portBackAnnotate | portBundle | portDelay |
           portGroup | portImplementation | portInstance |
           portList | portListAlias | portMap | portRef |
           program | property | propertyDisplay |
           protectionFrame | pt | rangeVector | rectangle |
           rectangleSize | rename | resolves | scale |
           scaleX | scaleY | section | shape | simulate |
           simulationInfo | singleValueSet | site | socket |
           socketSet | status | steady | string |
           stringDisplay | strong | symbol | symmetry |
           table | tableDefault | technology | textHeight |
           timeInterval | timeStamp | timing | transform |
           transition | trigger | true | unconstrained |
           undefined | union | unit | unused | userData |
           version | view | viewList | viewMap | viewRef |
           viewType | visible | voltageMap | waveValue |
           weak | weakJoined | when | written 

globalPortRef ::=
	'(''globalPortRef' portNameRef')'

greaterThan ::=
	'(''greaterThan' numberValue')'

gridMap ::=
	'(''gridMap' numberValue numberValue ')'

ignore ::=
	'(''ignore'')'

includeFiguregroup ::=
	'(''includeFigureGroup'
	    (figureGroupRef | figureOp)
	')'

initial ::=
	'(''initial'')'

instance ::=
	'(''instance'
	     instanceNameDef
	     (viewRef | viewList)
	     { < transform > | parameterAssign | portInstance |
	     < designator > | timing | property | comment | userData }
	')'

instanceBackAnnotate ::=
	'(''instanceBackAnnotate' instanceRef
	    { < designator > | timing | property | comment }
	')'

instanceGroup ::=
	'(''instanceGroup' {instanceRef }')'

instanceMap ::=
	'(''instanceMap'
	    { instanceRef | instanceGroup | comment | userData }
	')'

instanceNameDef ::=
	NameDef | array

instanceNameRef ::=
	NameRef | member

instanceRef ::=
	'(''instanceRef' instanceNameRef [ instanceRef | viewRef ]
	')'

integer ::=
	'(''integer' {integerValue | integerDisplay | integer} ')'

integerDisplay ::=
	'(''integerDisplay' integerValue { display } ')'

integerValue ::=
	integerToken

interface ::=
	'(''interface'
	     { port | portBundle | < symbol > | < protectionFrame > |
	     <arrayRelatedInfo > | parameter | joined | mustJoin | 
	     weakJoined | permutable | timing | < designator > | 
	     simulate | property | comment | userData }
	 ')'

interFiguregroupSpacing ::=
	'(''interFigureGroupSpacing' ruleNameDef
	     figureGroupObject
	     figureGroupObject
	     (range | singleValueSet)
	     { comment | userData }
	')'

intersection ::=	
	'(''intersection'
	     ( figureGroupRef | figureOp )
	     { figureGroupRef | figureOp }
	')'

intraFigureGroupSpacing ::=
	'(''intraFigureGroupSpacing' ruleNameDef
	     figureGroupObject
	     (range | singleValueSet)
	     { comment | userData }
	')'

inverse ::=
	'(''inverse'
	     (figuregroupRef | figureOp)
	')'

isolated ::=
	'(''isolated'')'

joined ::=
	'(''joined'
	     { portRef | portList | globalPortRef }
	')'

justify ::=
	'(''justify'
	     ('UPPERLEFT' | 'UPPERCENTER' | 'UPPERRIGHT' |
	     'CENTERLEFT' | 'CENTERCENTER' | 'CENTERRIGHT' |
	     'LOWERLEFT' | 'LOWERCENTER' | 'LOWERRIGHT')
	')'

keywordDisplay ::=
	'(''keywordDisplay' keywordNameRef
	    { display }
	')'

keywordLevel ::=
	'(''keywordLevel' integerToken ')'

keywordMap ::=
	'(''keywordMap' keywordLevel { comment } ')'

keywordNameRef ::=
	identifier

layerNameDef ::=
	NameDef

lessThan ::=
	'(''lessThan' numberValue')'

library ::=
	'(''library' libraryNameDef
	     edifLevel
	     technology
	     { < status > | cell | comment | userData }
	')'

LibraryNameDef ::=
	NameDef

LibraryNameRef ::=
	NameRef

libraryRef ::=
	'(''libraryRef' libraryNameRef')'

listOfNets ::=
	'(''listOfNets' {net} ')'

listOfPorts ::=
	'(''listOfPorts' { port | portBundle } ')'

loadDelay ::=
	'(''loadDelay'
	    (miNoMaxValue | miNoMaxDisplay)
	    (miNoMaxValue | miNoMaxDisplay)
	')'

logicAssign ::=
	'(''logicAssign'
	( portNameRef | portRef )
	( portRef | logicRef | table)
	[ delay | loadDelay ]
	')'

logicInput ::=
	'(''logicInput'
	    (portList | portRef | portNameRef)
	    logicWaveform
	')'

logicList ::=
	'(''logicList' { logicNameRef | logicOneOf | ignore } ')'

logicMapInput ::=
	'(''logicMapInput' { logicRef } ')'

logicMapOutput ::=
	'(''logicMapOutput' { logicRef } ')'

logicNameDef ::=
	nameDef

logicNameRef ::=
	nameRef

logicOneOf ::=
	'(''logicOneOf' { logicnameRef | logicList } ')'

logicOutput ::=
	'(''logicOutput'
	    (portList | portRef | portNameRef)
	    logicWaveform
	')'

logicPort ::=
	'(''logicPort' portNameDef 
	   { property | comment | userData } 
	')'

logicRef ::=
	'(''logicRef' logicNameRef [ libraryRef ]')'

logicValue ::=
	'(''logicValue' logicNameDef
	    { < voltageMap > | < currentMap > | < booleanMap > |
	    < compound > | < weak > | < strong > | < dominates > |
	    < logicMapOutput > | < logicMapInput > | <isolated> |
	    resolves | property | comment | userData }
	')'

logicWaveform ::=
	'(''logicWaveform'
	    { logicnameRef | logicList | logicOneOf | ignore }
	')'

maintain ::=
	'(''maintain'
	     (portNameRef | portRef)
	     [ delay | loadDelay ] 
	')'

match ::=
	'(''match'
	    (portNameRef | portRef | portList)
	    (logicNameRef | logicList | logicOneOf)
	')'

member ::=
	'(''member' nameRef integerValue { integerValue } ')'

MinoMax ::=
	'(''minomax' {minomaxValue | minomaxDisplay | minomax} ')'

MinoMaxDisplay ::=
	'(''minomaxDisplay' minomaxValue {display} ')'

MinoMaxValue ::=
	mnm | numberValue

mnm ::=
	'(''mnm'
	 (numberValue | undefined | unconstrained)
	 (numberValue | undefined | unconstrained)
	 (numberValue | undefined | unconstrained)
    ')'

multipleValueSet ::=
	'(''multipleValueSet' { rangeVector } ')'

mustJoin ::=
	'(''mustJoin'
	    { portRef | portList | weakJoined | joined }
	')'

name ::=
	'(''name' Identifier { display } ')'

nameDef ::=
	identifier | name | rename

nameRef ::=
	identifier | name

net ::=
	'(''net' netnamedef
	      joined
	      { < criticality > | netDelay | figure | net | 
		instance | commentGraphics | property | comment | userData }
	')'

netBackAnnotate ::=
	'(''netBackAnnotate' netRef
	    {netDelay | <criticality> | property | comment }
	')'

netBundle ::=
	'(''netBundle' netNameDef
	    listOfNets
	    { figure | property | commentgraphics | comment | userData }
	')'

netDelay ::=
	'(''netDelay' derivation delay { transition | becomes } ')'

netgroup ::=
	'(''netGroup' { netRef | netNameRef } ')'

netMap ::=
	'(''netMap'
	    { netRef | netGroup | comment | userData }
	')'

NetNameDef ::=
	nameDef | array

NetNameRef ::=
	nameRef | member

netRef ::=
	'(''netRef' netNameRef 
	    [ netRef | instanceRef | viewRef ]
	')'

noChange ::=
	'(''noChange'')'

nonPermutable ::=
	'(''nonPermutable' { portRef | permutable } ')'

notAllowed ::=
	'(''notAllowed' ruleNameDef
	    figureGroupObject
	    { comment | userData }
	')'

notchSpacing ::=
	'(''notchSpacing' ruleNameDef
	    figureGroupObject
	    (range | singleValueSet)
	    { comment | userData }
	')'

number ::=
	'(''number' {numberValue | numberDisplay | number} ')'

numberDefinition ::=
	'(''numberDefinition'
	   {scale | <gridMap> | comment}
	')'

numberDisplay ::=
	'(''numberDisplay' numberValue {display} ')'

numberValue ::=
	scaledInteger

offPageConnector ::=
	'(''offPageConnector' portNameDef 
	    { < unused > | property | comment | userdata}
	')'

offsetEvent ::=
	'(''offsetEvent' event numberValue ')'

openShape ::=
	'(''openShape' curve { property } ')'

orientation ::=
	'(''orientation' 
	   ('R0' | 'R90' | 'R180' | 'R270' | 'MX' | 'MY' | 'MYR90' | 'MXR90')
	')'

origin ::=
	'(''origin' pointValue')'

overhangDistance ::=
	'(''overhangDistance' ruleNameDef
	    figureGroupObject
	    figureGroupObject
    	    (range | singleValueSet)
	    { comment | userData }
	')'

overlapDistance ::=
	'(''overlapDistance' ruleNameDef
	    figureGroupObject
	    figureGroupObject
	    (range | singleValueSet)
	    { comment | userData }
	')'

oversize ::=
	'(''oversize'
	     integerValue
	     (figureGroupRef | figureOp)
	     cornerType
	')'

owner ::=
	'(''owner' stringToken')'

page ::=
	'(''page' instanceNameDef
	     { instance | net | netBundle | commentGraphics |
	     portImplementation | < pageSize > | < boundingBox > |
	     comment | userData }
	')'

pageSize ::=
	'(''pageSize' rectangle')'

parameter ::=
	'(''parameter' valueNameDef typedValue [ unit ] ')'

parameterAssign ::=
	'(''parameterAssign' valueNameRef typedValue ')'

parameterDisplay ::=
	'(''parameterDisplay' valueNameRef {display} ')'

path ::=	
	'(''path' pointList { property } ')'

pathDelay ::=
	'(''pathDelay' delay { event } ')'

pathWidth ::=
	'(''pathWidth' integerValue ')'

permutable ::=
	'(''permutable'
	    { portRef | permutable | nonPermutable }
	')'

physicalDesignRule ::=
	'(''physicalDesignRule'
	    { figureWidth | figureArea | rectangleSize |
	    figurePerimeter | overlapDistance | overhangDistance |
	    enclosureDistance | interFigureGroupSpacing | 
	    intraFigureGroupSpacing | notchspacing | notAllowed | 
	    figureGroup | comment | userData }
	')'

plug ::=
	'(''plug' { socketSet } ')'

point ::=
	'(''point' {pointValue | pointDisplay | point} ')'

pointDisplay ::=
	'(''pointDisplay' pointValue {display} ')'

pointList ::=
	'(''pointList' { pointValue } ')'

pointValue ::=
	pt

polygon ::=
	'(''polygon' pointList { property } ')'

port ::=
	'(''port' portNameDef
	     { < direction > | < unused > | portDelay |
	     < designator > | < dcFaninLoad > | < dcFanoutLoad > |
	     < dcMaxFanin > | < dcMaxFanout > | < acLoad > |
	     property | comment | userData }
	')'

portBackAnnotate ::=
	'(''portBackAnnotate' portRef
	     { < designator > | portDelay | < dcFaninLoad > |
	     < dcFanoutLoad > | < dcMaxFanin > | < dcMaxFanout > |
	     < acLoad > | property | comment } ')'

portBundle ::=
	'(''portBundle' portNameDef
	    listOfPorts
	    { property | comment | userData } 
	')'

portDelay ::=
	'(''portDelay'
	    derivation
	    (delay | loadDelay)
	    { transition | becomes }
	')'

portGroup ::=
	'(''portGroup' { portRef | portNameRef } ')'

portImplementation ::=
	'(''portImplementation' (portRef | portNameRef)
	    { < connectLocation > | figure | instance |
	    commentGraphics | propertyDisplay | keywordDisplay |
	    property | userData | comment }
	')'

portInstance ::=
	'(''portInstance' (portRef | portNameRef)
	     { < unused > | < designator > | < dcFaninLoad > | 
               < dcFanoutLoad > | < dcMaxFanin > | < dcMaxFanout > | 
	       < acLoad > | portdelay | property | comment | userData } 
	')'

portList ::=
	'(''portList' {portRef | portNameRef} ')'

portListAlias ::=
	'(''portListAlias' portnameDef portList ')'

portMap ::=
	'(''portMap'
	     { portRef | portgroup | comment | userData }
	')'

PortNameDef ::=
	NameDef | array

PortnameRef ::=
	NameRef | member

portRef ::=
	'(''portRef' portNameRef 
	    [ portRef | instanceRef | viewRef ]
	')'

program ::=
	'(''program'
	    stringToken
	     [ version ]
	')'

property ::=
	'(''property' propertyNameDef typedValue
	    { <owner> | <unit> | property | comment }
	')'

propertyDisplay ::=
	'(''propertyDisplay' propertyNameRef
	    { display }
	')'

propertyNameDef ::=
	NameDef

propertyNameRef ::=
	NameRef

protectionFrame ::=
	'(''protectionFrame'
	    { portImplementation | figure | instance | commentGraphics |
	    < boundingBox > | propertyDisplay | keywordDisplay |
	    parameterDisplay | property | comment | userData }
	')'

pt ::=
	'(''pt' integerValue integerValue ')'

range ::=
	lessThan | greaterThan | atMost | atLeast | exactly | between

rangeVector ::=
	'(''rangeVector' { range | singleValueSet } ')'

rectangle ::=
	'(''rectangle' pointValue pointValue { property }')'

rectangleSize ::=
	'(''rectangleSize' ruleNameDef
	     figureGroupObject
	     (rangeVector | multipleValueSet)
	     { comment | userData }
	')'

rename ::=
	'(''rename'
	    (identifier | name)
	    (stringToken | stringDisplay)
	')'

resolves ::=
	'(''resolves'
	    { logicNameRef }
	')'

ruleNameDef ::=
	NameDef

scale ::=
	'(''scale' numberValue numberValue unit ')'

scaledInteger ::=
	integerToken | e

scaleX ::=
	'(''scaleX' integerValue integerValue ')'

scaleY ::=
	'(''scaleY' integerValue integerValue ')'

section ::=
	'(''section'
	    stringValue
	    { section | stringValue | instance }
	')'

shape ::=
	'(''shape'
	    curve
	    { property }
	')'

simulate ::=
	'(''simulate' simulateNameDef
	    { portListAlias | waveValue | apply | comment | userData }
	')'

simulateNameDef ::=
	NameDef

simulationInfo ::=
	'(''simulationInfo'
	    { logicValue | comment | userData }
	')'

singleValueSet ::=
	'(''singleValueSet'
	    { range }
	')'

site ::=
	'(''site' viewRef [ transform ] ')'

socket ::=
	'(''socket' [symmetry] ')'

socketSet ::=
	'(''socketSet' symmetry  { site } ')'

status ::=
	'(''status'
	     { written | comment | userData }
	')'

steady ::=
	'(''steady'
	     (portNameRef | portRef | portList)
	     duration
	     [transition | becomes ]
	')'

string ::=
	'(''string'
	    {stringValue | stringDisplay | string}
	')'

stringDisplay ::=
	'(''stringDisplay' stringValue { display } ')'

stringValue::=
	stringToken

strong ::=
	'(''strong' logicNameRef ')'

symbol ::=
	'(''symbol'
	    { portImplementation | figure | instance | annotate |
	    commentGraphics | < pageSize > | < boundingBox > |
	    propertyDisplay | keywordDisplay | parameterdisplay |
	    property | comment | userData }
	')'

symmetry ::=
	'(''symmetry' { transform }')'

table ::=
	'(''table'
	     { entry | <tableDefault> }
	')'

tableDefault ::=
	'(''tableDefault'
	    (logicRef | portRef | noChange | table)
	    [ delay | loadDelay ]
	')'

technology ::=
	'(''technology' numberdefinition
	     { figureGroup | fabricate | < simulationInfo > | 
	     < physicalDesignRule > | comment | userData }
	')'

textHeight ::=
	'(''textHeight' integerValue ')'

timeInterval ::=
	'(''timeInterval'
	     (event | offsetEvent)
	     (event | offsetEvent | duration)
	')'

timeStamp ::=
	'(''timeStamp'
	    integerToken integerToken integerToken
	    integerToken integerToken integerToken
	')'

timing ::=
	'(''timing'
	    derivation
	    { pathDelay | forbiddenEvent | comment | userdata }
	')'

transform ::=
	'(''transform'
	     [ scaleX ] [ scaleY ] [ delta ]
	     [ orientation ] [ origin ]
	')'

transition ::=
	'(''transition'
	    (logicNameRef | logicList | logicOneOf)
	    ( logicnameRef | logicList | logicOneOf )
	')'

trigger ::=
	'(''trigger' { change | steady | initial } 	')'

true ::=
	'(''true'')'

typedValue ::=
	boolean | integer | miNoMax | number | point | string

unconstrained ::=
	'(''unconstrained'')'

undefined ::=
	'(''undefined'')'

union ::=
	'(''union'
	    ( figureGroupRef | figureOp )
	    { figureGroupRef | figureOp }
	')'

unit ::=
	'(''unit'
	  ('ANGLE' | 'CAPACITANCE' | 'CONDUCTANCE' | 'CHARGE' | 'CURRENT' | 
	   'DISTANCE' | 'ENERGY' | 'FLUX' | 'FREQUENCY' | 'INDUCTANCE' |
	   'MASS' | 'POWER' | 'RESISTANCE' | 'TEMPERATURE' | 'TIME' | 'VOLTAGE')
	')'

unused ::=
	'(''unused'')'	

userdata ::=
	'(''userData' identifier
	     { integerToken | stringToken | identifier | form }
	')'

ValueNameDef ::=
	NameDef | array

ValueNameRef ::=
	NameRef | member

version ::=
	'(''version' stringToken')'

view ::=
	'(''view' viewNameDef
	     viewType
	     interface
	     { < status > | < contents > | comment | property | userData }
	')'

viewList ::=
	'(''viewList'
	     { viewRef | viewList}
	')'

viewMap ::=
	'(''viewMap'
	      { portMap | portBackAnnotate | instanceMap |
	      instanceBackAnnotate | netMap | netBackAnnotate |
	      comment | userData }
	')'

viewNameDef ::=
	NameDef

viewNameRef ::=
	NameRef

viewRef ::=
	'(''viewRef' viewNameRef  [ cellRef ] ')'

viewType ::=
	'(''viewType'
	   ('MASKLAYOUT' | 'PCBLAYOUT' | 'NETLIST' | 'SCHEMATIC' | 'SYMBOLIC' |
	    'BEHAVIOR' | 'LOGICMODEL' | 'DOCUMENT' | 'GRAPHIC' | 'STRANGER')
	')'

visible ::=
	'(''visible' booleanValue ')'

voltageMap ::=
	'(''voltageMap' MinomaxValue ')'

waveValue ::=
	'(''waveValue' logicNameDef numberValue logicWaveform ')'

weak ::=
	'(''weak' logicNameRef ')'

weakJoined ::=
	'(''weakJoined'
	     { portRef | portList | joined }
	')'

when ::=
	'(''when'
	    trigger
	    { after | follow | logicAssign | maintain | comment | userData }
	')'

written ::=
	'(''written'
	     timeStamp
	     { < author > | < program > | < dataOrigin > |
	     property | comment | userData }
	')'

