import visitor.*;
import syntaxtree.*;
import java.io.*;

/** This class provides the basic mechanism to parse an EDIF file.
 */
public class EdifParser {


  public static void main(String[] args) {
    if (args.length == 1) {
      new EdifParser(args[0], null);
    } else if (args.length == 2) {
      new EdifParser(args[0], args[1]);
    } else {
      System.err.println("Usage: EdifParser edifFileName");
      System.err.println("   or  EdifParser edifFileName outputFileName");
      System.exit(1);
    }
  }

  /** The default target.  Initial value of "Virtex" */
  public EdifParser(String filename, String outFilename) {
    EdifVisitor myVisitor = new EdifVisitor(outFilename);
    EdifParserCore parser;
    try {
      System.out.println("Parsing file "+ filename+ ", please wait...");
      parser = new EdifParserCore(new FileInputStream(filename));
      Node root = parser.edif();
      System.out.println("Edif file " + filename + " parsed successfully.");
      root.accept(myVisitor);
      System.out.println("Depth first visitor completed successfully...");
    } catch (ParseException e) {
      System.out.println("Encountered errors during parse.");
      System.out.println(e);
    } catch (FileNotFoundException e) {
      System.out.println("File " + filename + " not found.");
    }
  }

}
