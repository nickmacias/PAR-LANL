#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define NODEBUG
#define NOSPEEDUP

#define SPACING 3  /* Spacing between placed components! */
//#define SPACING 1  /* Spacing between placed components! */

/*
 * Various structures for storing circuits, nets, etc.
 */

/*
 * The network structures are a bit funky, so we'll do some ASCII art here:

CIRCUIT
-------
 .NETS ---->NETLIST
            -------
             .NET (NULL)
             .NEXT                   /          \
               |                     |1st netnode|
               |                     |   node    |
               |                     \  ======>  /
               v
/   1st  \  NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
| netlist|  -------   |   -------  |   -------  |   -------  |
|  node  |   .NET-----+   .C       |   .C       |   .C       |
|   ||   |   .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
|   ||   |     |          .NEXT----+   .NEXT----+   .NEXT----+
|   ||   |     |
\   vv   /     |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
               .
               .
               .

netnode.c is an instance of a component
netnode.ioindex is the index of an I/O port

Place and Route algorithm taken from Akshay Sharma, "Development of a
Place and Route Tool for the RaPiD architecture," Master's Project,
Autumn Quarter, 2001, University of Washington (Advisor Scott A. Hauck).

See that paper for earlier sources for the simulated annealing placement
algorithm and the Pathfinder routing algorithm.

 */

/* circuit holds the main circuit, as well as (possibly) subcircuits */
struct circuit{
  char libout[1024]; /* name of output library */
  char *name; /* user's name for this circuit */
  int height,width; /* Matrix dimensions */

/* members for top-level I/O */
  char **io; /* array of input and output names */
  int *iodir; /* 0=input, 1=output */
  char *ioside; /* n, s, w or e ('' if missing) */
  int *iocell1,*iocell2; /* range of locs (-1 if missing) */
  int numio; /* shortcut to # of IO defs */
  char *placedside; /* fill when edgeport is placed */
  int *placedrow,*placedcol;

  struct instance *components; /* Linked list of instances of components */
  struct netlist *nets; /* Linked list of all nets */
  struct libentry *library; /* Linked list of library entries */
  struct cell *matrix; /* Cells within the (sub-)matrix */
  /* struct route routes; */ /* Linked list of all routes */
/* As usual, first nodes are not used... */
};

/* Linked list of all nets */
struct netlist{
  struct netnode *net; /* Headnode of a single net */
  struct netlist *next;
};

/* Single node within a net */
struct netnode{
/* I/O for the toplevel circuit is special, and is indicated by edge=1 */
  int edge; /* 0=normal instance port;1=edge port */
  struct instance *c;
  int ioindex; /* These define the connection point to the net */
               /* If edge=0, actual point is c->libdef->io[ioindex] */
               /* If edge=1, this is an index to circuit->io[] */
  struct netnode *next;
};

struct instance{
  char *name; /* Instance name */
  char *basename; /* Basename of object of which this is an instance */
                  /* (may be a circuit!?) */

/* These next two members are paired with each other
 * ioname[7] corresponds to libentry->io[ioindex[7]], etc.
 */
  char **ioname; /* Ordered array of input/output names used in this instance */
  int *ioindex; /* index in struct libentry */

  int ulrow, ulcol, rotation; /* Placement within matrix */
// placement constraints
  int ulrowc,ulcolc,lrrowc,lrcolc,rotc; // rot<0 means unconstrained

  struct libentry *libdef; /* Point to library entry */
  struct instance *next; /* Pointer to next node */
};

// Statistics block
struct stat_block{
  int Glen_str,Glen_turn,Glen_sum,  // sum of route segment lengths
      Glen_str_n,Glen_turn_n,Glen_sum_n,  // # of route segments
      Mlen_str,Mlen_turn,Mlen_sum,  // max route segment lengths
      cells_route,cells_comp,cells_total;  // Cell utilization
};

/* Library structure - store basename, size, I/O defs and cells
 * for an entry in a library */

struct libentry{
  char *basename; /* basename in struct instance */
  char *desc; /* Null-terminated, freeform description */
  int h,w; /* dimensions */
  int version,num_versions;
/*
 * Inside .LIB library file, components have a form "basename{ver/num}"
 * where num is the number of versions of this component, and
 * ver is the number of this particular version.
 * Inside PAR input files, only the basename is used, but above a certain
 * threshold temperature, any version will be allowed in the Place step.
 *
 */

  char **cells; /* Array of h*w standard .GRD file entries */
  int numio; /* Number of I/O defs in io[] array */
  struct iodef *io; /* Array of I/O ports */
  struct stat_block stats;
  struct libentry *next; /* linked list! */
};

struct iodef{ /* Define a single I/O port */
  char *iobasename; /* native name of I/O port */
  int row,col; /* cell containing the I/O port */
  char side[4]; /* i.e., dso,, dwi, etc. */
};

/* Structures for cells within the matrix */
struct cell{
  /* int inputs,outputs; */ /* Masks showing input ports in use(1) or free(0) */
  int hcost_N,hcost_S,hcost_E,hcost_W; /* Historical costs, 4 inputs */
  int pcost_N,pcost_S,pcost_E,pcost_W; /* Penalty costs, 4 inputs */
/* -1 means never available (cell belongs to a component) */

  struct netnode *netN,*netS,*netW,*netE; /* Net to which input belongs */
/* NULL if input does not belong to any net */

  struct instance *instance; /* Pointer to this instance, or NULL if free */
  int row,col; /* Location within component */
               /* reset to abs [r,c] during routing */
  char visited[4]; // non-blank once we fan out to this: NSWE
                  // set if the given INPUT has been visited (fanned-out to)
  char counted[4]; // used only during final use tally (NSWE)
};

// structure for final cell routing
struct finalcells{
  char nn,ns,nw,ne,sn,ss,sw,se,wn,ws,ww,we,en,es,ew,ee; // from->to
};

// new structures for routing phase
struct matrix{
  int height;
  int width; // size of matrix
  struct cellport *cells; // tags for cell and all I/O
  struct finalcells *finalcells;  // actual routing solution
};

#define DNI 0
#define DNO 1
#define DSI 2
#define DSO 3
#define DWI 4
#define DWO 5
#define DEI 6
#define DEO 7

#define N 0
#define S 1
#define W 2
#define E 3

struct cellport{
  int ss[8]; // net #: +=src, -=sink, 0=neither
  char edge[8]; // EDGE flags
  int hcost[4];
  int pcost[4]; // costs of each input
  char occupied; // set to '1' if cell is used by some component
  int r,c; // for backlink reference
  char visited[4]; // for loop avoidance
  char counted[4]; // for cost update
};

struct pqnode{
  int cost;			// node cost=(1+Hn)*Pn
  int pqcost;			// Cost in priority queue
                                // (=cost from SRC to this node)
  struct cellport *cell;	// pointer to cell containing this node
  int r,c;			// [r,c] of this node
  int ss;			// source of net to which this node belongs
				// or 0 if node is unused
				// Sink (-) or Source (+) or 0
  char side;			// n, s, w or e
  char edge;			// '1' or ' ';
  struct pqnode *parent;	// Pointer to node from which we faned-out
  struct pqnode *next;		// Pointer to next (higher-cost) node
  struct treenode *rtnode;	// Set if this node is in routing_tree
};

struct treenode{
  struct treenode *n,*s,*w,*e,*back;
  char backside; /* side of parent from which we came */
  struct pqnode *data;
};

struct pqnode_list{
  struct pqnode *node;
  struct pqnode_list *next;
}; // Save popped PQ nodes for possible later disposal


char *substr();
struct netnode *make_node(struct circuit*,char *,char *);
struct netnode *get_node();
struct instance *get_named_instance(struct circuit*,char *);
struct instance *next_instance(struct circuit*,int);
int rando(int);
struct cell *getcell(struct circuit *,int,int);
int place_component(struct circuit *, struct instance *,int,int,int);
int place_edgeport(struct circuit *, int epnum,int,int,char);
struct instance *get_indexed_instance(struct circuit *,int);
int rotate_iodef(struct iodef *,int,int,int);

/* Special instance for identifying edges */
static struct instance *EDGEINST=(struct instance*) 1;

static int fancy=0; /* 1 means show circuit with page clears, etc. */

// Initialize a routing tree to a single source node
struct treenode *init_rt(struct circuit *,struct matrix *,
                         struct netnode *,int);

// initialize priority queue to a routing tree
int init_pq(struct pqnode *,struct treenode *);

// make a new Priority Queue node
struct pqnode *new_pqnode(struct cellport *,int,int,
                  char,int,int,struct pqnode *);

// Pop lowest-cost node from PQ and return it
struct pqnode *remove_pq_node(struct pqnode*);

// return a fanout node (or NULL)
struct pqnode *next_fanout(int,struct pqnode *,struct matrix *,int);

// set VISITED node in cell (matrix) structure
int set_visited(struct matrix *,int,int,char,int);

// malloc() success checker
int mcheck(char *);

// traverse and record nodes on a single route
int compute_route(struct matrix*,struct circuit *circuit,
                  int,int,int,char,char,struct treenode *);

int do_route(struct matrix*,int,int,char,char,struct circuit*);

int show_route(struct matrix*,int,int,int,char,char,struct treenode*,
               struct circuit*);

struct stat_block *route(struct circuit*,struct matrix*);

struct stat_block *compute_stats(struct circuit*,struct matrix*,
                               int,struct treenode**);
int trace_path(struct circuit*,int,int,char,
               struct finalcells*,int,int,int);
