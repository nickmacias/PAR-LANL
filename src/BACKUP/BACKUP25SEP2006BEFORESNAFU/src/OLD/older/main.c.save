#include <stdio.h>
#include <ctype.h>

/*
 * Various structures for storing circuits, nets, etc.
 */

/*
 * The network structures are a bit funky, so we'll do some ASCII art here:

CIRCUIT
-------
 .NETS ---->NETLIST
            -------
             .NET (NULL)
             .NEXT                   /          \
               |                     |1st netnode|
               |                     |   node    |
               |                     \  ======>  /
               v
/   1st  \  NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
| netlist|  -------   |   -------  |   -------  |   -------  |
|  node  |   .NET-----+   .C       |   .C       |   .C       |
|   ||   |   .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
|   ||   |     |          .NEXT----+   .NEXT----+   .NEXT----+
|   ||   |     |
\   vv   /     |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
               .
               .
               .

netnode.c is an instance of a component
netnode.ioindex is the index of an I/O port
 */

/* circuit holds the main circuit, as well as (possibly) subcircuits */
struct circuit{
  char *name; /* user's name for this circuit */
  int height,width; /* Matrix dimensions */
  char **inputs; /* array of input names */
  char **outputs; /* array of output names - terminated by NULL */ 
  struct instance *components; /* Linked list of instances of components */
  struct netlist *nets; /* Linked list of all nets */
  struct libentry *library; /* Linked list of library entries */
  /* struct route routes; */ /* Linked list of all routes */
/* As usual, first nodes are not used... */
};

/* Linked list of all nets */
struct netlist{
  struct netnode *net; /* Headnode of a single net */
  struct netlist *next;
};

/* Single node within a net */
struct netnode{
  struct instance *c;
  int ioindex; /* These define the connection point to the net */
  struct netnode *next;
};

struct instance{
  char *name; /* Instance name */
  char *basename; /* Basename of object of which this is an instance */
                  /* (may be a circuit!?) */
  char **ioname; /* Ordered array of input/output names used in this instance */
  int *ioindex; /* index in struct libentry */
  int ulrow, ulcol, rotation; /* location/orientation of instance in matrix */
  struct libentry *libdef; /* Point to library entry */
  struct instance *next; /* Pointer to next node */
};

/* Library structure - store basename, size, I/O defs and cells
 * for an entry in a library */

struct libentry{
  char *basename; /* basename in struct instance */
  char *desc; /* Null-terminated, freeform description */
  int h,w; /* dimensions */
  char **cells; /* Array of h*w standard .GRD file entries */
  int numio; /* Number of I/O defs in io[] array */
  struct iodef *io; /* Array of I/O ports */
  struct libentry *next; /* linked list! */
};

struct iodef{ /* Define a single I/O port */
  char *iobasename; /* Matches .net basenames */
  int row,col; /* cell containing the I/O port */
  char side[4]; /* i.e., dso,, dwi, etc. */
};

/* Structures for cells within the matrix */
struct cell{
  int inputs,outputs; /* Masks showing I/O ports in use(1) or free(0) */
  struct instance *instance; /* Pointer to this instance, or NULL or free */
  struct libentry *libentry; /* Pointer to library def of component */
  int row,col; /* Location within component */
};

/* MASKS for cell inputs and outputs */
#define DN 1
#define DS 2
#define DW 4
#define DE 8
#define CN 16
#define CS 32
#define CW 64
#define CE 128

struct cell *matrix; /* 2D! */


char *substr();
struct netnode *make_node(struct circuit*,char *,char *);
struct netnode *get_node();
struct instance *find_instance(struct circuit*,char *);
struct instance *next_instance(struct circuit*,int);

main(argc,argv)
int argc;
char **argv;
/* Usage: main [input] */
{
  struct circuit *circuit; /* Main circuit! */
  FILE *fp;
  char buffer[1024];
  int errcnt=0; /* # of parse etc. errors */
  int i,j;

  if (argc != 2){
    fprintf(stderr,"Usage: %s inputfile\n",argv[0]);
    exit(1);
  }

/* Initialize structures */
  circuit=malloc(sizeof(struct circuit));
  circuit->components=malloc(sizeof(struct instance));
  circuit->components->next=(struct instance*) NULL;
  circuit->nets=malloc(sizeof(struct netlist));
  circuit->nets->next=(struct netlist*) NULL;
  circuit->library=malloc(sizeof(struct libentry));
  circuit->library->next=(struct libentry*)NULL;


/*
 * Read the netlist description
 */
  fp=fopen(argv[1],"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Cannot open input file %s\n",argv[1]);
    exit(1);
  }

  while (0 == get_line(fp,buffer)){
    printf("<%s>\n",buffer);
    if (0 != parse_line(circuit,buffer)){ /* Load structures accordingly */
      fprintf(stderr,"ERROR: Cannot parse <%s>\n",buffer);
      ++errcnt;
    }
/* Structures have been loaded - continue! */
  }
  fclose(fp);
  if (errcnt > 0)
    fprintf(stderr,"Total of %d error%s\n",errcnt,(errcnt==1)?"":"s");
  else fprintf(stderr,"\nNo errors detected!\n\n");

  dump_circuit(circuit); /* Display results so far */
}

dump_circuit(struct circuit *circuit)
{
  struct instance *inst; /* temporary variable */
  struct netlist *templist;
  struct netnode *tempnode;
  int i;


/* Show results of parsing */
  printf("Circuit Name=%s\n",circuit->name);
  printf("dims=%dx%d\n",circuit->height,circuit->width);
  i=0;

  while (circuit->inputs[i] != (char *)NULL)
    printf("Circuit Input:%s\n",circuit->inputs[i++]);
  i=0;
  while (circuit->outputs[i] != (char *)NULL)
    printf("Circuit Output:%s\n",circuit->outputs[i++]);

  printf("\nINSTANCES:\n");
  inst=next_instance(circuit,1); /* Start new instance retrieval */
  while (NULL != (inst=next_instance(circuit,0))){
    printf("%s(%s) IO assignments: ",inst->name,inst->basename);
    i=0; /* Dump the input/output mapping */
    while (inst->ioname[i]!=(char *)NULL){
      printf("%s(index=%d) ",inst->ioname[i],inst->ioindex[i]);
      ++i;
    }
    printf("\n");
    printf("Component is %dx%d\n",inst->libdef->h,inst->libdef->w);
  }


  printf("\nLIBRARY ENTRIES:\n");
  struct libentry *le;
  le=circuit->library;
  while (le->next != (struct libentry*)NULL){
    le=le->next;
    printf("%s\n",le->basename);
/* dump the I/O info for this component */
    for (i=0;i<le->numio;i++){
      printf("IO[%d]:%s=[%d,%d]%s\n",i,le->io[i].iobasename,
                                     le->io[i].row,
                                     le->io[i].col,
                                     le->io[i].side);
    }
    printf("\n");
  }

  printf("\n\nNETS:\n");
  templist=circuit->nets;
  while (templist->next != NULL){
    templist=templist->next;
    tempnode=templist->net;
    printf("[");
    while (tempnode->next != NULL){
      tempnode=tempnode->next;
      printf("%s:%d ",tempnode->c->name,tempnode->ioindex);
    }
    printf("]\n");
  }

/* Now test the network traversal calls */
  printf("\n\nTESTING NETWORK RETRIEVAL PROCEDURES\n");
  struct netnode *nn;
  get_net(circuit,1);
  while (0 == get_net(circuit,0)){
    printf("NEW NETWORK: ");
    while ((nn=get_node()) != (struct netnode*)NULL){
      printf("%s.%d ",nn->c->name,nn->ioindex);
    }
    printf("\n");
  }
}

/* Read a single "line" from the input file */
int get_line(FILE *fp, char *buf)
{
  int i,j,comment,quote;
  char buffer[1024];

  strcpy(buffer,"");
  quote=0; /* Set inside " " */
  while (strlen(buffer) == 0){
    if (NULL == fgets(buf,1024,fp)) return(1);
    comment=0; /* Set once we see ";" */
    j=0; /* Output location */
    for (i=0;i<strlen(buf);i++){
      if (buf[i]==';') comment=1;
      if (buf[i]=='"') quote=1-quote;
      if ((comment==0) && ((quote==1)||(!isspace(buf[i])))) buffer[j++]=buf[i];
    }
    buffer[j]='\0';
    strcpy(buf,buffer);
  } /* Loop until we actually read something */
  return(0);
}

/*
 * Individual line parser. Legal statements are:
 *
 * .define newname(height,width,v1:in,v2:in,v3:out,...)
 * which begins the definition of a new circuit, and specifies its inputs
 * and output. height and width are the dims of the encompasing matrix
 *
 * .library "libname" specifies the name of a component library to load
 * note that libname is case- and space-sensitive!
 *
 * .instance name=basename(ioname=baseioname,ioname=baseionane,...)
 * which defines an instantiation of some base cell or subcircuit. This
 * syntax allows the optional renaming of inputs and outputs.
 *
 * .net name(instance1.io1,instance2.io2,instance3.io3,...)
 * which defines an (optionally-named) net. A net connects two or more nodes,
 * with each node specified by an instance/io pair (where the io is either:
 * the baseioname name in the instance's base; or, if the ioname=baseioname
 * structure was used to rename an io, the ioname assigned to a baseioname).
 *
 */
int parse_line(circuit,buffer)
struct circuit *circuit;
char *buffer;
{
  char buf[1024],temp[1024],temp1[1024],temp2[1024];
  char in[64][256],out[64][256]; /* Temp storage! */
  int i,j,k,l,more,eov,numin,numout,err;
  struct instance *inst;
  struct netnode *net,*netnode,*tempn; /* Temporary nets */
  struct netlist *templist;

/* Check the type of the statement */

/*** .DEFINE STATEMENT ***/

  if (match(buffer,".define",0)){ /* A circuit definition statement */
    i=find(buffer,"(",0);
    if (i < 0){
      fprintf(stderr,"Excpeting \"(\" in .define statement\n");
      return(1); /* Error */
    }

/* Now we can pick off the name */
    strcpy(temp,substr(buffer,7,0,i-1));
    circuit->name=malloc((1+strlen(temp)) * sizeof(char));
    strcpy(circuit->name,temp);

    ++i; /* Start of height */
    j=find(buffer,",",i);
    if (j < 0){fprintf(stderr,"Expecting height,width\n");return(1);}
    sscanf(substr(buffer,i,0,j-1),"%d",&circuit->height);
    i=j+1;
    j=find(buffer,",",i);
    if (j < 0) j=find(buffer,")",i);
    if (j < 0){fprintf(stderr,"Expecting height,width\n");return(1);}
    sscanf(substr(buffer,i,0,j-1),"%d",&circuit->width);
    i=j+1; /* Start of I/O defs */

    numin=numout=0;
    more=1;
    while (more==1){
      eov=find(buffer,",",i);
      if (eov < 0) {more=0;eov=find(buffer,")",i);}
      if (eov < 0){fprintf(stderr,"EOL not found\n");return(1);}
      strcpy(temp,substr(buffer,i,0,eov-1)); /* var:direc */
      j=find(temp,":",0);
      if (j < 1){fprintf(stderr,"Expecting \":\"\n");return(1);}
      strcpy(temp2,substr(temp,0,0,j-1)); /* VAR */
      if (0==strcmp("in",substr(temp,j+1,2,0))){ /* Input var */
        strcpy(in[numin++],temp2);
      } else if (0==strcmp("out",substr(temp,j+1,3,0))){ /* Output var */
        strcpy(out[numout++],temp2);
      } else {fprintf(stderr,"Expecting \"in\" or \"out\"\n");return(1);}

      i=eov+1;
    }
/* Copy the in[] and out[] arrays to the CIRCUIT structure */
    circuit->inputs=malloc((numin+1)*sizeof(char *)); /* Make room for arrays */
/* Need extra space for the NULL array terminator */
    for (i=0;i<numin;i++){
      circuit->inputs[i]=malloc((1+strlen(in[i]))*sizeof(char));
      strcpy(circuit->inputs[i],in[i]);
    }
    circuit->inputs[numin]=(char *)NULL;

    circuit->outputs=malloc((numout+1)*sizeof(char *));
    for (i=0;i<numout;i++){
      circuit->outputs[i]=malloc((1+strlen(out[i]))*sizeof(char));
      strcpy(circuit->outputs[i],out[i]);
    }
    circuit->outputs[numout]=(char *)NULL;

    if ((circuit->height==0)||(circuit->width==0)){
      fprintf(stderr,"Expecting height,width in start of .define statement\n");
      return(1);
    }

/* Initialize the matrix */
    matrix=malloc(circuit->height*circuit->width*sizeof(struct cell));
    for (i=0;i<circuit->height;i++){
      for (j=0;j<circuit->width;j++){
        matrix[circuit->width*i+j].inputs=0;
        matrix[circuit->width*i+j].outputs=0;
        matrix[circuit->width*i+j].instance=NULL;
        matrix[circuit->width*i+j].libentry=NULL;
      }
    }

    return(0);
  } /*** END OF .DEFINE STATEMENT ***/

/*** .LIBRARY STATEMENT ***/
  if (match(buffer,".library\"",0)){ /* Library load */
    i=find(buffer,"\"",9);
    if (i < 0){fprintf(stderr,"Expecting \"libname\"\n");return(1);}
    strcpy(temp,substr(buffer,9,0,i-1)); /* library name */
    return(load_library(circuit,temp));
  }

/*** .INSTANCE STATEMENT ***/
  if (match(buffer,".instance",0)){ /* component instantiation */
    i=find(buffer,"=",0);
    if (i < 0){fprintf(stderr,"Expecting \"=\"\n");return(1);}
    strcpy(temp,substr(buffer,9,0,i-1)); /* Name of this instance */

/* Make an instance node for this instance */
    inst=malloc(sizeof(struct instance)); /* New memory please! */
    inst->name=malloc((1+strlen(temp))*sizeof(char));
    strcpy(inst->name,temp);

/* Insert this instance into circuit's linked list */
    inst->next=circuit->components->next;
    circuit->components->next=inst;

/*
 * Now parse the basename(io=baseio,io=baseio,...)
 * and load into arrays
 */

/* First get basename */
    j=find(buffer,"(",i+1);
    if (j < 0){fprintf(stderr,"Excepting \"(\"\n");return(1);}
    strcpy(temp,substr(buffer,i+1,0,j-1));
    inst->basename=malloc((1+strlen(temp))*sizeof(char));
    strcpy(inst->basename,temp);

/* Now pull off the IO assignments */
    i=0;j++; /* j is loc of first io=io */
    if (buffer[j]==')') ++j; /* () allowed */

    while (buffer[j] != '\0'){ /* More to parse */
      k=find(buffer,"=",j);
      if (k < 0){fprintf(stderr,"Expecting \"=\"\n");return(1);}
      strcpy(in[i],substr(buffer,j,0,k-1));
/* Find RHS */
      j=k+1;
      k=find(buffer,",",j);
      if (k < 0) k=find(buffer,")",j);
      if (k < 0){fprintf(stderr,"Expecting \",\" or \")\"\n");return(1);}
      strcpy(out[i],substr(buffer,j,0,k-1));
      j=k+1;
      ++i;
    }

/* Now store in[] and out[] into the inst structure */
    inst->ioname=malloc((1+i)*sizeof(char *));
    inst->ioindex=malloc((1+i)*sizeof(int));
    err=0; /* Set if we miss an I/O port */

    for (j=0;j<i;j++){
      inst->ioname[j]=malloc((1+strlen(in[j]))*sizeof(char));
      strcpy(inst->ioname[j],in[j]);
      inst->ioindex[j]=find_io(circuit->library,inst->basename,out[j]);
      if (inst->ioindex[j] < 0){ /* Can't find I/O port */
        err=1; /* WIll fail at end - but finish loop first! */
      }
    }
/* and terminate the arrays! */
    inst->ioname[i]=(char *)NULL;

/* Store a pointer to the library entry of this instance's basename */
    struct libentry *le;
    le=circuit->library;
    while (le->next != (struct libentry*) NULL){
      le=le->next;
      if (0==strcmp(le->basename,inst->basename)){
        inst->libdef=le; /* Found the librray definition */
        return(err); /* Exit here */
      }
    } /* Finished with library, still no match! */
    fprintf(stderr,"ERROR: Cannot find \"%s\" in library\n",inst->basename);
    return(1);
  } /*** END OF .INSTANCE ***/

/*** .NET statement ***/
/* .net statement is ".net(inst:io,inst:io,...)" */

  if (match(buffer,".net(",0)){ /* Network membership */
    i=5; /* starting location of next inst.io, or EOL */
/* Start a new net */
    net=malloc(sizeof (struct netnode)); /* Make a new net */
    net->next=(struct netnode*) NULL; /* Empty list */

    while (buffer[i]!='\0'){
      j=find(buffer,",",i);
      if (j < 0) j=find(buffer,")",i);
      if (j<0){fprintf(stderr,"Expecting \",\" or \")\"\n");return(1);}
/* inst.io is sandwiched between i and j-1 */
      k=find(buffer,".",i);
      if (k<0){fprintf(stderr,"Expecting \".\"\n");return(1);}
      strcpy(temp1,substr(buffer,i,0,k-1));
      strcpy(temp2,substr(buffer,k+1,0,j-1));

/* Find instance "temp1" and I/O "temp2," make node, and add to this net */
      netnode=make_node(circuit,temp1,temp2);
      if (netnode == (struct netnode*) NULL) return(1); /* ERROR! */
/*
 * Need to add netnode to end of our net. Note that if the temp1/temp2 node
 * already existed, then netnode will be the net containing it
 * (so we can't just insert netnode into the beginning of our net...)
 */
      tempn=net;
      while (tempn->next != (struct netnode*) NULL) tempn=tempn->next;
      tempn->next=netnode; /* Add to end */

      i=j+1;
    }

/* Add this network to circuit structure */
    templist=circuit->nets;
    while (templist->next != (struct netlist*)NULL) templist=templist->next;
    templist->next=malloc(sizeof (struct netlist)); /* Point to new netlist node */
    templist=templist->next; /* This is the new netlist node */
    templist->next=(struct netlist*)NULL;
    templist->net=net;
    return(0);
  }

  return(1); /* Unknown statement */
}

/* find_io(library,basename,ioname) looks for an IO port named "ioname"
 * in the library entry for "basename"
 * Returns either the index, or -1 if the IO port is not found
 */
int find_io(struct libentry *library,char *basename, char *ioname)
{
  int i;
  struct libentry *le;
  struct iodef *iodef; /* Temp vars */

/* Scan the linked list beginning with library */
  le=library->next;
  while (le != (struct libentry*)NULL){
    if (0==strcmp(basename,le->basename)){ /* Found component */
      for (i=0;i<le->numio;i++){
        if (0==strcmp(ioname,(le->io[i]).iobasename)) return(i);
      }
      fprintf(stderr,
              "ERROR: No I/O port named \"%s\" found in component \"%s\"\n",
              ioname,basename);
      return(-1);
    }
    le=le->next;
  }
  fprintf(stderr,"ERROR: Cannot find component named \"%s\"\n",
         basename);
  return(-1);
}

/*
 * make_mode() reads the names of an instance and an I/O port,
 * and returns a netnode containing the corresponding instance and
 * I/O index. If this is a new node, the next member will be NULL.
 * If however this node already belongs to another network N, then:
 *  - the return node will be the nextwork N; and
 *  - N will be removed from circuit->nets (in anticipation of the
 *    currently-being assembled net being eventually added to circuit->nets)
 *
 */
struct netnode *make_node(struct circuit *circuit,char *temp1,char *temp2)
{
  struct netnode *new;
  struct instance *ins;
  int i;

  new=malloc(sizeof (struct netnode));

/* Find the instance named "temp1" */
  ins=circuit->components->next; 
  while (ins != (struct instance*) NULL){
    if (0==strcmp(ins->name,temp1)) break;
    ins=ins->next;
  }
  if (ins==(struct instance*)NULL){
    fprintf(stderr,"Cannot find instance \"%s\"\n",temp1);
    return((struct netnode*) NULL);
  }

/* Now find the I/O port temp2 */
  i=0;
  while (ins->ioname[i] != (char *)NULL){
    if (0==strcmp(ins->ioname[i],temp2)) break;
    ++i;
  }
  if (ins->ioname[i] == (char *)NULL){
    fprintf(stderr,"Cannot find I/O port named \"%s\"\n",temp2);
    return((struct netnode*) NULL);
  }

/* Now check all nets to see if this node belongs to any existing nets */
  struct netlist *nlist;
  struct netnode *nnode;
  struct netlist *prevnet;
  prevnet=circuit->nets; /* Node prior to this one */
  nlist=prevnet->next; /* Linked list of networks */
  while (nlist != (struct netlist*)NULL){ /* nlist is a net */
    nnode=nlist->net; /* Starting node of this new net */
    while (nnode->next != (struct netnode*)NULL){ /* Check node n2 */
      nnode=nnode->next; /* Move to next node */
      if ((nnode->c==ins) && (nnode->ioindex==i)){
/*
 * Node already belongs to a net. Remove the net from circuit->netlist
 * and return that net.
 */
/*        printf("FOUND pre-existing net containing %s.%s\n",temp1,temp2);*/
        struct netnode *tempreturn;
        tempreturn=nlist->net->next;
/* Remove from circuit->nets */
        prevnet->next=nlist->next;
        return(tempreturn); /* Network containing node! */
      }
    } /* End of this net */
    prevnet=nlist;
    nlist=nlist->next; /* Move to next net */
  }

/* Node is new! */
  new->next=(struct netnode*) NULL;
  new->c=ins;
  new->ioindex=i;
  new->next=(struct netnode*) NULL;
  return(new);
}

/*
 * match() accepts a base string, a pattern, and a location, and checks
 * to see if the patternm appears in the base string beginning at the
 * given location. It returns 1 if the pattern matches, otherwise 0
 */

int match(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=0;i<strlen(pattern);i++){
    if (pattern[i]!=base[startloc]) return(0);
    if (base[startloc++]=='\0') return(0);
  }
  return(1); /* Matches! */
}

/*
 * find() searches for a pattern in a base string, beginning
 * at a given location. It returns the location, or (-1)
 * if the pattern is not found.
 */
int find(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=startloc;i<strlen(base);i++)
    if (1==match(base,pattern,i)) return(i);
  return(-1);
}

char substr_return[2048]; /* but play nice... */
/* substr() - returns a substring */
/* DO NOT NEST! */
char *substr(string,start,len,end) /* Inclusive! */
/* If LEN=0, then END gives ending position */
char *string;
int start,len,end;
{
  int i,j;
  if (len != 0) end=start+len-1; /* We'll use END regardless */
  j=0; /* output location */
  for (i=start;i<=end;i++){
    substr_return[j++]=string[i];
  }
  substr_return[j]='\0';
  return(substr_return);
}

/* Load a library of components from disc */
/* Input must be formatted precisely - no mercy! */
load_library(struct circuit *circuit,char *name)
{
  FILE *fp;
  char buffer[1024],temp1[1024],temp2[1024];
  struct libentry *le;
  int i,j,r,c;
  char side[32];

  fp=fopen(name,"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Cannot open library <%s>\n ",name);
    return(1);
  }

  while (NULL != fgets(buffer,1023,fp)){
    buffer[-1+strlen(buffer)]='\0';
/* Build libentry node */
    le=malloc(sizeof (struct libentry)); /* New library entry! */
    le->basename=malloc((1+strlen(buffer))*sizeof(char));
    strcpy(le->basename,buffer); /* Store the component's name */

    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    le->desc=malloc((1+strlen(buffer))*sizeof(char));
    strcpy(le->desc,buffer); /* Store the component's description */

    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->h));
    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->w)); /* Dims */

/* Now load the cell definitions - these are needed only for final output */
    le->cells=malloc(((le->h)*(le->w))*(sizeof (char*)));
    for (i=0;i<(le->h)*(le->w);i++){
      fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
      le->cells[i]=malloc((1+strlen(buffer))*sizeof(char));
      strcpy(le->cells[i],buffer); /* Store the cell spec */
    }

/* How many IO? */
    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->numio));

/* Store the I/O definitions */
    le->io=malloc((le->numio)*sizeof(struct iodef)); /* Array of iodefs */

    for (i=0;i<le->numio;i++){
      fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
/* Store the I/O def */
      sscanf(buffer,"%s %d %d %s",temp1,&r,&c,temp2);
      (le->io[i]).iobasename=malloc(1+strlen(buffer));
      strcpy((le->io[i]).iobasename,temp1);
      le->io[i].row=r;
      le->io[i].col=c;
      if (strlen(temp2) != 3){
        fprintf(stderr,"expecting def of side, but got \"%s\"\n",temp2);
        return(1);
      }
      strcpy((le->io[i]).side,temp2);
    }

/* Insert new node (le) into start of list (so new engtries are used first) */
    le->next=circuit->library->next;
    circuit->library->next=le;
  }
  fclose(fp);
  return(0);
}

/* Here we have a set of routines for traversing the various data structures */

/* get_net() initializes things for a subsequent get_node().
 * get_net(1) resets to the first net; get_net(0) does next net
 * return value of 0=OK, 1=no more nets
 */
static struct netlist *nl;
static struct netnode *nn;
get_net(struct circuit *circuit,int init)
{
  if (init==1){ /* re-init */
    nl=circuit->nets;
    return(0);
  }
  if (nl->next==(struct netlist*) NULL) return(1); /* No more networks */
  nl=nl->next; /* Next net in linked list */
  nn=nl->net; /* Start of this net */
  return(0);
}

struct netnode *get_node()
{
  if (nn->next == (struct netnode*) NULL) return(nn->next);
  nn=nn->next;
  return(nn);
}

/* Routines to retrieve instances. First, a specific instance */
struct instance *find_instance(struct circuit *circuit,char *name)
{
  struct instance *temp;
  temp=circuit->components;
  while (temp->next != NULL){
    temp=temp->next;
    if (0==strcmp(temp->name,name)) return(temp);
  }
  return(NULL);
}


static struct instance *nextinst;
struct instance *next_instance(struct circuit *circuit,int init)
{
  if (init==1){
    nextinst=circuit->components;
    return(NULL); /* Unused */
  }
  if (nextinst->next == NULL) return(NULL);
  nextinst=nextinst->next;
  return(nextinst);
}
