#include "par.h"
#include <math.h>

double cost(struct circuit*);
double init_temp(struct circuit*);

place(struct circuit *circuit)
{
  double temp; /* Temperature of the cookie batter */
  struct instance *instance;
  int i,j,k,r,r2,c2,tries,success,saver,savec,more,saverot;
  char saveside;
  int N,M,num_moves,D,num_acc;
  double c,oldcost,R;

  N=count_instances(circuit); /* # of instance ports */
  M=circuit->numio; /* # of edgeports */
  temp=init_temp(circuit); /* This will leave an initial circuit */
  printf("init temp=%g\n",temp);
  num_moves=(int)(10.*pow((double)(N+M),1.33));
  printf("# moves per temp=%d\n",num_moves);
  display_placement(circuit,1); /* Show the key */
  clear_page();display_placement(circuit,0); /* Initial placement */

  D=circuit->height+circuit->width; /* Initial delta constraint */
  more=1; /* Clear for end of loop */

  while (more==1){
    num_acc=0; /* Number of accepted moves */

    oldcost=cost(circuit); /* seed this */
    for (i=0;i<num_moves;i++){
/* Move a component */
      j=rando(N+M); /* Random instance/edgeport number */
      if (j < N){ /* instance port */
        instance=get_indexed_instance(circuit,j); /* Pick random inst */
        find_instance_rc(circuit,instance,&saver,&savec,&saverot); /* find [r,c] */
        unplace_instance(circuit,instance); /* Remove it */
        if (0==place_component_random(circuit,instance,saver,savec,D,20)){
/* Do we accept this move? */
          c=cost(circuit);
          if (accept(c,oldcost,temp)==1){ /* Accept this move */
            ++num_acc;
if (fancy==1) display_placement(circuit,0);/*%%%*/
            oldcost=c;
          } else { /* Don't accept */
            unplace_instance(circuit,instance); /* Remove it */
            place_component(circuit,instance,saver,savec,saverot); /* Restore old loc */
          }
        } else { /* wasn't able to place at all! */
          place_component(circuit,instance,saver,savec,saverot); /* Restore old loc */
        }
      } else { /* try moving an edgeport */
        j=j-N; /* edgeport index */
        saver=circuit->placedrow[j];
        savec=circuit->placedcol[j];
        saveside=circuit->placedside[j];
        unplace_edgeport(circuit,j); /* Remove it */
        if (0==place_edgeport_random(circuit,j,saver,savec,D,20)){
/* Do we accept this move? */
          c=cost(circuit);
          if (accept(c,oldcost,temp)==1){ /* Accept this move */
            ++num_acc;
if (fancy==1) display_placement(circuit,0);/*%%%*/
            oldcost=c;
          } else { /* Don't accept */
            unplace_edgeport(circuit,j); /* Remove it */
            place_edgeport(circuit,j,saver,savec,saveside); /* Restore old loc */
          }
        } else { /* wasn't able to place at all! */
          place_edgeport(circuit,j,saver,savec,saveside); /* Restore old loc */
        }
      } /* End of edgeport move attempt */
    } /* End of all moves at this temp */
    printf("Cost=%g ",c);
    R=((double)num_acc)/((double)num_moves);
    printf("R=%g ",R);

/* Adjust temp */
    if (R > 0.96) temp=temp*0.5;
    else if (R > 0.8) temp=temp*0.9;
    else if (R > 0.15) temp=temp*0.95;
    else temp=temp*0.8;
    printf("New temp=%g ",temp);

/* Adjust D */
    D=(int)(((double)D)*(1.-0.44+R));
    /*if (D < 10) D=10;*/
    printf("New D=%d\n",D);
    display_placement(circuit,0);
    if (D==0) more=0;
    if (temp < .00001) more=0;
  } /* end of all temps */
  display_placement(circuit,1); /* Show key again */
  display_placement(circuit,2); /* create .GRD file */
}

accept(double c,double oldcost,double temp)
{
  int n;
  double r,thresh;

  if (c < oldcost) return(1);
/* See if rand(0,1) < exp(-delta_cost/T) */
  n=rand() % 10000000;
  r=((double)n)/10000000.;
  thresh=exp((oldcost-c)/temp);
  if (r < thresh) return(1);
  return(0);
}

double init_temp(struct circuit *circuit)
{
  double temp;
  double sum,sum2;
  int i,j,N,M;
  struct instance *instance;

  sum=sum2=0.; /* statistics */

  N=count_instances(circuit);
  M=circuit->numio;
/* N+M gives total number of ports */

/* First, make an initial random placement of components */
  unplace_all(circuit);
  instance=next_instance(circuit,1);
  while (NULL != (instance=next_instance(circuit,0))){
    if (place_component_random(circuit,instance,0,0,-1,20)) return(1);
  }

/* Place edgeports - but do the most-constrained ones first! */
  for (i=0;i<M;i++){
    if (place_edgeport_random(circuit,i,0,0,-1,20)) return(1);
  }

/* Now start moving components and studying cost */
  for (i=0;i<N+M;i++){
    j=rando(N+M); /* Random instance or edgeport number */
    if (j < N){ /* Regular instance */
      instance=get_indexed_instance(circuit,j); /* Pick random inst */
      unplace_instance(circuit,instance); /* Remove it */
      if (place_component_random(circuit,instance,0,0,-1,20)) return(1);
    } else { /* placing an edgeport */
      j=j-N; /* edgeport number */
      unplace_edgeport(circuit,j);
      if (place_edgeport_random(circuit,j,0,0,-1,20)) return(1);
    }
    temp=cost(circuit);
    sum+=temp;
    sum2+=(temp*temp);
  }

/* compute stdev of these temps */
  return(20*sqrt( ((((double)(N+M))*sum2) - (sum*sum))/((double)((N+M)*((N+M)-1))) ));
}

/* unplace a single instance */
unplace_instance(struct circuit *circuit, struct instance *instance)
{
  int r,c;
  struct cell *cell;
  for (r=0;r<circuit->height;r++){
    for (c=0;c<circuit->width;c++){
      cell=getcell(circuit,r,c);
      if (cell->instance == instance) cell->instance=NULL;
    }
  }
}

/* unplace an edgeport */
unplace_edgeport(struct circuit *circuit, int epnum)
{
  int r,c;
  struct cell *cell;
  r=circuit->placedrow[epnum];
  c=circuit->placedcol[epnum];
  if ((r >= 0) && (c >= 0)){
    cell=getcell(circuit,r,c);
    cell->instance=NULL;
  }
  circuit->placedrow[epnum]=circuit->placedcol[epnum]=(-1);
  circuit->placedside[epnum]=' '; /* Not necessary? */
}


/* undo all placements! */
unplace_all(struct circuit *circuit)
{
  int r,c,i;
  struct cell *cell;

  for (r=0;r<circuit->height;r++){
    for (c=0;c<circuit->width;c++){
      cell=getcell(circuit,r,c);
      cell->instance=NULL;
    }
  }
/* Now reset edgeport structures */
  for (i=0;i<circuit->numio;i++)
    unplace_edgeport(circuit,i);
}

/* Perform the mapping and placement of components inside the matrix */
/* D is maximum displacement from oldr and oldc */
place_component_random(struct circuit *circuit, struct instance *instance,
                       int oldr, int oldc, int D, int tries)
{
  int success,r,c,rot;
  int more;

/* Try placing this instance */
  success=0;
  while ((success==0) && (--tries > 0)){
    more=1;
    while (more==1){
/* Don't place component's cells along any edge */
      r=1+rando(-1+circuit->height - instance->libdef->h);
      c=1+rando(-1+circuit->width - instance->libdef->w);
      rot=rando(4); /* Random CW rotation */
      if (iabs(r-oldr)+iabs(c-oldc) <= D) more=0;
      if (D<=0) more=0; /* unconditional acceptance */
    }
    if (0==place_component(circuit,instance,r,c,rot)){
      success=1;
      instance->ulrow=r;instance->ulcol=c;instance->rotation=rot;
    }
  }

  if (success==0){ /* Place error */
    fprintf(stderr,"Cannot place %s - giving up.\n",instance->name);
    return(1);
  }
  return(0); /* Success! */
}

place_edgeport_random(struct circuit *circuit, int epnum,
                       int oldr, int oldc, int D, int tries)
/* Only need [oldr,oldc] for computing distance of move */
{
  int success,r,c,rot,min,max,loc,i;
  int more;
  char side;
  
/* Try placing this edgeport */
  success=0;
  while ((success==0) && (--tries > 0)){
    more=1;
    while (more==1){
/* Look at ioside, iocell1 and iocell2 */
      side=circuit->ioside[epnum];
      if (side==' ') /* unspecified */
        side="nswe"[rando(4)];
/* Now choose [r,c] to place edgeport */
      if (circuit->iocell1[epnum] == -1){ /* Unspecified */
        min=0;
        max=((side=='n')||(side=='s'))?circuit->width-1:circuit->height-1;
      } else {
        min=circuit->iocell1[epnum];max=circuit->iocell2[epnum];
      } /* Those specify range of possible cell locs */
      loc=min+rando(1+max-min); /* place cell here */
      switch(side){
        case 'n':r=0;c=loc;break;
        case 's':r=circuit->height-1;c=loc;break;
        case 'w':r=loc;c=0;break;
        case 'e':r=loc;c=circuit->width-1;break;
        default:printf("\nRISTC!\n");exit(1);
      }

      if (iabs(r-oldr)+iabs(c-oldc) <= D) more=0;
      if (D<=0) more=0; /* unconditional acceptance */
    }
    if (0==place_edgeport(circuit,epnum,r,c,side)){
      success=1;
    }
  }

  if (success==0){ /* Place error */
    fprintf(stderr,"Cannot place edgeport %d - giving up.\n",epnum);
    return(1);
  }
  return(0); /* Success! */
}

/* Try placing a component at a particular UL location */
place_component(struct circuit *circuit, struct instance *instance,
                int row,int col,int rot)
{
  int r2,c2,h,w,i;
  struct cell *cell;

  h=instance->libdef->h;
  w=instance->libdef->w;
  if ((rot==1)||(rot==3)){
    i=h;h=w;w=i; /* Swap h and w */
  }

/* By adding/subtracting 1, we ensure clear space around component */
  for (r2=(-1);r2<1+h;r2++){
    for (c2=(-1);c2<1+w;c2++){
      if (getcell(circuit,row+r2,col+c2)->instance != NULL)
         return(1); /* Occupied */
    }
  }

/* Component fits - store it */
  for (r2=0;r2<h;r2++){
    for (c2=0;c2<w;c2++){
      cell=getcell(circuit,row+r2,col+c2);

/* instance may be rotated - this affects where cells are stored */

      cell->instance=instance; /* Store component info */
/* Store location of cell within component */
      switch (rot%4){
        case 0:cell->row=r2;cell->col=c2;break;
        case 1:cell->row=c2;cell->col=(h-1)-r2;break;
        case 2:cell->row=(w-1)-r2;cell->col=(h-1)-c2;break;
        case 3:cell->row=(w-1)-c2;cell->col=r2;break;
      }
    }
  }
  instance->ulrow=row;instance->ulcol=col;instance->rotation=rot;
  return(0); /* Success */
}

place_edgeport(struct circuit *circuit, int epnum, int row,int col,char side)
{
  int r2,c2,h,w,i;
  struct cell *cell;

/* By adding/subtracting 1, we ensure clear space around component */
  if ((cell=getcell(circuit,row,col))->instance != NULL)
    return(1); /* Occupied */

  cell->instance=EDGEINST; /* Just a flag for quick reference! */
  circuit->placedrow[epnum]=row;
  circuit->placedcol[epnum]=col;
  circuit->placedside[epnum]=side;
  return(0); /* Success */
}

/* Need to compute the cost of a placement */
double cost(struct circuit *circuit)
{
  int i,j,k,col,row,first,rotation,height,width;
  int r,c;
  char side;
  int baseloc, thisloc; /* Modified locations of nodes */
  int ulcol,ulrow; /* UL location of each component */
  struct netnode *netnode;
  struct iodef iodef;
  int cutsize, total_cutsize, max_cutsize; /* for determining cost */

/*
 * When determining if a net crosses a certain column, we need to
 * take into account not only the [r,c] of the cell, but also the side
 * of the I/O port. Considering columns, if we have a cell in column C,
 * we define its column C' as 2*C+1 for an Eastern port, 2*C-1 for a
 * Western port, and 2*c for a Northern or Southern port.
 *
 * By using C' in comparisons, we consider N/S neutral in terms
 * of column crossings, but consider E/W appropriately.
 * Similar treatment applied to rows
 */
  total_cutsize=0; /* Sum of all cutsize */
  max_cutsize=0; /* maximum */

/* Scan the columns */
  for (col=0;col<circuit->width;col++){
    get_net(circuit,1);
/* check this net for crossings over column (col) */
    while (0==get_net(circuit,0)){
      cutsize=0; /* Set if this net crosses column */
      first=1;
      while (NULL != (netnode=get_node())){
        if (netnode->edge==0){
          ulcol=netnode->c->ulcol; /* Placement of component inside matrix */
          iodef=netnode->c->libdef->io[netnode->ioindex];
          height=netnode->c->libdef->h;
          width=netnode->c->libdef->w;
          rotation=netnode->c->rotation;
          rotate_iodef(&iodef,height,width,rotation); /* Rotate this COPY */
          if (first==1){
/* Find modified base column */
            baseloc=2*(ulcol+iodef.col);
            if (iodef.side[1]=='w') --baseloc;
            if (iodef.side[1]=='e') ++baseloc; /* else C'=2*C */
            first=0;
          }
/* See where this instance is located */
          thisloc=2*(ulcol+iodef.col);
          if (iodef.side[1]=='w') --thisloc;
          if (iodef.side[1]=='e') ++thisloc;
        } else { /* edgeport */
          thisloc=2*circuit->placedcol[netnode->ioindex];
          side=circuit->placedside[netnode->ioindex];
          if (side=='w') --thisloc;
          if (side=='e') ++thisloc;
          if (first==1){baseloc=thisloc;first=0;}
        }

/* Now see if we cross column COL */
        if (((baseloc-2*col)*(thisloc-2*col)) < 0) ++cutsize;
      } /* End of net */
      total_cutsize+=cutsize;
      if (cutsize > max_cutsize) max_cutsize=cutsize;
    } /* End of all networks */
  } /* All columns scanned */

/* Now scan the rows */
  for (row=0;row<circuit->height;row++){
    get_net(circuit,1);
/* check this net for crossings over row */
    while (0==get_net(circuit,0)){
      first=1;
      while (NULL != (netnode=get_node())){
        if (netnode->edge == 0){
          ulrow=netnode->c->ulrow; /* Placement of component inside matrix */
          iodef=netnode->c->libdef->io[netnode->ioindex];
          height=netnode->c->libdef->h;
          width=netnode->c->libdef->w;
          rotation=netnode->c->rotation;
          rotate_iodef(&iodef,height,width,rotation);
          if (first==1){
/* Find modified base row */
            baseloc=2*(ulrow+iodef.row);
            if (iodef.side[1]=='n') --baseloc;
            if (iodef.side[1]=='s') ++baseloc; /* else R'=2*R */
            first=0;
          }
/* See where this instance is located */
          thisloc=2*(ulrow+iodef.row);
          if (iodef.side[1]=='n') --thisloc;
          if (iodef.side[1]=='s') ++thisloc;
        } else { /* edgeport */
          thisloc=2*circuit->placedrow[netnode->ioindex];
          side=circuit->placedside[netnode->ioindex];
          if (side=='n') --thisloc;
          if (side=='s') ++thisloc;
          if (first==1){baseloc=thisloc;first=0;}
        }

/* Now see if we cross row */
        if (((baseloc-2*row)*(thisloc-2*row)) < 0) ++cutsize;
      } /* End of net */
      total_cutsize+=cutsize;
      if (cutsize > max_cutsize) max_cutsize=cutsize;
    } /* End of all networks */
  } /* All rows scanned */

/* now compute cost */
  return(.3*(double)max_cutsize +
         .7*(double)total_cutsize/(double)(circuit->height+circuit->width));
}

rotate_iodef(struct iodef *iodef,int height,int width,int rotation)
{
  int i,r,c,rcount;
  for (rcount=0;rcount<rotation;rcount++){ /* Rotate CW once */
    r=iodef->row;c=iodef->col;
    iodef->row=c;
    iodef->col=(height-1)-r; /* New [r,c] coords */
    i=height;height=width;width=i; /* Swap h & w */
    switch (iodef->side[1]){
      case 'w':iodef->side[1]='n';break;
      case 'n':iodef->side[1]='e';break;
      case 'e':iodef->side[1]='s';break;
      case 's':iodef->side[1]='w';break;
      default: fprintf(stderr,"ERROR: Unknown side <%s> in rotate_iodef()\n",
                               iodef->side);
      return(1);
    } /* One rotation done */
  } /* All rotations done */
  return(0);
}
