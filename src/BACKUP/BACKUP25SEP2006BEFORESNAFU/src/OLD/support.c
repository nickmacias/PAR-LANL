#include "par.h"
/*
 * match() accepts a base string, a pattern, and a location, and checks
 * to see if the patternm appears in the base string beginning at the
 * given location. It returns 1 if the pattern matches, otherwise 0
 */

int match(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=0;i<strlen(pattern);i++){
    if (pattern[i]!=base[startloc]) return(0);
    if (base[startloc++]=='\0') return(0);
  }
  return(1); /* Matches! */
}

/*
 * find() searches for a pattern in a base string, beginning
 * at a given location. It returns the location, or (-1)
 * if the pattern is not found.
 */
int find(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=startloc;i<strlen(base);i++)
    if (1==match(base,pattern,i)) return(i);
  return(-1);
}

char substr_return[2048]; /* but play nice... */
/* substr() - returns a substring */
/* DO NOT NEST! */

char *substr(string,start,len,end) /* Inclusive! */
/* If LEN=0, then END gives ending position */
char *string;
int start,len,end;
{
  int i,j;
  if (len != 0) end=start+len-1; /* We'll use END regardless */
  j=0; /* output location */
  for (i=start;i<=end;i++){
    substr_return[j++]=string[i];
  }
  substr_return[j]='\0';
  return(substr_return);
}

/* Circuit initialization */
init_circuit(struct circuit *circuit)
{
  circuit->components=malloc(sizeof(struct instance));
  circuit->components->next=(struct instance*) NULL;
  circuit->nets=malloc(sizeof(struct netlist));
  circuit->nets->next=(struct netlist*) NULL;
  circuit->library=malloc(sizeof(struct libentry));
  circuit->library->next=(struct libentry*)NULL;
}

static int raninit=0;
/* Return an integer between 0 and max-1 */
rando(int max)
{
  if (raninit==0) srand(827771);
  raninit=1;

  return(rand() % max);
}

/* Here we have a set of routines for traversing the various data structures */

/* get_net() initializes things for a subsequent get_node().
 * get_net(1) resets to the first net; get_net(0) does next net
 * return value of 0=OK, 1=no more nets
 */
static struct netlist *nl;
static struct netnode *nn;
get_net(struct circuit *circuit,int init)
{
  if (init==1){ /* re-init */
    nl=circuit->nets;
    return(0);
  }
  if (nl->next==(struct netlist*) NULL) return(1); /* No more networks */
  nl=nl->next; /* Next net in linked list */
  nn=nl->net; /* Start of this net */
  return(0);
}

struct netnode *get_node()
{
  if (nn->next == (struct netnode*) NULL) return(nn->next);
  nn=nn->next;
  return(nn);
}

insert_at_head_of_current_net(struct netnode *node)
{
  node->next=(nl->net)->next;
  (nl->net)->next=node;
}

/* We may want to return to the start of this current net... */
get_node_reset()
{
  nn=nl->net; /* Start of this (same) net */
}

/* Routines to retrieve instances. First, a specific instance */
struct instance *get_names_instance(struct circuit *circuit,char *name)
{
  struct instance *temp;
  temp=circuit->components;
  while (temp->next != NULL){
    temp=temp->next;
    if (0==strcmp(temp->name,name)) return(temp);
  }
  return(NULL);
}

/* Count instances */
count_instances(struct circuit *circuit)
{
  int i;
  struct instance *instance;

  i=0;
  instance=next_instance(circuit,1);
  while (NULL != (instance=next_instance(circuit,0))) ++i;
  return(i);
}

/* Find UL [r,c] of an instance */
find_instance_rc(struct circuit *circuit, struct instance *instance,
                  int *rout, int *cout,int *rotation)
{
  int r,c;
  struct instance *in;
  for (r=0;r<circuit->height;r++){
    for (c=0;c<circuit->width;c++){
      if (getcell(circuit,r,c)->instance == instance){
        *rout=r;*cout=c;*rotation=instance->rotation;
        return(0);
      }
    }
  }
  return(1);
}

/* Retrieve nth instance */
struct instance *get_indexed_instance(struct circuit *circuit,int ind)
{
  int i;
  struct instance *instance;

  i=0;
  instance=next_instance(circuit,1);
  while (NULL != (instance=next_instance(circuit,0))){
    if (i==ind) return(instance);
    ++i;
  }
  return(NULL);
}

/* Successively return instances */
static struct instance *nextinst;
struct instance *next_instance(struct circuit *circuit,int init)
{
  if (init==1){
    nextinst=circuit->components;
    return(NULL); /* Unused */
  }
  if (nextinst->next == NULL) return(NULL);
  nextinst=nextinst->next;
  return(nextinst);
}

struct cell *getcell(struct circuit *circuit,int row,int col)
{
  return(&circuit->matrix[row*circuit->width + col]);
}

static int initgo=1;
static FILE *gridfp;

grid_output(char *s)
{
  if (initgo==1){
    gridfp=fopen("test.grd","w");
    initgo=0;
  }
  fprintf(gridfp,"%s",s);
  fflush(gridfp);
}

/* Cheap ASCII dump of matrix */
display_placement(struct circuit *circuit,int key)
/* key=0 means just print placement
 * key=1 means show the legend
 * key=2 means create .GRD file
 */
{
  char m[64][64];
  int h,w,r,c,i,j,k,l,ulcol,ulrow,rotation,realwidth,realheight,r2,c2,
      r1,c1;
  char ch;
  struct instance *instance;
  struct cell *cell;
  char buffer[1024],entry[1024];
  struct libentry *le;

  h=circuit->height;w=circuit->width;

  if (key==2){ /* Create .GRD file */
    sprintf(buffer,"GRID_FILE\n%d\n%d\n",h,w);
    grid_output(buffer); /* Output header */
    for (r=0;r<h;r++){
      for (c=0;c<w;c++){
        cell=getcell(circuit,r,c);
        if ((cell->instance == NULL) ||
            (cell->instance == EDGEINST)) grid_output("\n");
        else {
          le=cell->instance->libdef;
          ulrow=cell->instance->ulrow;
          ulcol=cell->instance->ulcol;
          rotation=(cell->instance->rotation)%4;
/* When we're indexing cell[r,c], we need to know the width of this
 * circuit. Normally this is le->w, but if the circuit is rotated
 * 90 or 270 degrees, it will be le->h
 */
          realwidth=((rotation==1)||(rotation==3))?le->h:le->w;
          realheight=((rotation==1)||(rotation==3))?le->w:le->h;

/* Find loc within component */
          r1=r-ulrow;c1=c-ulcol;
/* also need to adjust row and col based on rotation */
          switch (rotation){
            case 0:r2=r1;c2=c1;break;
            case 3:r2=c1;c2=(le->w-1)-r1;break;
            case 2:r2=(le->h-1)-r1;c2=(le->w-1)-c1;break;
            case 1:r2=(le->h-1)-c1;c2=r1;break;
          }
/*printf("%s rot=%d:[r,c]=[%d,%d]  [r1,c1]=[%d,%d]  [r2,c2]=[%d,%d]\n",
cell->instance->basename,rotation,r,c,r1,c1,r2,c2);*/
          strcpy(entry,le->cells[(r2*realwidth)+c2]);
          rotate_entry(entry,rotation);
          grid_output(entry);grid_output("\n");
/*printf("%d,%d:%s rot=%d\n",r,c,entry,rotation);*/
        }
      }
    }
    return;
  }

/* Initialize output array */
  for (r=0;r<h;r++){for(c=0;c<w;c++){m[r][c]='.';}}

/* Scan matrix[] and save info */
  ch='a';
  instance=next_instance(circuit,1);

  while (NULL != (instance=next_instance(circuit,0))){
    for (r=0;r<h;r++){
      for (c=0;c<w;c++){
        cell=getcell(circuit,r,c);
        if (cell->instance == instance) m[r][c]=ch;
      }
    }
    ch=ch+1; /* Next character */
  }

/* Now assign edge ports (use numbers) */
  for (i=0;i<circuit->numio;i++){
    r=circuit->placedrow[i];
    c=circuit->placedcol[i];
    if ((r >= 0) && (c >= 0)) m[r][c]='0'+i;
  }

  if (key==1){
/* Output the key */
    ch='a';
    instance=next_instance(circuit,1);
    while (NULL != (instance=next_instance(circuit,0))){
      printf("%c:%s(%s)\n",ch,instance->name,instance->basename);
      ch=ch+1;
    }
    printf("\n");
    return;
  }

/* Now dump the output */
  reset_page();
  for (r=0;r<h;r++){
    for (c=0;c<w;c++){
      printf("%c",m[r][c]);
    }
    printf("\n");
  }
  printf("\n");
}

clear_page()
{
  if (fancy==1) printf("%c[2J",27);
}

reset_page()
{
  if (fancy==1) printf("%c[1;1H",27);
}

iabs(int x)
{
  return((x<0)?(-x):x);
}

rotate_entry(char *entry,int rotation)
{
/* Parse standard loader .GRD file entry, and bump the rotations */
  char output[1024],start[1024],end[32],temp[1024];
  int more,look,i,r;


  strcpy(output,""); /* Build this output string */
  more=1;
  look=0;
  while (more==1){
    i=find(entry,"",look); /* Find libname terminator */
    i=find(entry,"",i+1); /* Find cellname terminator */
    strcat(output,substr(entry,look,0,i)); /* lib^Bcell^B */
    look=find(entry,"",i+1); /* Is there a ^B after rot? */
    if (look==-1){ /* No */
      strcpy(temp,substr(entry,i+1,0,strlen(entry)-1)); /* Rotation */
      more=0; /* End of parse */
    } else { /* ^B marks start of next member of composite cell */
      strcpy(temp,substr(entry,i+1,0,look-1)); /* Rotation */
      ++look; /* Look after ^B */
    }
/* Now bump up temp */
    sscanf(temp,"%d",&r);
    r=(r+rotation)%4;
    sprintf(temp,"%d",r);

    strcat(output,temp);
    if (more==1) strcat(output,"");
  }
  strcpy(entry,output); /* Save the output */
  return;
}
