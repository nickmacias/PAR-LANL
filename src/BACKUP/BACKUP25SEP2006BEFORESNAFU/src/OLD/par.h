#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

/*
 * Various structures for storing circuits, nets, etc.
 */

/*
 * The network structures are a bit funky, so we'll do some ASCII art here:

CIRCUIT
-------
 .NETS ---->NETLIST
            -------
             .NET (NULL)
             .NEXT                   /          \
               |                     |1st netnode|
               |                     |   node    |
               |                     \  ======>  /
               v
/   1st  \  NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
| netlist|  -------   |   -------  |   -------  |   -------  |
|  node  |   .NET-----+   .C       |   .C       |   .C       |
|   ||   |   .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
|   ||   |     |          .NEXT----+   .NEXT----+   .NEXT----+
|   ||   |     |
\   vv   /     |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
            NETLIST   +-->NETNODE  +-->NETNODE  +-->NETNODE  +-->...
            -------   |   -------  |   -------  |   -------  |
             .NET-----+   .C       |   .C       |   .C       |
             .NEXT        .IOINDEX |   .IOINDEX |   .IOINDEX |
               |          .NEXT----+   .NEXT----+   .NEXT----+
               |
               |
               v
               .
               .
               .

netnode.c is an instance of a component
netnode.ioindex is the index of an I/O port

Place and Route algorithm taken from Akshay Sharma, "Development of a
Place and Route Tool for the RaPiD architecture," Master's Project,
Autumn Quarter, 2001, University of Washington (Advisor Scott A. Hauck).

See that paper for earlier sources for the simulated annealing placement
algorithm and the Pathfinder routing algorithm.

 */

/* circuit holds the main circuit, as well as (possibly) subcircuits */
struct circuit{
  char *name; /* user's name for this circuit */
  int height,width; /* Matrix dimensions */

/* members for top-level I/O */
  char **io; /* array of input and output names */
  int *iodir; /* 0=input, 1=output */
  char *ioside; /* n, s, w or e ('' if missing) */
  int *iocell1,*iocell2; /* range of locs (-1 if missing) */
  int numio; /* shortcut to # of IO defs */
  char *placedside; /* fill when edgeport is placed */
  int *placedrow,*placedcol;

  struct instance *components; /* Linked list of instances of components */
  struct netlist *nets; /* Linked list of all nets */
  struct libentry *library; /* Linked list of library entries */
  struct cell *matrix; /* Cells within the (sub-)matrix */
  /* struct route routes; */ /* Linked list of all routes */
/* As usual, first nodes are not used... */
};

/* Linked list of all nets */
struct netlist{
  struct netnode *net; /* Headnode of a single net */
  struct netlist *next;
};

/* Single node within a net */
struct netnode{
/* I/O for the toplevel circuit is special, and is indicated by edge=1 */
  int edge; /* 0=normal instance port;1=edge port */
  struct instance *c;
  int ioindex; /* These define the connection point to the net */
               /* If edge=0, actual point is c->libdef->io[ioindex] */
               /* If edge=1, this is an index to circuit->io[] */
  struct netnode *next;
};

struct instance{
  char *name; /* Instance name */
  char *basename; /* Basename of object of which this is an instance */
                  /* (may be a circuit!?) */

/* These next two members are paired with each other
 * ioname[7] corresponds to libentry->io[ioindex[7]], etc.
 */
  char **ioname; /* Ordered array of input/output names used in this instance */
  int *ioindex; /* index in struct libentry */

  int ulrow, ulcol, rotation; /* Placement within matrix */
  struct libentry *libdef; /* Point to library entry */
  struct instance *next; /* Pointer to next node */
};

/* Library structure - store basename, size, I/O defs and cells
 * for an entry in a library */

struct libentry{
  char *basename; /* basename in struct instance */
  char *desc; /* Null-terminated, freeform description */
  int h,w; /* dimensions */
  char **cells; /* Array of h*w standard .GRD file entries */
  int numio; /* Number of I/O defs in io[] array */
  struct iodef *io; /* Array of I/O ports */
  struct libentry *next; /* linked list! */
};

struct iodef{ /* Define a single I/O port */
  char *iobasename; /* native name of I/O port */
  int row,col; /* cell containing the I/O port */
  char side[4]; /* i.e., dso,, dwi, etc. */
};

/* Structures for cells within the matrix */
struct cell{
  /* int inputs,outputs; */ /* Masks showing input ports in use(1) or free(0) */
  int hcost_N,hcost_S,hcost_E,hcost_W; /* Historical costs, 4 inputs */
  int pcost_N,pcost_S,pcost_E,pcost_W; /* Penalty costs, 4 inputs */
/* -1 means never available (cell belongs to a component) */

  struct netnode *netN,*netS,*netW,*netE; /* Net to which input belongs */
/*  null if input is not connected to a net */

  struct instance *instance; /* Pointer to this instance, or NULL if free */
  int row,col; /* Location within component */
};

/* MASKS for cell inputs and outputs */
/***
#define DN 1
#define DS 2
#define DW 4
#define DE 8
#define CN 16
#define CS 32
#define CW 64
#define CE 128
***/

char *substr();
struct netnode *make_node(struct circuit*,char *,char *);
struct netnode *get_node();
struct instance *get_named_instance(struct circuit*,char *);
struct instance *next_instance(struct circuit*,int);
int rando(int);
struct cell *getcell(struct circuit *,int,int);
int place_component(struct circuit *, struct instance *,int,int,int);
int place_edgeport(struct circuit *, int epnum,int,int,char);
struct instance *get_indexed_instance(struct circuit *,int);
int rotate_iodef(struct iodef *,int,int,int);

/* Special instance for identifying edges */
static struct instance *EDGEINST=(struct instance*) 1;

static int fancy=0; /* 1 means show circuit with page clears, etc. */
