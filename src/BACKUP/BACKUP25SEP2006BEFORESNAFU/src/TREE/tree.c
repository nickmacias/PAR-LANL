#include <stdio.h>

struct treenode{
  struct treenode *n,*s,*w,*e,*back;
  char backside; /* side of parent from which we came */
  int data;
};

struct treenode *newtreenode(int data)
{
  struct treenode *new;
  new=malloc(sizeof (struct treenode));
  new->n=new->s=new->w=new->e=new->back=NULL;
  new->data=data;
  new->backside=' ';
  return(new);
}

add_treenode(struct treenode *root,char side,struct treenode *new)
{
  switch (side){
    case 'n':root->n=new;break;
    case 's':root->s=new;break;
    case 'w':root->w=new;break;
    case 'e':root->e=new;break;
    default:fprintf(stderr,"Unknown side <%c> in add_treenode",side);
  }
  new->back=root;
  new->backside=side;
}

traceback(struct treenode *t)
{
  while (t != NULL){
    printf("Data=%d, came from %c of parent. ",t->data,t->backside);
    t=t->back;
  }
}

main()
{
  struct treenode *t1,*t2,*t3;
  t1=newtreenode(11); t2=newtreenode(2); t3=newtreenode(333);
  add_treenode(t1,'s',t2);
  add_treenode(t2,'w',t3);

  traceback(t3);
  traceback(t3);
}
