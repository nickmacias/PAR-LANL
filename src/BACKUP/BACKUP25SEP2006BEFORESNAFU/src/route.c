#include "par.h"
#include <math.h>

static struct stat_block null_stat;

struct stat_block *route(struct circuit *circuit,struct matrix *matrix)
{
  int num_nets,i,over,j,oversum,count;
  struct treenode **routing_tree_array;
  char buffer[128];
  struct pqnode_list pqnode_list; // Save nodes we pop from queue
  struct stat_block *stats;

  pqnode_list.next=NULL;
  routing_tree_array=NULL;
  printf("Beginning routing\n");
  //matrix=malloc(sizeof (struct matrix));
  //mcheck((char *)matrix);

// Setup null stat block in case we do no routing...
  null_stat.Mlen_str=null_stat.Mlen_turn=null_stat.Mlen_sum=
  null_stat.Glen_str=null_stat.Glen_turn=null_stat.Glen_sum=
  null_stat.Glen_str_n=null_stat.Glen_turn_n=null_stat.Glen_sum_n=
  null_stat.cells_route=null_stat.cells_comp=null_stat.cells_total=0;

/* load connectivity and initial weights */
  num_nets=init_route_matrix(circuit,matrix);
// This populates the matrix structure

// Set up space for routing trees (one per net)
// routing_tree_array*[] will be final routing solution
  routing_tree_array=malloc(((num_nets==0)?1:num_nets) *
                            sizeof (struct treenode *));
  mcheck((char *)routing_tree_array);
  for (i=0;i<((num_nets==0)?1:num_nets);i++)
    routing_tree_array[i]=NULL;

  over=1;
  count=0;
  while (over>=1){
    printf("%d:",++count);
//gets(buffer);
    oversum=0; // over-use for this (complete) routing pass
    reset_matrix(matrix); // prepare for next pass


/* Route each net */
    get_net(circuit,1);
    while (0==get_net(circuit,0)){
      route_net(circuit,matrix,num_nets,routing_tree_array,&pqnode_list);
    }
    adjust_costs(matrix,routing_tree_array,num_nets); // do this
    over=dump_cells(matrix,circuit,&oversum);
    printf("<%d> ",oversum);fflush(stdout);
  }
  printf("\n");

// Finished routing here - show the final routes
  compute_routes(matrix,num_nets,routing_tree_array);

// Compute routing statistics (delays)
  stats=compute_stats(circuit,matrix,num_nets,routing_tree_array);
  return stats;
}

/*
 * use get_node() calls to read net's source, and all sinks
 */
route_net(struct circuit *circuit,
          struct matrix *matrix, int num_nets,
          struct treenode **routing_tree_array,
          struct pqnode_list *pqnode_list)
{
  struct netnode *source, *sink;
  int num_sinks,sinki,need_sink,fo,current_cost,netnum,i;
  struct treenode *routing_tree;
  struct pqnode queue;  // initial node in priority queue
  struct pqnode *pqnode,*pqnode2;
  struct pqnode_list *tmp_pqnode_list;

  queue.next=NULL; // empty queue
  source=get_node();  // source of first net

  netnum=0;
  while (source != NULL){ /* Route this net */
    free_rt(routing_tree_array[netnum++]);
    routing_tree=init_rt(circuit,matrix,source,netnum); // build a new tree
    num_sinks=count_nodes();
    for (sinki=0;sinki<num_sinks;sinki++){  // route to all sinks
//printf("\n\nnetnum=%d sinknum=%d\n",netnum,sinki);
      free_pq(&queue); // release nodes not in tree
      init_pq(&queue,routing_tree);  // initialize queue to tree (cost=0)
      need_sink=1;
      while (need_sink==1){
        pqnode=remove_pq_node(&queue);
        if (pqnode == NULL){
#ifdef DEBUG
          printf("\nRan out of nodes!, net=%d\n",netnum);exit(1);
#endif
          fprintf(stderr,"\nRan out of nodes!, net=%d\n",netnum);return(1);
        }
//printf("removed pqnode [%d,%d]\n",pqnode->r,pqnode->c);
// Add to pqnode_list
        add_pqnode_list(pqnode_list,pqnode);
        current_cost=pqnode->pqcost;  // cost from src to pqnode
        for (fo=0;(fo<=3) && (need_sink==1);fo++){  // check each fanout
          pqnode2=next_fanout(netnum,pqnode,matrix,fo); // fanout from pqnode
          if (pqnode2 != NULL){
//printf("Fanned out to pqnode2 [%d,%d]\n",pqnode2->r,pqnode2->c);
// Is this node a sink?
            if ((pqnode2->rtnode==NULL) && // Not yet in routing tree
                (pqnode2->ss == (-netnum))){ // is a sink from current source
              backtrace(pqnode2); // update costs and RT
              reload_visited(matrix,routing_tree,netnum);
              need_sink=0; // now go find another sink
            } else if ((pqnode2->ss == 0) && // not sink/source on another net
                       (matrix->cells[pqnode2->r*matrix->width+pqnode2->c]
                                     .occupied==' ')){ // available
              add_pq_node(&queue,pqnode2,pqnode2->cost+current_cost+1);
            }
          }
        } // all fanouts done
      }  // found a sink
    } // all sinks found

// Now we can free nodes from pqnode_list if they're not in an RT
    free_pqnode_list(pqnode_list);

    routing_tree_array[netnum-1]=routing_tree; // save tree for final analysis
    if (1==get_net(circuit,0)) source=NULL;
      else source=get_node();  // source of next net
  }
}

/*
 * Initialize the routing-related aspects of the cell[][] array.
 * This is only done once, since it loads static information into the cells
 * Return # of nets (0 means error)
 */
init_route_matrix(struct circuit *circuit,struct matrix *matrix)
{
  int ind,i,j,k,row,col,done,source,num_net;
  int ulcol,ulrow,height,width,rotation;
  int finalrow,finalcol;
  char side,finalside;
  struct cell *cell;
  struct netnode *netnode,*sourcenode,*prevnode;
  struct iodef *iodef,iodefcopy;

// Setup empty matrix structure
  matrix->height=circuit->height;
  matrix->width=circuit->width;
//if (matrix->cells !=  NULL) free(matrix->cells);
  matrix->cells=malloc(matrix->height*matrix->width*sizeof (struct cellport));
  mcheck((char *)matrix->cells);

  for (row=0;row<circuit->height;row++){
    for (col=0;col<circuit->width;col++){
      ind=row*matrix->width + col;
// Setup cellport structure
      cell=getcell(circuit,row,col);
      matrix->cells[ind].ss[DNI]=0;
      matrix->cells[ind].ss[DSI]=0;
      matrix->cells[ind].ss[DWI]=0;
      matrix->cells[ind].ss[DEI]=0;
      matrix->cells[ind].ss[DNO]=0;
      matrix->cells[ind].ss[DSO]=0;
      matrix->cells[ind].ss[DWO]=0;
      matrix->cells[ind].ss[DEO]=0; // set these below
      matrix->cells[ind].edge[DNI]=' ';
      matrix->cells[ind].edge[DSI]=' ';
      matrix->cells[ind].edge[DWI]=' ';
      matrix->cells[ind].edge[DEI]=' ';
      matrix->cells[ind].edge[DNO]=' ';
      matrix->cells[ind].edge[DSO]=' ';
      matrix->cells[ind].edge[DWO]=' ';
      matrix->cells[ind].edge[DEO]=' '; // edge I/O flags
      matrix->cells[ind].hcost[N]=0;
      matrix->cells[ind].hcost[S]=0;
      matrix->cells[ind].hcost[W]=0;
      matrix->cells[ind].hcost[E]=0;
      matrix->cells[ind].pcost[N]=1;
      matrix->cells[ind].pcost[S]=1;
      matrix->cells[ind].pcost[W]=1;
      matrix->cells[ind].pcost[E]=1;
      matrix->cells[ind].occupied=(cell->instance==NULL)?' ':'1';
      matrix->cells[ind].r=row;
      matrix->cells[ind].c=col;
      matrix->cells[ind].visited[N]=' ';
      matrix->cells[ind].visited[S]=' ';
      matrix->cells[ind].visited[W]=' ';
      matrix->cells[ind].visited[E]=' ';
      matrix->cells[ind].counted[N]=' ';
      matrix->cells[ind].counted[S]=' ';
      matrix->cells[ind].counted[W]=' ';
      matrix->cells[ind].counted[E]=' ';
    }
  }

/* Next we set the net-related elements */
/* First, scan each net, and move the source node to the head of the net list.
 * Then, sweep each net again, and for each sink node, find the cell and side
 * of the input, and store the SOURCE netnode in the apropriate netX
 */

  get_net(circuit,1); /* Initialize the net retriever */
  num_net=0;

  while (0==get_net(circuit,0)){
    ++num_net;
    sourcenode=prevnode=NULL;
/* Identify the source of this net */
    done=0; /* Set once we find a source node */
    while ((done==0) && (NULL != (netnode=get_node()))){
      if (netnode->edge==1){
        source=(circuit->iodir[netnode->ioindex]==0)?1:0; /* main circuit input? */
      } else {
        i=netnode->ioindex; /* Connection point to net */
        iodef=&netnode->c->libdef->io[i]; /* IO definition for connection pt */
        source=(iodef->side[2]=='o')?1:0;
      }
      if (source==1){ /* Source! */
        sourcenode=netnode;
/* Is this the first node? */
        if (prevnode!=NULL){ /* move this node to front of net */
          prevnode->next=sourcenode->next;
          insert_at_head_of_current_net(sourcenode);
        }
        done=1; /* Exit this loop */
      } /* End of source node processing */
      prevnode=netnode; /* Remember next node's predecessor */
    } /* End of net */

/* Now scan this net again, and add the port information to the cell array */
    get_node_reset();
    printf("Net %d: ",num_net);
    while (NULL != (netnode=get_node())){
      if (netnode->edge==0){
        ulrow=netnode->c->ulrow;
        ulcol=netnode->c->ulcol; /* Placement of component inside matrix */
        iodefcopy=netnode->c->libdef->io[netnode->ioindex];
        height=netnode->c->libdef->h;
        width=netnode->c->libdef->w;
        rotation=netnode->c->rotation;
        rotate_iodef(&iodefcopy,height,width,rotation); /* Rotate this COPY */
        finalrow=ulrow+iodefcopy.row;
        finalcol=ulcol+iodefcopy.col;
        finalside=iodefcopy.side[1];
      } else { /* edgeport */
        finalrow=circuit->placedrow[netnode->ioindex];
        finalcol=circuit->placedcol[netnode->ioindex];
        finalside=circuit->placedside[netnode->ioindex];
      }

      printf("[%d,%d]%c(edge=%d) ",
             finalrow,finalcol,finalside,netnode->edge);
// now update matrix->cells[]
      ind=finalrow*matrix->width + finalcol;
      if ((sourcenode==netnode) && (netnode->edge==0)){ // normal source
        switch(finalside){
          case 'n':matrix->cells[ind].ss[DNO]=num_net;break;
          case 's':matrix->cells[ind].ss[DSO]=num_net;break;
          case 'w':matrix->cells[ind].ss[DWO]=num_net;break;
          case 'e':matrix->cells[ind].ss[DEO]=num_net;break;
        }
      } else if ((sourcenode==netnode) && (netnode->edge==1)){ // edge source
        switch(finalside){
          case 'n':matrix->cells[ind].ss[DNI]=num_net;
                   matrix->cells[ind].edge[DNI]='1';break;
          case 's':matrix->cells[ind].ss[DSI]=num_net;
                   matrix->cells[ind].edge[DSI]='1';break;
          case 'w':matrix->cells[ind].ss[DWI]=num_net;
                   matrix->cells[ind].edge[DWI]='1';break;
          case 'e':matrix->cells[ind].ss[DEI]=num_net;
                   matrix->cells[ind].edge[DEI]='1';break;
        }
      } else if ((sourcenode!=netnode) && (netnode->edge==0)){ // normal sink
        switch(finalside){
          case 'n':matrix->cells[ind].ss[DNI]=(-num_net);break;
          case 's':matrix->cells[ind].ss[DSI]=(-num_net);break;
          case 'w':matrix->cells[ind].ss[DWI]=(-num_net);break;
          case 'e':matrix->cells[ind].ss[DEI]=(-num_net);break;
        }
      } else if ((sourcenode!=netnode) && (netnode->edge==1)){ // edge sink
        switch(finalside){
          case 'n':matrix->cells[ind].ss[DNO]=(-num_net);
                   matrix->cells[ind].edge[DNO]='1';break;
          case 's':matrix->cells[ind].ss[DSO]=(-num_net);
                   matrix->cells[ind].edge[DSO]='1';break;
          case 'w':matrix->cells[ind].ss[DWO]=(-num_net);
                   matrix->cells[ind].edge[DWO]='1';break;
          case 'e':matrix->cells[ind].ss[DEO]=(-num_net);
                   matrix->cells[ind].edge[DEO]='1';break;
        }
      }
    }
    printf("\n\n");
  } /* End of all nets */
  return(num_net);
}

// useful output routine
dump_cells(struct matrix *matrix,struct circuit *circuit,int *ov)
{
  int r,c,i,over,oversum;
  struct cellport *cell;

  over=oversum=0; // set if anything is over-routed

  for (r=0;r<matrix->height;r++){
    for (c=0;c<matrix->width;c++){
      i=r*matrix->width + c;
      cell=&matrix->cells[i];
      if (/***(cell->hcost[N] > 0) ||
          (cell->hcost[S] > 0) ||
          (cell->hcost[W] > 0) ||
          (cell->hcost[E] > 0) ||***/
          (cell->pcost[N]  > 1) ||
          (cell->pcost[S]  > 1) ||
          (cell->pcost[W]  > 1) ||
          (cell->pcost[E]  > 1)){
         ++over;
         oversum+=cell->pcost[N]+cell->pcost[S]+cell->pcost[W]+cell->pcost[E];

#ifdef DEBUG
         if (over==1) printf("OVER-ROUTED RESOURCES:\n");
         printf("[%d,%d] pcost(nswe)=%d %d %d %d hcost(nswe)=%d %d %d %d\n",
                 r,c,cell->pcost[N],cell->pcost[S],
                 cell->pcost[W],cell->pcost[E],
                 cell->hcost[N],cell->hcost[S],
                 cell->hcost[W],cell->hcost[E]);
#endif
        *ov+=oversum;
      }
    }
  }
  return(over);
}
