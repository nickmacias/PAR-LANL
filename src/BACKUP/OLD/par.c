#include "par.h"

main(argc,argv)
int argc;
char **argv;
/* Usage: main [input] */
{
  struct circuit *circuit; /* Main circuit! */
  FILE *fp;
  char buffer[1024];
  int errcnt=0; /* # of parse etc. errors */
  int i,j;

  if (argc != 2){
    fprintf(stderr,"Usage: %s inputfile\n",argv[0]);
    exit(1);
  }

  circuit=malloc(sizeof(struct circuit));
  init_circuit(circuit);

/*
 * Read the netlist description
 */
  fp=fopen(argv[1],"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Cannot open input file %s\n",argv[1]);
    exit(1);
  }

  while (0 == get_line(fp,buffer)){
    /*printf("<%s>\n",buffer);*/
    if (0 != parse_line(circuit,buffer)){ /* Load structures accordingly */
      fprintf(stderr,"ERROR: Cannot parse <%s>\n",buffer);
      ++errcnt;
    }
/* Structures have been loaded - continue! */
  }
  fclose(fp);
  if (errcnt > 0){
    fprintf(stderr,"Total of %d error%s\n",errcnt,(errcnt==1)?"":"s");
    exit(1);
  } else fprintf(stderr,"\nNo errors detected!\n\n");

  dump_circuit(circuit); /* Display results so far */

/* Run the placer */
  place(circuit);

/* Write final circuit placement into a LOADER .grd file */
  display_placement(circuit,2);

/* Route the circuit */
  route(circuit);
}
