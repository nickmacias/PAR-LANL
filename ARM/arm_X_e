;
; arm_X_e - initial supercell in ARM spiral
; This block is identical to arm_w_e, except there is no incoming
; running sum. Instead, the initial sum is taken to be the constant offset
; for the final Heavyside calculation.
; Thus, this block needs to be able to store that constant.
;
; It is supplied via the WEIGHT inputs, in two nibbles.
; ld_const0 and ld_const1 are toggled to load them
; and they are sent to the A inputs of the adder.
;

.define arm_X_e(64,64,\

;*** WESTERN INPUTS
		wcount_tickw:in,\ ; toggle to tick [R,C] weight load select
		ld_const0_w:in,ld_const1_w:in,\ ; Load constant offset
		cin0:in,cin1:in, \ ; Incoming column info

		ldw:in,\ ; Load next state


		winw0:in,winw1:in,winw2:in,winw3:in,\  ; weight to store
		wldw:in,\  ; LOAD signal to store weight


;*** EASTERN OUTPUTS
		wcount_ticke:out,\
		wcout0:out,wcout1:out,\ ; Column of cell in which to set wts
		cout0:out,cout1:out,\ ; outgoing column info
		ld_const0_e:out,ld_const1_e:out,\ ; Load constant offset

		lde:out,\

		woute0:out,woute1:out,woute2:out,woute3:out,\ ; weight to store

		wlde:out,\ ; WEIGHT Load SIGNAL


;*** NORTHERN INPUTS

		wcount_tickn:in,\ ; toggle to tick [R,C] weight load select
		rin1:in,rin0:in,\ ; Incoming row info

		ld_const0_n:in,ld_const1_n:in,\

		ldn:in,\

		winn3:in,winn2:in,winn1:in,winn0:in,\; Incoming weight
		wldn:in,\ ; LOAD WEIGHT signal


;*** SOUTHERN OUTPUTS
		wcount_ticks:out,\
		wrout1:out,wrout0:out,\ ; Row of cell in which to set wts
		rout1:out,rout0:out,\;Row info

		ld_const0_s:out,ld_const1_s:out,\
		lds:out,\

		wouts3:out,wouts2:out,wouts1:out,wouts0:out,\; Weight
		wlds:out,\; LOAD WEIGHT signal

;*** Running sum: in and out

		;*sin0:in,sin1:in,sin2:in,sin3:in,\
		;*sin4:in,sin5:in,sin6:in,sin7:in,\  ; running sum in

		sout0:out,sout1:out,sout2:out,sout3:out,\
		sout4:out,sout5:out,sout6:out,sout7:out \ ; running sum out
	)


;*** SUM inputs- side depends on supercell position
;*.locate sin0,w,15,15
;*.locate sin1,w,18,18
;*.locate sin2,w,21,21
;*.locate sin3,w,24,24
;*.locate sin4,w,27,27
;*.locate sin5,w,30,30
;*.locate sin6,w,33,33
;*.locate sin7,w,36,36

;*** SUM outputs - side depends on supercell position
.locate sout0,e,15,15
.locate sout1,e,18,18
.locate sout2,e,21,21
.locate sout3,e,24,24
.locate sout4,e,27,27
.locate sout5,e,30,30
.locate sout6,e,33,33
.locate sout7,e,36,36


;*** Other edge I/O
;*** Fix input/output locations so supercells line up
.locate wcount_tickw,w,6,6
.locate cin0,w,9,9
.locate cin1,w,12,12
.locate winw0,w,39,39
.locate winw1,w,42,42
.locate winw2,w,45,45
.locate winw3,w,48,48
.locate wldw,w,51,51
.locate ld_const0_w,w,54,54
.locate ld_const1_w,w,57,57
.locate ldw,w,60,60


.locate wcount_tickn,n,6,6
.locate rin1,n,9,9
.locate rin0,n,12,12
.locate winn3,n,39,39
.locate winn2,n,42,42
.locate winn1,n,45,45
.locate winn0,n,48,48
.locate wldn,n,51,51
.locate ld_const0_n,n,54,54
.locate ld_const1_n,n,57,57
.locate ldn,n,60,60


.locate wcount_ticke,e,6,6
.locate cout0,e,9,9
.locate cout1,e,12,12
.locate woute0,e,39,39
.locate woute1,e,42,42
.locate woute2,e,45,45
.locate woute3,e,48,48
.locate wlde,e,51,51
.locate ld_const0_e,e,54,54
.locate ld_const1_e,e,57,57
.locate lde,e,60,60


.locate wcount_ticks,s,6,6
.locate rout1,s,9,9
.locate rout0,s,12,12
.locate wouts3,s,39,39
.locate wouts2,s,42,42
.locate wouts1,s,45,45
.locate wouts0,s,48,48
.locate wlds,s,51,51
.locate ld_const0_s,s,54,54
.locate ld_const1_s,s,57,57
.locate lds,s,60,60


.library "../MAIN.LIB"   ; generic input library
.libout "ARM.LIB"

;*** Instantiate components


;*** Dynamic [R,C] Calculation

.instance r1=one()
.instance c1=one()	; Constant for increment
.instance rinc=add_2()
.instance cinc=add_2()	; For incrementing [R,C] from supercell-to-supercell
.instance rincl0=and2()
.instance rincl1=and2()
.instance rincl2=nand2()
.instance cincl0=and2()
.instance cincl1=and2()
.instance cincl2=nand2()	; Glue logic for Mod 3 operation


;*** Weight propagation throughout supercell network plus storage

.instance winor0=or2() 
.instance winor1=or2() 
.instance winor2=or2() 
.instance winor3=or2()	; Weight prop: compute Nin + Win

.instance wldor=or2()	; LOAD prop: compute Nin + Win

.instance wtld=and2()	; Final input to WEIGHT register

.instance wt=latch4()	; Stores supercell's weight

.instance wcount=count4_clkb_clrb()
; moves through [R,C] supercells for loading weight/X

.instance wcountl1=or2()
.instance wcountl2=not()

.instance wtl1=xnor()
.instance wtl2=xnor()
.instance wtl4=xnor()
.instance wtl5=xnor()
.instance wtl7=and2()
.instance wtl8=and2()
.instance wtl9=and2()	; Glue logic for weight [R,C] matching


;*** constant storage
.instance const0=latch4()
.instance const1=latch4()

;*** constant offset load logic
.instance constor0=or2()
.instance constor1=or2()


;*** X load

.instance xreg=dff_negedge()	; Stores supercell's STATE


;*** Calculation of running sum

.instance add=add_8() @[24,24]



;*** glue logic for conditional addition of weight to running sum
.instance xregl0=and2()
.instance xregl1=and2()
.instance xregl2=and2()
.instance xregl3=and2()


;*** LOAD SIGNAL logic (next state)
.instance ldlog1=or2()
.instance ldlog2=or2()
.instance ldlog3=or2()
.instance ldlog4=nor2()

;*** Now we wire the components together...


; LOAD signal
.net(ldw,ldlog1.a)
.net(ldn,ldlog1.b)
.net(ldlog1.x,lde,lds,ldlog2.a)
.net(ldlog2.b,wtld.x)
.net(ldlog2.x,xreg.clk) ; Load XREG on WLDE (preload) or LDE (STEP)

;* Only load sign bit into X reg in arm_w_X
;.net(ldlog4.a,add.s7) ; sign bit from running sum

.net(ldlog4.b,ld_const1_e) ; if ld_const1_e=1, add.s7 is blocked
.net(ldlog4.x,ldlog3.a)
.net(ldlog3.b,ld_const0_e) ; can specify new X value from ld_const0_e
                           ; if ld_const1_e=1; or from add.s7
.net(ldlog3.x,xreg.d);


;*** ROW calculation

.net(rin0,rinc.a0)
.net(rin1,rinc.a1)
.net(r1.x,rinc.b0)
.net(rinc.s0,rincl0.b,rincl2.b)
.net(rinc.s1,rincl1.b,rincl2.a)
.net(rincl2.x,rincl0.a,rincl1.a)
.net(rout0,rincl0.x)
.net(rout1,rincl1.x)


;*** COLUMN calculation

.net(cin0,cinc.a0)
.net(cin1,cinc.a1)
.net(c1.x,cinc.b0)
.net(cinc.s0,cincl0.b,cincl2.b)
.net(cinc.s1,cincl1.b,cincl2.a)
.net(cincl2.x,cincl0.a,cincl1.a)
.net(cout0,cincl0.x)
.net(cout1,cincl1.x)


;*** WEIGHT transmittal

.net(winn0,winor0.a)
.net(winw0,winor0.b)
.net(winor0.x,wouts0,woute0)
.net(winn1,winor1.a)
.net(winw1,winor1.b)
.net(winor1.x,wouts1,woute1)
.net(winn2,winor2.a)
.net(winw2,winor2.b)
.net(winor2.x,wouts2,woute2)
.net(winn3,winor3.a)
.net(winw3,winor3.b)
.net(winor3.x,wouts3,woute3)


;*** WEIGHT STORAGE

.net(woute0,wt.d0)
.net(woute1,wt.d1)
.net(woute2,wt.d2)
.net(woute3,wt.d3) ; from weight distribution

.net(wldn,wldor.a)
.net(wldw,wldor.b)
.net(wldor.x,wlds,wlde,wtld.a)


;*** Constant loading
.net(woute0,const0.d0,const1.d0)
.net(woute1,const0.d1,const1.d1)
.net(woute2,const0.d2,const1.d2)
.net(woute3,const0.d3,const1.d3)
.net(ld_const0_e,const0.ld)
.net(ld_const1_e,const1.ld)



;*** Move to next [R,C] for loading weight and X
.net(lde,wcountl2.a)
.net(wcountl2.x,wcount.clrb)
.net(wcount_tickw,wcountl1.a)
.net(wcount_tickn,wcountl1.b)
.net(wcountl1.x,wcount.clkb,wcount_ticke,wcount_ticks)

;*** Constant propogation
.net(ld_const0_w,constor0.a)
.net(ld_const0_n,constor0.b)
.net(constor0.x,ld_const0_s,ld_const0_e)
.net(ld_const1_w,constor1.a)
.net(ld_const1_n,constor1.b)
.net(constor1.x,ld_const1_s,ld_const1_e)


;*** Row/Column matching with WEIGHT load requests
.net(wcount.c2,wtl1.a)
.net(rin0,wtl1.b)
.net(wcount.c3,wtl2.a)
.net(rin1,wtl2.b)
.net(wcount.c0,wtl4.a)
.net(cin0,wtl4.b)
.net(wcount.c1,wtl5.a)
.net(cin1,wtl5.b)
.net(wtl1.x,wtl8.a)
.net(wtl2.x,wtl8.b)
.net(wtl4.x,wtl7.a)
.net(wtl5.x,wtl7.b)
.net(wtl9.a,wtl8.x)
.net(wtl9.b,wtl7.x)
.net(wtl9.x,wtld.b)
.net(wtld.x,wt.ld)

;*** Weight->Adder input

.net(xreg.q,xregl0.a,xregl1.a,xregl2.a,xregl3.a)
.net(wt.q0,xregl0.b)
.net(wt.q1,xregl1.b)
.net(wt.q2,xregl2.b)
.net(wt.q3,xregl3.b)
.net(xregl0.x,add.b0)
.net(xregl1.x,add.b1)
.net(xregl2.x,add.b2)
.net(xregl3.x,add.b3)
.net(xregl3.x,add.b4)
.net(xregl3.x,add.b5)
.net(xregl3.x,add.b6)
.net(xregl3.x,add.b7) ; Sign-extended!


;* Adder input
.net(const0.q0,add.a0)
.net(const0.q1,add.a1)
.net(const0.q2,add.a2)
.net(const0.q3,add.a3)
.net(const1.q0,add.a4)
.net(const1.q1,add.a5)
.net(const1.q2,add.a6)
.net(const1.q3,add.a7)

;* Adder output
.net(sout0,add.s0)
.net(sout1,add.s1)
.net(sout2,add.s2)
.net(sout3,add.s3)
.net(sout4,add.s4)
.net(sout5,add.s5)
.net(sout6,add.s6)
.net(sout7,add.s7)

;*** END OF INPUT ***
