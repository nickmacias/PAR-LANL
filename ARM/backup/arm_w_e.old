.define arm_w_e(54,54,\

;*** WESTERN INPUTS
		wcin0:in,wcin1:in,\  ; Column of cell in which to set weights
		cin0:in,cin1:in, \ ; Incoming column info

		sinw0:in,sinw1:in,sinw2:in,sinw3:in,\
		sinw4:in,sinw5:in,sinw6:in,sinw7:in,\  ; running sum

		winw0:in,winw1:in,winw2:in,winw3:in,\  ; weight to store
		wldw:in,\  ; LOAD signal to store weight

;*** WESTERN OUTPUTS
		;soutw7:out,soutw6:out,soutw5:out,soutw4:out,\
		;soutw3:out,soutw2:out,soutw1:out,soutw0:out,\; outgoing sum


;*** EASTERN INPUTS
		;sine0:in,sine1:in,sine2:in,sine3:in,\
		;sine4:in,sine5:in,sine6:in,sine7:in,\  ; running sum

;*** EASTERN OUTPUTS
		wcout0:out,wcout1:out,\ ; Column of cell in which to set wts
		cout0:out,cout1:out,\ ; outgoing column info

		soute0:out,soute1:out,soute2:out,soute3:out,\
		soute4:out,soute5:out,soute6:out,soute7:out,\ ; running sum

		woute0:out,woute1:out,woute2:out,woute3:out,\ ; weight to store

		wlde:out,\ ; WEIGHT Load SIGNAL


;*** NORTHERN INPUTS

		wrin1:in,wrin0:in,\ ; Row of cell in which to set wts
		rin1:in,rin0:in,\ ; Incoming row info

		;sinn7:in,sinn6:in,sinn5:in,sinn4:in,\
		;sinn3:in,sinn2:in,sinn1:in,sinn0:in,\ ; Running sum

		winn3:in,winn2:in,winn1:in,winn0:in,\; Incoming weight
		wldn:in,\ ; LOAD WEIGHT signal

;*** NORTHERN OUTPUTS
		;soutn7:out,soutn6:out,soutn5:out,soutn4:out,\
		;soutn3:out,soutn2:out,soutn1:out,soutn0:out,\; Running sum


;*** SOUTHERN INPUTS
		;sins0:in,sins1:in,sins2:in,sins3:in,\
		;sins4:in,sins5:in,sins6:in,sins7:in,\ ; Running sum

;*** SOUTHERN OUTPUTS
		wrout1:out,wrout0:out,\ ; Row of cell in which to set wts
		rout1:out,rout0:out,\;Row info

		;souts7:out,souts6:out,souts5:out,souts4:out,\
		;souts3:out,souts2:out,souts1:out,souts0:out,\; Running sum

		wouts3:out,wouts2:out,wouts1:out,wouts0:out,\; Weight
		wlds:out\; LOAD WEIGHT signal
	)


;*** Fix input/output locations so supercells line up

.locate wcin0,w,3,3
.locate wcin1,w,6,6
.locate cin0,w,9,9
.locate cin1,w,12,12
.locate sinw0,w,15,15
.locate sinw1,w,18,18
.locate sinw2,w,21,21
.locate sinw3,w,24,24
.locate sinw4,w,27,27
.locate sinw5,w,30,30
.locate sinw6,w,33,33
.locate sinw7,w,36,36
.locate winw0,w,39,39
.locate winw1,w,42,42
.locate winw2,w,45,45
.locate winw3,w,48,48
.locate wldw,w,51,51


.locate wrin1,n,3,3
.locate wrin0,n,6,6
.locate rin1,n,9,9
.locate rin0,n,12,12
.locate winn3,n,39,39
.locate winn2,n,42,42
.locate winn1,n,45,45
.locate winn0,n,48,48
.locate wldn,n,51,51


.locate wcout0,e,3,3
.locate wcout1,e,6,6
.locate cout0,e,9,9
.locate cout1,e,12,12
.locate soute0,e,15,15
.locate soute1,e,18,18
.locate soute2,e,21,21
.locate soute3,e,24,24
.locate soute4,e,27,27
.locate soute5,e,30,30
.locate soute6,e,33,33
.locate soute7,e,36,36
.locate woute0,e,39,39
.locate woute1,e,42,42
.locate woute2,e,45,45
.locate woute3,e,48,48
.locate wlde,e,51,51


.locate wrout1,s,3,3
.locate wrout0,s,6,6
.locate rout1,s,9,9
.locate rout0,s,12,12
.locate wouts3,s,39,39
.locate wouts2,s,42,42
.locate wouts1,s,45,45
.locate wouts0,s,48,48
.locate wlds,s,51,51

.library "../MAIN.LIB"   ; generic input library
.libout "ARM.LIB"

;*** Instantiate components


;*** Dynamic [R,C] Calculation

.instance r1=one()
.instance c1=one()	; Constant for increment
.instance rinc=add_2()
.instance cinc=add_2()	; For incrementing [R,C] from supercell-to-supercell
.instance rincl0=and2()
.instance rincl1=and2()
.instance rincl2=nand2()
.instance cincl0=and2()
.instance cincl1=and2()
.instance cincl2=nand2()	; Glue logic for Mod 3 operation


;*** Weight propagation throughout supercell network plus storage

.instance winor0=or2() 
.instance winor1=or2() 
.instance winor2=or2() 
.instance winor3=or2()	; Weight prop: compute Nin + Win

.instance wldor=or2()	; LOAD prop: compute Nin + Win

.instance wtld=and2()	; Final input to WEIGHT register

.instance wt=latch4()	; Stores supercell's weight

.instance wtl1=xnor()
.instance wtl2=xnor()
.instance wtl4=xnor()
.instance wtl5=xnor()
.instance wtl7=and2()
.instance wtl8=and2()
.instance wtl9=and2()	; Glue logic for weight [R,C] matching


;*** X load

.instance xreg=dff_poslevel()	; Stores supercell's STATE


;*** Calculation of running sum

.instance add=add_8() @[24,24]



;*** glue logic for conditional addition of weight to running sum
.instance xregl0=and2()
.instance xregl1=and2()
.instance xregl2=and2()
.instance xregl3=and2()


;*** Now we wire the components together...


;*** ROW calculation

.net(rin0,rinc.a0)
.net(rin1,rinc.a1)
.net(r1.x,rinc.b0)
.net(rinc.s0,rincl0.b,rincl2.b)
.net(rinc.s1,rincl1.b,rincl2.a)
.net(rincl2.x,rincl0.a,rincl1.a)
.net(rout0,rincl0.x)
.net(rout1,rincl1.x)


;*** COLUMN calculation

.net(cin0,cinc.a0)
.net(cin1,cinc.a1)
.net(c1.x,cinc.b0)
.net(cinc.s0,cincl0.b,cincl2.b)
.net(cinc.s1,cincl1.b,cincl2.a)
.net(cincl2.x,cincl0.a,cincl1.a)
.net(cout0,cincl0.x)
.net(cout1,cincl1.x)


;*** WEIGHT transmittal

.net(wrin0,wrout0)
.net(wrin1,wrout1)
.net(wcin0,wcout0)
.net(wcin1,wcout1)

.net(winn0,winor0.a)
.net(winw0,winor0.b)
.net(winor0.x,wouts0,woute0)
.net(winn1,winor1.a)
.net(winw1,winor1.b)
.net(winor1.x,wouts1,woute1)
.net(winn2,winor2.a)
.net(winw2,winor2.b)
.net(winor2.x,wouts2,woute2)
.net(winn3,winor3.a)
.net(winw3,winor3.b)
.net(winor3.x,wouts3,woute3)


;*** WEIGHT STORAGE

.net(woute0,wt.d0)
.net(woute1,wt.d1)
.net(woute2,wt.d2)
.net(woute3,wt.d3) ; from weight distribution

.net(wldn,wldor.a)
.net(wldw,wldor.b)
.net(wldor.x,wlds,wlde,wtld.a)

;*** Row/Column matching with WEIGHT load requests
.net(wrin0,wtl1.a)
.net(rin0,wtl1.b)
.net(wrin1,wtl2.a)
.net(rin1,wtl2.b)
.net(wcin0,wtl4.a)
.net(cin0,wtl4.b)
.net(wcin1,wtl5.a)
.net(cin1,wtl5.b)
.net(wtl1.x,wtl8.a)
.net(wtl2.x,wtl8.b)
.net(wtl4.x,wtl7.a)
.net(wtl5.x,wtl7.b)
.net(wtl9.a,wtl8.x)
.net(wtl9.b,wtl7.x)
.net(wtl9.x,wtld.b)
.net(wtld.x,wt.ld)

;*** Weight->Adder input

;***.net(xreg.clr,wtld.x)
.net(xreg.q,xregl0.a,xregl1.a,xregl2.a,xregl3.a)
.net(wt.q0,xregl0.b)
.net(wt.q1,xregl1.b)
.net(wt.q2,xregl2.b)
.net(wt.q3,xregl3.b)
.net(xregl0.x,add.b0)
.net(xregl1.x,add.b1)
.net(xregl2.x,add.b2)
.net(xregl3.x,add.b3)


;* Adder input
.net(sinw0,add.a0)
.net(sinw1,add.a1)
.net(sinw2,add.a2)
.net(sinw3,add.a3)
.net(sinw4,add.a4)
.net(sinw5,add.a5)
.net(sinw6,add.a6)
.net(sinw7,add.a7)

;* Adder output
.net(soute0,add.s0)
.net(soute1,add.s1)
.net(soute2,add.s2)
.net(soute3,add.s3)
.net(soute4,add.s4)
.net(soute5,add.s5)
.net(soute6,add.s6)
.net(soute7,add.s7)

;*** END OF INPUT ***
